clearvars; clc;
close all;
yalmip('clear')

addpath('utils');
addpath('utils\data');

load('parameters.mat');
load('nominalNondisp.mat');

%% Optimization variables
% Input
nonDispPower = sdpvar(gridWindows,1);
clustSoc0    = sdpvar(1,nCluster);

% Control actions
uCh          = sdpvar(gridWindows,nCluster);
uDch         = sdpvar(gridWindows,nCluster);
spinReserves = sdpvar(gridWindows,1);
% Outputs
bessPower    = sdpvar(gridWindows,1);
genPower     = sdpvar(gridWindows,1);
netPower     = sdpvar(gridWindows,1);

% States
clustSoc     = sdpvar(gridWindows+1,nCluster);


%% Constraints
gridConstraints = [];

% Load balance in nominal condition
gridConstraints = [gridConstraints; bessPower + genPower + netPower ==  nondispNomGrid(1:length(time15Min))];

% % Generator operational limits
gridConstraints = [gridConstraints; genPower <= maxGenPower];
gridConstraints = [gridConstraints; genPower >= minGenPower];

% % Network selling/buying constraint
gridConstraints = [gridConstraints; netPower <= maxNetPower];
gridConstraints = [gridConstraints; netPower >= minNetPower];

% % Minimum spin reserves
for t = 1:gridWindows
    gridConstraints = [gridConstraints; spinReserves(t) == maxGenPower - genPower(t) + min(sum(HLSampling^-1*bessCapacity.*fract.*(clustSoc(t,:)-0.1*ones(1,nCluster))),sum(maxDCHclustPower))];
    gridConstraints = [gridConstraints; spinReserves(t) >= minSpinReserve];
end

% Bess operational limits
gridConstraints = [gridConstraints; bessPower <= sum(maxDCHclustPower)];
gridConstraints = [gridConstraints; bessPower >= -sum(maxCHclustPower)];

% Bess operational limits
gridConstraints = [gridConstraints; uDch >= 0];
gridConstraints = [gridConstraints; uCh  >= 0];

for t = 1:gridWindows
    gridConstraints = [gridConstraints; uDch(t,:) <= maxDCHclustPower];
    gridConstraints = [gridConstraints; uCh(t,:)  <= maxCHclustPower];
end


for t = 1:gridWindows
    gridConstraints = [gridConstraints; (uCh(t,1) == 0 & uCh(t,2) == 0 & uCh(t,3) == 0 & uCh(t,4) == 0)  |  (uDch(t,1) == 0 & uDch(t,2) == 0 & uDch(t,3) == 0 & uDch(t,4) == 0)];
end

gridConstraints = [gridConstraints; sum(uDch,2) - sum(uCh,2)  == bessPower];

% SoC initial condition
gridConstraints = [gridConstraints; clustSoc(1,:) == clusterSocSim0];

% SoC dynamic
for t = 1:gridWindows
    gridConstraints = [gridConstraints; clustSoc(t+1,:) == clustSoc(t,:) + HLSampling*Kbch.*uCh(t,:) - HLSampling*Kbdch.*uDch(t,:)]; %#ok<*AGROW>
end
% SoC limit
gridConstraints = [gridConstraints; clustSoc <= 0.9];
gridConstraints = [gridConstraints; clustSoc >= 0.1];
gridConstraints = [gridConstraints; clustSoc(end,:) <= 0.55];
gridConstraints = [gridConstraints; clustSoc(end,:) >= 0.45];
%% Cost function
gridcostfnc = 0;

% Cost term
for t = 1:gridWindows
    gridcostfnc = gridcostfnc + (p1*genPower(t)^2 + p2*genPower(t) + p3) ...
                              + nbuy(t)*max([0,netPower(t)])...
                              + nsell(t)*min([0,netPower(t)]);
%                               - rcost(t)*max([0, spinReserves(t)-minSpinReserve]);
end

% % Bess filtering term
for t = 2:gridWindows
    gridcostfnc = gridcostfnc + bcost(t)*(bessPower(t)-bessPower(t-1))^2;
     gridcostfnc = gridcostfnc + bcost(t)*(netPower(t)-netPower(t-1))^2;
end

% Cluster balance term
h = 1;
for t = 2:gridWindows+1
    Lc{t} = [];
    for j = 1:nCluster-1
        for k = 1:nCluster-h
            clustError = clustSoc(t,j) - clustSoc(t,k + h); 
            Lc{t} = [Lc{t}, clustError];
        end
        h = h + 1;
    end
    h = 1;
    gridcostfnc = gridcostfnc + 1e3*norm(Lc{t},inf);
end

%% Optimization options
% Gurobi
    optimOptions = sdpsettings('solver','gurobi','verbose',2);

%% Optimizer definition
% % Input
% bessIn  = {clustSoc0, nonDispPower};
% % Output
% bessOut = {gridcostfnc, clustSoc, bessPower, uCh, uDch, genPower, netPower};
% Optimizer
% gridOptim  = optimizer(gridConstraints, gridcostfnc, optimOptions, bessIn, bessOut);
tic
gridOptim = optimize(gridConstraints, gridcostfnc, optimOptions);
dtGrid = toc;
%% Simulation

GridCost       = value(gridcostfnc);
GridOptimSoc   = value(clustSoc);
GridBessPower  = value(bessPower);
GridUCh        = value(uCh);
GridUDch       = value(uDch);
GridGenPower   = value(genPower);
GridNetPower   = value(netPower);
for i = 1:size(GridBessPower)
    if abs(GridBessPower(i)) < 1e-4
        GridFract(i,:)  = zeros(1,nCluster); %#ok<*SAGROW> 
    else
        GridFract(i,:)  = GridUCh(i,:)./GridBessPower(i) + GridUDch(i,:)./GridBessPower(i);
    end
end


save("utils\data\scheduledPower.mat","GridCost","GridOptimSoc","GridBessPower","GridGenPower","GridNetPower","GridFract");
save("utils\MPC_optimizer\gridOptim.mat","gridOptim")

%% Plot figures
plot_flag = true;
if plot_flag
    figure
    plot(time15Min, GridNetPower, time15Min, GridGenPower, time15Min, GridBessPower, time15Min, nondispNomPower(1:length(time15Min)));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power exchange during the day')

    figure
    plot([time15Min,24], GridOptimSoc);
    xlabel('Time [h]');ylabel('SoC [ ]');
    legend('A','B','C','D')
    title('Cluster state of charge during the day')
    yline(0.9,'r')
    yline(0.1,'r')

    figure
    tiledlayout(3,1)
    nexttile
    stairs(time15Min, GridUCh);
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Charging power')
    nexttile
    stairs(time15Min, GridUDch);
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharging power')
    nexttile
    stairs(time15Min, GridFract);
    xlabel('Time [h]');ylabel('Ci [ ]');
    title('Cluster fraction')
    legend('A','B','C','D')

    figure
    title('Spin Reserves');
    nexttile
    plot(time15Min,maxGenPower*ones(size(GridGenPower))-GridGenPower)
    xlabel('Time [h]');ylabel('Power [KW]');
    yline(minSpinReserve,'r')
    nexttile
    plot(time15Min,sum(HLSampling*bessCapacity.*fract.*GridOptimSoc(1:end-1,:),2))
    xlabel('Time [h]');ylabel('Power [KW]');
    yline(minSpinReserve,'r')
end

