clearvars; clc;
%close all;
yalmip('clear')

addpath('utils');
addpath('utils\data');

load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');

%% Optimization variables
% Inputs

for SHWindows = 1:LLWindows
    % Scheduled generator and network power
    PGenBar       = sdpvar(SHWindows,1);
    PNetBar       = sdpvar(SHWindows,1);

    % Gen power
    genPower      = sdpvar(SHWindows,nScenarios,'full');
    duGen         = sdpvar(SHWindows,nScenarios,'full');
    
    % Network power
    netPower      = sdpvar(SHWindows,nScenarios,'full');
    duNet         = sdpvar(SHWindows,nScenarios,'full');
    
    % Mixing coefficient
    alfa         = sdpvar(SHWindows,1);
    
    % delta Energy exchanged with the network
    eta           = sdpvar(SHWindows+1,nScenarios,'full');
    eta0          = sdpvar(1,1);
    gamma         = sdpvar(1);

    % Non dispachable power scenarios
    nondispScenarios  = sdpvar(SHWindows,nScenarios,'full');
    deltaScenarios    = sdpvar(SHWindows,nScenarios,'full');
  
    LLconstraints = [];
    % Operational constraints
%     LLconstraints = [LLconstraints; netPower  <= maxNetPower + 10];
%     LLconstraints = [LLconstraints; netPower  >= minNetPower - 10];
    LLconstraints = [LLconstraints; genPower <= maxGenPower];
    LLconstraints = [LLconstraints; genPower >= minGenPower];
    
    % gamma bounds
    LLconstraints = [LLconstraints; alfa >= 0];
    LLconstraints = [LLconstraints; alfa <= 1];
    LLconstraints = [LLconstraints; gamma >= 0;];

    % Network energy balance
    for i = 1:nScenarios
        LLconstraints = [LLconstraints; netPower(:,i) == duNet(:,i) + PNetBar]; %#ok<*AGROW>
        LLconstraints = [LLconstraints; genPower(:,i) == duGen(:,i) + PGenBar]; %#ok<*AGROW> 
    end
    
    
    % Energy difference
    for i = 1:nScenarios
        LLconstraints = [LLconstraints; eta(1,i) == eta0];
        for t = 1:SHWindows
            LLconstraints = [LLconstraints; eta(t+1,i) == eta(t,i) + duNet(t,i)*LLTimePeriod];
        end
%         LLconstraints = [LLconstraints; abs(eta(end,i)) <= gamma + 0.5*etaMax];        
        LLconstraints = [LLconstraints; eta(end,i) <= gamma + 0.5*etaMax];
        LLconstraints = [LLconstraints; -eta(end,i) <= gamma + 0.5*etaMax];
    end
    
    
    for i = 1:nScenarios
        % Power balance
        LLconstraints = [LLconstraints; genPower(:,i) + netPower(:,i) == nondispScenarios(:,i)];
        LLconstraints = [LLconstraints; deltaScenarios(:,i) == nondispScenarios(:,i) - PGenBar - PNetBar];
    
        for t = 1:SHWindows
            LLconstraints = [LLconstraints; duGen(t,i) == alfa(t)*deltaScenarios(t,i);];
            LLconstraints = [LLconstraints; duNet(t,i)  == (1-alfa(t))*deltaScenarios(t,i);];
        end
    
    
        % Cost function
         F(i) = 1e3*gamma^2 + 1*(eta(2:end,i)'*eta(2:end,i)) + 1*(duGen(:,i)'*duGen(:,i)); %#ok<*SAGROW> 
    end
    LLcostfnc = sum(F)/nScenarios;
    %% Optimization options
    % Gurobi
    optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.BarHomogeneous',0,'gurobi.NumericFocus',3,'gurobi.NonConvex',1);
    %optimOptions = sdpsettings('solver','mosek','verbose',2);
    LLIn  = {nondispScenarios, PGenBar, PNetBar, eta0};
    LLOut = {alfa, eta, genPower, netPower};
    MPC_LL_alfa_off{SHWindows} = optimizer(LLconstraints, LLcostfnc, optimOptions, LLIn, LLOut);
end

save('utils\MPC_optimizer\MPC_LL_alfa_off','MPC_LL_alfa_off');