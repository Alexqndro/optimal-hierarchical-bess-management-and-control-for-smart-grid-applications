clearvars; close all; clc
yalmip('clear')

%% Addpaths
addpath('utils\');
addpath('utils\data');
addpath('utils\MPC_optimizer');
addpath('Figures')
%% Load parameters, scheduled power and optimizers
load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');
load('results.mat');
%% Constraint violation

%% Energy costs 
results.GR.GridGenPower  = GridGenPower;
results.GR.GridNetPower  = GridNetPower;
results.GR.GridBessPower = GridBessPower;
results.GR.GridOptimSoc  = GridOptimSoc;


for k = 1:length(time15Min)
    results.LL.LLNetPower15Min(k,1)  = sum(results.LL.LLNetPower((k-1)*LLWindows + 1: k*LLWindows))/15;
    results.LL.LLGenPower15Min(k,1)  = sum(results.LL.LLGenPower((k-1)*LLWindows + 1: k*LLWindows))/15;
    results.LL.LLScenarios15Min(k,1) = sum(nondispRealPower((k-1)*LLWindows + 1: k*LLWindows))/15;
end

gridCost = HLSampling*(0.4*ones(size(time15Min))*results.LL.LLScenarios15Min - nsell*results.GR.GridNetPower  - gcost*results.GR.GridGenPower);
HLCost   = HLSampling*0.4*ones(size(time15Min))*results.LL.LLScenarios15Min - nsell*results.HL.HLNetPower - gcost*results.HL.HLGenPower;
LLCost   = HLSampling*0.4*ones(size(time15Min))*results.LL.LLScenarios15Min - nsell*results.LL.LLNetPower15Min - gcost*results.LL.LLGenPower15Min;

% %% FIGURE SAVING
PS = PLOT_STANDARDS();
% 
% Grid powers:
figure(1);
fig1_comps.fig = gcf;
hold on
fig1_comps.p1 = plot(results.T.time15min, results.GR.GridNetPower);
fig1_comps.p2 = plot(results.T.time15min, results.GR.GridGenPower);
fig1_comps.p3 = plot(results.T.time15min, results.GR.GridBessPower);
fig1_comps.p4 = plot(results.T.time15min, nondispNomGrid(1:96));
fig1_comps.p5 = plot(results.T.time15min, zeros(size(results.T.time15min)));
hold off
fig1_comps.plotTitle = title('Daily planned powers');
fig1_comps.plotXLabel = xlabel('time [h]');
fig1_comps.plotYLabel = ylabel('power [KW]');


fig1_comps.plotLegend = legend([fig1_comps.p1, fig1_comps.p2, fig1_comps.p3, fig1_comps.p4], 'Network', 'Generation', 'BESS', 'Net-demand', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
set(fig1_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
    legendHeight], 'Box', 'on');
set(fig1_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
    'EdgeColor', PS.MyGrey1);
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig1_comps.p1, 'Color', PS.Blue1, 'LineWidth', 2);
set(fig1_comps.p2, 'Color', PS.DGreen1, 'LineWidth', 2);
set(fig1_comps.p3, 'Color', PS.Red3, 'LineWidth', 2);
set(fig1_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig1_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig1_comps.p5, 'LineStyle', '-.', 'Color', PS.Red1, 'LineWidth', 1);

%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig1_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig1_comps, 'Figures/Grid_power_small.png', 'small');
SAVE_MY_FIGURE(fig1_comps, 'Figures/Grid_power_big.png', 'big');

% HL powers
figure(2);
fig2_comps.fig = gcf;
hold on
fig2_comps.p1 = plot(results.T.time15min, results.HL.HLNetPower);
fig2_comps.p2 = plot(results.T.time15min, results.HL.HLGenPower);
fig2_comps.p3 = plot(results.T.time15min, results.HL.HLBessPower(1:96));
fig2_comps.p4 = plot(results.T.time15min, nondispNomPower(1:96));
fig2_comps.p5 = plot(results.T.time15min, results.GR.GridNetPower);
fig2_comps.p6 = plot(results.T.time15min, results.GR.GridGenPower);
fig2_comps.p7 = plot(results.T.time15min, results.GR.GridBessPower);
fig2_comps.p8 = plot(results.T.time15min, nondispNomGrid(1:96));
fig2_comps.p9 = plot(results.T.time15min, zeros(size(results.T.time15min)));

hold off
fig2_comps.plotTitle = title('HL planned powers');
fig2_comps.plotXLabel = xlabel('time [h]');
fig2_comps.plotYLabel = ylabel('power [KW]');


fig1_comps.plotLegend = legend([fig2_comps.p1, fig2_comps.p2, fig2_comps.p3, fig2_comps.p4], ...
'HL Network', ' HL Generation', 'HL BESS', 'HL Net-demand', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
set(fig1_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
    legendHeight], 'Box', 'on');
set(fig1_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
    'EdgeColor', PS.MyGrey1);
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig2_comps.p1, 'Color', PS.Blue1, 'LineWidth', 2);
set(fig2_comps.p2, 'Color', PS.DGreen1, 'LineWidth', 2);
set(fig2_comps.p3, 'Color', PS.Red3, 'LineWidth', 2);
set(fig2_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig2_comps.p5, 'LineStyle', 'none', 'Marker', '.', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Blue1, 'MarkerFaceColor', PS.Blue1);
set(fig2_comps.p6, 'LineStyle', 'none', 'Marker', '.', 'MarkerSize', 8, 'MarkerEdgeColor', PS.DGreen1, 'MarkerFaceColor', PS.DGreen1);
set(fig2_comps.p7, 'LineStyle', 'none', 'Marker', '.', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Red3, 'MarkerFaceColor', PS.Red3);
set(fig2_comps.p8, 'LineStyle', 'none', 'Marker', '.', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Orange2, 'MarkerFaceColor', PS.Orange2);
set(fig2_comps.p9, 'LineStyle', '-.', 'Color', PS.Red1, 'LineWidth', 1);

%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig2_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig2_comps, 'Figures/HL_power_small.png', 'small');
SAVE_MY_FIGURE(fig2_comps, 'Figures/HL_power_big.png', 'big');


% LL powers
figure(3);
fig3_comps.fig = gcf;
hold on
fig3_comps.p1 = plot(results.T.time1min, results.LL.LLNetPower);
fig3_comps.p2 = plot(results.T.time1min, results.LL.LLGenPower);
fig3_comps.p3 = plot(results.T.time1min, results.LL.LLBessPower);
fig3_comps.p4 = plot(results.T.time1min, results.LL.Scenarios(1:1440,end));
fig3_comps.p5 = plot(results.T.time15min, results.HL.HLNetPower);
fig3_comps.p6 = plot(results.T.time15min, results.HL.HLGenPower);
fig3_comps.p7 = plot(results.T.time15min, results.HL.HLBessPower(1:96));
fig3_comps.p8 = plot(results.T.time15min, nondispNomPower(1:96));
fig3_comps.p9 = plot(results.T.time1min, zeros(size(results.T.time1min)));

hold off
fig3_comps.plotTitle = title('LL planned powers');
fig3_comps.plotXLabel = xlabel('time [h]');
fig3_comps.plotYLabel = ylabel('power [KW]');


fig3_comps.plotLegend = legend([fig3_comps.p1, fig3_comps.p2, fig3_comps.p3, fig3_comps.p4], ...
'LL Network', ' LL Generation', 'LL BESS', 'LL Net-demand', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
set(fig3_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
    legendHeight], 'Box', 'on');
set(fig3_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
    'EdgeColor', PS.MyGrey1);
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig3_comps.p1, 'Color', PS.Blue1, 'LineWidth', 2);
set(fig3_comps.p2, 'Color', PS.DGreen1, 'LineWidth', 2);
set(fig3_comps.p3, 'Color', PS.Red3, 'LineWidth', 2);
set(fig3_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
% set(fig3_comps.p5, 'LineStyle', 'none', 'Marker', 'x', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Blue1, 'MarkerFaceColor', PS.Blue1);
% set(fig3_comps.p6, 'LineStyle', 'none', 'Marker', 'x', 'MarkerSize', 8, 'MarkerEdgeColor', PS.DGreen1, 'MarkerFaceColor', PS.DGreen1);
% set(fig3_comps.p7, 'LineStyle', 'none', 'Marker', 'x', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Red3, 'MarkerFaceColor', PS.Red3);
% set(fig3_comps.p8, 'LineStyle', 'none', 'Marker', 'x', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Orange2, 'MarkerFaceColor', PS.Orange2);
set(fig3_comps.p5, 'LineStyle', '-.', 'Color', PS.Blue1, 'LineWidth', 1);
set(fig3_comps.p6, 'LineStyle', '-.', 'Color', PS.DGreen1, 'LineWidth', 1);
set(fig3_comps.p7, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig3_comps.p8, 'LineStyle', '-.', 'Color', PS.Orange2, 'LineWidth', 1);
set(fig3_comps.p9, 'LineStyle', '-.', 'Color', PS.Red1, 'LineWidth', 1);

%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig3_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig3_comps, 'Figures/LL_power_small.png', 'small');
SAVE_MY_FIGURE(fig3_comps, 'Figures/LL_power_big.png', 'big');

% SoC:
figure(4);
fig4_comps.fig = gcf;
hold on
fig4_comps.p1 = plot(results.T.time1min, results.LL.clusterSocSimi(1:end-1,1));
fig4_comps.p2 = plot(results.T.time1min, results.LL.clusterSocSimi(1:end-1,2));
fig4_comps.p3 = plot(results.T.time1min, results.LL.clusterSocSimi(1:end-1,3));
fig4_comps.p4 = plot(results.T.time1min, results.LL.clusterSocSimi(1:end-1,4));
fig4_comps.p5 = plot(results.T.time1min, 0.1*ones(size(results.T.time1min)));
fig4_comps.p6 = plot(results.T.time1min, 0.9*ones(size(results.T.time1min)));

hold off
fig4_comps.plotTitle = title('Cluster SoC');
fig4_comps.plotXLabel = xlabel('time [h]');
fig4_comps.plotYLabel = ylabel('SoC [ ]');


fig4_comps.plotLegend = legend([fig4_comps.p1, fig4_comps.p2, fig4_comps.p3, fig4_comps.p4], ...
'Cluster A', 'Cluster B', 'Cluster C', 'Cluster D', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
set(fig4_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
    legendHeight], 'Box', 'on');
set(fig4_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
    'EdgeColor', PS.MyGrey1);
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig4_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig4_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig4_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig4_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig4_comps.p5, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig4_comps.p6, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% ADJUST AXES PROPERTIES

% Change axis properties
ax = gca;
ax.YAxis.Limits = [0, 1];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig4_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig4_comps, 'Figures/SoC_ll_small.png', 'small');
SAVE_MY_FIGURE(fig4_comps, 'Figures/SoC_ll_big.png', 'big');

% Cluster fraction:
figure(5);
fig5_comps.fig = gcf;
fig5_comps.t1 = tiledlayout(2,1);

fig5_comps.n(1) = nexttile;
hold on
fig5_comps.p1 = plot(results.T.time15min, results.HL.clFractCH(1:end-1,1));
fig5_comps.p2 = plot(results.T.time15min, results.HL.clFractCH(1:end-1,2));
fig5_comps.p3 = plot(results.T.time15min, results.HL.clFractCH(1:end-1,3));
fig5_comps.p4 = plot(results.T.time15min, results.HL.clFractCH(1:end-1,4));
fig5_comps.p5 = plot(results.T.time15min, zeros(size(results.T.time15min)));
fig5_comps.p6 = plot(results.T.time15min, ones(size(results.T.time15min)));

fig5_comps.plotTitle = title('Charging cluster fractions');
fig5_comps.plotXLabel = xlabel('time [h]');
fig5_comps.plotYLabel = ylabel('$C_{j,CH}$ []','Interpreter','latex');
fig5_comps.plotLegend = legend([fig5_comps.p1, fig5_comps.p2, fig5_comps.p3, fig5_comps.p4], ...
'CH A', 'CH B', 'CH C', 'CH D', 'Interpreter', 'latex');%
legendX0 = .7; legendY0 = .7; legendWidth = .15; legendHeight = .15;
set(fig5_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
     legendHeight], 'Box', 'on');
set(fig5_comps.plotLegend, 'FontSize', 10, 'LineWidth', 0.5, ...
     'EdgeColor', PS.MyGrey1);
ax = gca;
ax.YAxis.Limits = [-0.1, 1.1];
hold off

fig5_comps.n(2) = nexttile;
fig5_comps.p7 = plot(results.T.time15min, results.HL.clFractDCH(1:end-1,1));
hold on
fig5_comps.p8 = plot(results.T.time15min, results.HL.clFractDCH(1:end-1,2));
fig5_comps.p9 = plot(results.T.time15min, results.HL.clFractDCH(1:end-1,3));
fig5_comps.p10 = plot(results.T.time15min, results.HL.clFractDCH(1:end-1,4));
fig5_comps.p11 = plot(results.T.time15min, zeros(size(results.T.time15min)));
fig5_comps.p12 = plot(results.T.time15min, ones(size(results.T.time15min)));
fig5_comps.plotTitle = title('Discharging cluster fractions');
fig5_comps.plotXLabel = xlabel('time [h]');
fig5_comps.plotYLabel = ylabel('$C_{j,DCH}$ []','Interpreter','latex');
fig5_comps.plotLegend = legend([fig5_comps.p5, fig5_comps.p6, fig5_comps.p7, fig5_comps.p8], ...
'DCH A', 'DCH B', 'DCH C', 'DCH D', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .18; legendWidth = .15; legendHeight = .15;
set(fig5_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
     legendHeight], 'Box', 'on');
set(fig5_comps.plotLegend, 'FontSize', 10, 'LineWidth', 0.5, ...
     'EdgeColor', PS.MyGrey1);
ax = gca;
ax.YAxis.Limits = [-0.1, 1.1];
hold off

%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig5_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig5_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig5_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig5_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig5_comps.p5, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig5_comps.p6, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);

set(fig5_comps.p7, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig5_comps.p8, 'Color', PS.Green1, 'LineWidth', 2);
set(fig5_comps.p9, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig5_comps.p10, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig5_comps.p11, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig5_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% ADJUST AXES PROPERTIES

% Change axis properties

%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig5_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig5_comps, 'Figures/clust_frac_small.png', 'small');
SAVE_MY_FIGURE(fig5_comps, 'Figures/clust_frac_big.png', 'big');


% Cluster powers:
figure(6);
fig6_comps.fig = gcf;
fig6_comps.t1 = tiledlayout(2,1);

fig6_comps.n(1) = nexttile;
hold on
fig6_comps.p1 = plot(results.T.time1min, results.LL.uchReal(:,1));
fig6_comps.p2 = plot(results.T.time1min, results.LL.uchReal(:,2));
fig6_comps.p3 = plot(results.T.time1min, results.LL.uchReal(:,3));
fig6_comps.p4 = plot(results.T.time1min, results.LL.uchReal(:,4));
fig6_comps.plotTitle = title('Charging cluster fractions');
fig6_comps.plotXLabel = xlabel('time [h]');
fig6_comps.plotYLabel = ylabel('power [KW]');
fig6_comps.plotLegend = legend([fig6_comps.p1, fig6_comps.p2, fig6_comps.p3, fig6_comps.p4], ...
'CH A', 'CH B', 'CH C', 'CH D', 'Interpreter', 'latex');%
legendX0 = .7; legendY0 = .7; legendWidth = .15; legendHeight = .15;
set(fig6_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
     legendHeight], 'Box', 'on');
set(fig6_comps.plotLegend, 'FontSize', 10, 'LineWidth', 0.5, ...
     'EdgeColor', PS.MyGrey1);
hold off

fig6_comps.n(2) = nexttile;
fig6_comps.p5 = plot(results.T.time1min, results.LL.udchReal(:,1));
hold on
fig6_comps.p6 = plot(results.T.time1min, results.LL.udchReal(:,2));
fig6_comps.p7 = plot(results.T.time1min, results.LL.udchReal(:,3));
fig6_comps.p8 = plot(results.T.time1min, results.LL.udchReal(:,4));
fig6_comps.plotTitle = title('Discharging cluster fractions');
fig6_comps.plotXLabel = xlabel('time [h]');
fig6_comps.plotYLabel = ylabel('power [KW]');
fig6_comps.plotLegend = legend([fig6_comps.p5, fig6_comps.p6, fig6_comps.p7, fig6_comps.p8], ...
'DCH A', 'DCH B', 'DCH C', 'DCH D', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .18; legendWidth = .15; legendHeight = .15;
set(fig6_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
     legendHeight], 'Box', 'on');
set(fig6_comps.plotLegend, 'FontSize', 10, 'LineWidth', 0.5, ...
     'EdgeColor', PS.MyGrey1);
hold off

%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig6_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig6_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig6_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig6_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig6_comps.p5, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig6_comps.p6, 'Color', PS.Green1, 'LineWidth', 2);
set(fig6_comps.p7, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig6_comps.p8, 'Color', PS.Orange2, 'LineWidth', 2);
% ADJUST AXES PROPERTIES

% Change axis properties

%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig6_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig6_comps, 'Figures/clust_power_small.png', 'small');
SAVE_MY_FIGURE(fig6_comps, 'Figures/clust_power_big.png', 'big');

% SoC:
figure(7);
fig7_comps.fig = gcf;
hold on
fig7_comps.p1 = plot(results.T.time15min, results.GR.GridOptimSoc(1:end-1,1));
fig7_comps.p2 = plot(results.T.time15min, results.GR.GridOptimSoc(1:end-1,2));
fig7_comps.p3 = plot(results.T.time15min, results.GR.GridOptimSoc(1:end-1,3));
fig7_comps.p4 = plot(results.T.time15min, results.GR.GridOptimSoc(1:end-1,4));
fig7_comps.p5 = plot(results.T.time15min, 0.1*ones(size(results.T.time15min)));
fig7_comps.p6 = plot(results.T.time15min, 0.9*ones(size(results.T.time15min)));

hold off
fig7_comps.plotTitle = title('Daily planned SoC');
fig7_comps.plotXLabel = xlabel('time [h]');
fig7_comps.plotYLabel = ylabel('SoC []');


fig7_comps.plotLegend = legend([fig7_comps.p1, fig7_comps.p2, fig7_comps.p3, fig7_comps.p4], ...
'Cluster A', 'Cluster B', 'Cluster C', 'Cluster D', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
set(fig7_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
    legendHeight], 'Box', 'on');
set(fig7_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
    'EdgeColor', PS.MyGrey1);
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig7_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig7_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig7_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig7_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig7_comps.p5, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig7_comps.p6, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% ADJUST AXES PROPERTIES

% Change axis properties
ax = gca;
ax.YAxis.Limits = [0, 1];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig7_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig7_comps, 'Figures/SoC_small_grid.png', 'small');
SAVE_MY_FIGURE(fig7_comps, 'Figures/SoC_big_grid.png', 'big');

% SoC 2
figure(8);
fig8_comps.fig = gcf;
hold on
fig8_comps.p1 = plot(results.T.time1min(1:180), results.LL.clusterSocSimi(1:180,1));
fig8_comps.p2 = plot(results.T.time1min(1:180), results.LL.clusterSocSimi(1:180,2));
fig8_comps.p3 = plot(results.T.time1min(1:180), results.LL.clusterSocSimi(1:180,3));
fig8_comps.p4 = plot(results.T.time1min(1:180), results.LL.clusterSocSimi(1:180,4));
fig8_comps.p5 = plot(results.T.time1min(1:180), 0.1*ones(1,180));
fig8_comps.p6 = plot(results.T.time1min(1:180), 0.9*ones(1,180));

hold off
fig8_comps.plotTitle = title('Cluster SoC');
fig8_comps.plotXLabel = xlabel('time [h]');
fig8_comps.plotYLabel = ylabel('SoC []');


fig8_comps.plotLegend = legend([fig8_comps.p1, fig8_comps.p2, fig8_comps.p3, fig8_comps.p4], ...
'Cluster A', 'Cluster B', 'Cluster C', 'Cluster D', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
set(fig8_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
    legendHeight], 'Box', 'on');
set(fig8_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
    'EdgeColor', PS.MyGrey1);
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig8_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig8_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig8_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig8_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig8_comps.p5, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig8_comps.p6, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% ADJUST AXES PROPERTIES

% Change axis properties
ax = gca;
ax.YAxis.Limits = [0, 1];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig8_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig8_comps, 'Figures/SoC_bal_small.png', 'small');
SAVE_MY_FIGURE(fig8_comps, 'Figures/SoC_bal_big.png', 'big');


figure(9);
fig9_comps.fig = gcf;
hold on
fig9_comps.p1 = plot(results.T.time15min, nbuy);
fig9_comps.p2 = plot(results.T.time15min, nsell);
fig9_comps.p3 = plot(results.T.time15min, 0.4*ones(size(time15Min)));

hold off
fig9_comps.plotTitle = title('Day-ahead energy prices');
fig9_comps.plotXLabel = xlabel('time [h]');
fig9_comps.plotYLabel = ylabel('cost [€]');


fig9_comps.plotLegend = legend([fig9_comps.p1, fig9_comps.p2, fig9_comps.p3], ...
'Buying cost', 'Selling price', 'Load selling price', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
set(fig9_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
    legendHeight], 'Box', 'on');
set(fig9_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
    'EdgeColor', PS.MyGrey1);
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig9_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig9_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig9_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);

% ADJUST AXES PROPERTIES

% Change axis properties
ax = gca;
ax.YAxis.Limits = [0, 1.2];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig9_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig9_comps, 'Figures/costs_small.png', 'small');
SAVE_MY_FIGURE(fig9_comps, 'Figures/costs_big.png', 'big');

figure(10);
fig10_comps.fig = gcf;
fig10_comps.t1 = tiledlayout(2,1);

fig10_comps.n(1) = nexttile;
fig10_comps.p1 = plot(P_genspan, C_approx);
fig10_comps.plotTitle = title('Generation cost');
fig10_comps.plotXLabel = xlabel('Power [KW]');
fig10_comps.plotYLabel = ylabel(['price ', num2str(char(8364))]);


fig10_comps.n(2) = nexttile;
fig10_comps.p2 = plot(P_genspan, C_approx./P_genspan);
fig10_comps.plotTitle = title('Generation cost for KW produced');
fig10_comps.plotXLabel = xlabel('Power [KW]');
fig10_comps.plotYLabel = ylabel(['price ', num2str(char(8364)), '/KW']);
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
% set(fig10_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig10_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig10_comps.p2, 'Color', PS.Blue3, 'LineWidth', 2);
% Change axis properties
ax = gca;
ax.YAxis.Limits = [0.25, 0.4];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig10_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig10_comps, 'Figures/gencost_small.png', 'small');
SAVE_MY_FIGURE(fig10_comps, 'Figures/gencost_big.png', 'big');

figure(11);
fig11_comps.fig = gcf;
fig11_comps.p1 = plot(results.T.time15min, 0.9*PVNomPower(1:length(results.T.time15min)));
fig11_comps.plotTitle = title('Nominal PV production');
fig11_comps.plotXLabel = xlabel('time [h]');
fig11_comps.plotYLabel = ylabel('Power [KW]');

%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig11_comps.p1, 'Color', PS.Red1, 'LineWidth', 2);

% ADJUST AXES PROPERTIES

% Change axis properties
ax = gca;
ax.YAxis.Limits = [0, 70];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig11_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig11_comps, 'Figures/pv_nom_small.png', 'small');
SAVE_MY_FIGURE(fig11_comps, 'Figures/pv_nom_big.png', 'big');

figure(12);
fig12_comps.fig = gcf;
fig12_comps.p1 = plot(results.T.time15min, loadNomPower(1:length(results.T.time15min)));
fig12_comps.plotTitle = title('Nominal load demand');
fig12_comps.plotXLabel = xlabel('time [h]');
fig12_comps.plotYLabel = ylabel('Power [KW]');

%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig12_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);

% ADJUST AXES PROPERTIES

% Change axis properties
ax = gca;
ax.YAxis.Limits = [55, 90];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig12_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig12_comps, 'Figures/load_nom_small.png', 'small');
SAVE_MY_FIGURE(fig12_comps, 'Figures/load_nom_big.png', 'big');

% Cluster fraction:
figure(13);
fig13_comps.fig = gcf;
hold on
fig13_comps.p1 = plot(results.T.time15min(1:12), results.HL.clFractDCH(1:12,1));
fig13_comps.p2 = plot(results.T.time15min(1:12), results.HL.clFractDCH(1:12,2));
fig13_comps.p3 = plot(results.T.time15min(1:12), results.HL.clFractDCH(1:12,3));
fig13_comps.p4 = plot(results.T.time15min(1:12), results.HL.clFractDCH(1:12,4));
fig13_comps.p5 = plot(results.T.time15min(1:12), zeros(1,12));
fig13_comps.p6 = plot(results.T.time15min(1:12), ones(1,12));
fig13_comps.plotTitle = title('Discharging cluster fractions');
fig13_comps.plotXLabel = xlabel('time [h]');
fig13_comps.plotYLabel = ylabel('$C_{j,DCH}$ []','Interpreter','latex');
fig13_comps.plotLegend = legend([fig13_comps.p1, fig13_comps.p2, fig13_comps.p3, fig13_comps.p4], ...
'DCH A', 'DCH B', 'DCH C', 'DCH D', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .18; legendWidth = .15; legendHeight = .15;
set(fig13_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
     legendHeight], 'Box', 'on');
set(fig13_comps.plotLegend, 'FontSize', 10, 'LineWidth', 0.5, ...
     'EdgeColor', PS.MyGrey1);
ax = gca;
ax.YAxis.Limits = [-0.1, 1.1];
hold off
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig13_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig13_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig13_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig13_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig13_comps.p5, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig13_comps.p6, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% ADJUST AXES PROPERTIES

% Change axis properties

%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig13_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig13_comps, 'Figures/clust_frac_bal_small.png', 'small');
SAVE_MY_FIGURE(fig13_comps, 'Figures/clust_frac_bal_big.png', 'big');

figure(14);
fig14_comps.fig = gcf;
hold on
fig14_comps.p1 = plot(results.T.time15min, PVNomPower(1:length(results.T.time15min)));
fig14_comps.p2 = plot(results.T.time15min, 0.9*PVNomPower(1:length(results.T.time15min)));
fig14_comps.plotTitle = title('Updated nominal PV production');
hold off
fig14_comps.plotXLabel = xlabel('time [h]');
fig14_comps.plotYLabel = ylabel('Power [KW]');
fig14_comps.plotLegend = legend([fig14_comps.p1, fig14_comps.p2], ...
'Updated PV', 'Day-ahead PV', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .18; legendWidth = .15; legendHeight = .15;
set(fig14_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
     legendHeight], 'Box', 'on');
set(fig14_comps.plotLegend, 'FontSize', 10, 'LineWidth', 0.5, ...
     'EdgeColor', PS.MyGrey1);
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig14_comps.p1, 'Color', PS.Red1, 'LineWidth', 2);
set(fig14_comps.p2, 'LineStyle', '--', 'Color', PS.Red3, 'LineWidth', 2);

% ADJUST AXES PROPERTIES

% Change axis properties
ax = gca;
ax.YAxis.Limits = [0, 70];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig14_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig14_comps, 'Figures/pv_hl_small.png', 'small');
SAVE_MY_FIGURE(fig14_comps, 'Figures/pv_hl_big.png', 'big');

figure(15);
fig15_comps.fig = gcf;
hold on
fig15_comps.p1 = plot(results.T.time15min, nbuy_HL);
fig15_comps.p2 = plot(results.T.time15min, nsell_HL);
fig15_comps.p3 = plot(results.T.time15min, 0.4*ones(size(time15Min)));
fig15_comps.p4 = plot(results.T.time15min, nsell);

hold off
fig15_comps.plotTitle = title('Intraday energy prices');
fig15_comps.plotXLabel = xlabel('time [h]');
fig15_comps.plotYLabel = ylabel('cost [€]');


fig15_comps.plotLegend = legend([fig15_comps.p1, fig15_comps.p2, fig15_comps.p3, fig15_comps.p4], ...
'Buying cost', 'Selling price', 'Load selling price','Day-ahead selling price', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
set(fig15_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
    legendHeight], 'Box', 'on');
set(fig15_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
    'EdgeColor', PS.MyGrey1);
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig15_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig15_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig15_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig15_comps.p4, 'LineStyle', '--', 'Color', PS.Red3, 'LineWidth', 2);

% ADJUST AXES PROPERTIES

% Change axis properties
ax = gca;
ax.YAxis.Limits = [0, 1.2];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig15_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig15_comps, 'Figures/costs_hl_small.png', 'small');
SAVE_MY_FIGURE(fig15_comps, 'Figures/costs_hl_big.png', 'big');

% SoC:
figure(16);
fig16_comps.fig = gcf;
hold on
fig16_comps.p1 = plot(results.T.time1min, results.LL.clusterSocSimi(1:end-1,1));
fig16_comps.p2 = plot(results.T.time1min, results.LL.clusterSocSimi(1:end-1,2));
fig16_comps.p3 = plot(results.T.time1min, results.LL.clusterSocSimi(1:end-1,3));
fig16_comps.p4 = plot(results.T.time1min, results.LL.clusterSocSimi(1:end-1,4));
fig16_comps.p5 = plot(results.T.time15min, results.GR.GridOptimSoc(1:end-1,1));
fig16_comps.p6 = plot(results.T.time15min, results.GR.GridOptimSoc(1:end-1,2));
fig16_comps.p7 = plot(results.T.time15min, results.GR.GridOptimSoc(1:end-1,3));
fig16_comps.p8 = plot(results.T.time15min, results.GR.GridOptimSoc(1:end-1,4));
fig16_comps.p9 = plot(results.T.time1min, 0.1*ones(size(results.T.time1min)));
fig16_comps.p10 = plot(results.T.time1min, 0.9*ones(size(results.T.time1min)));

fig16_comps.plotTitle = title('Cluster SoC');
fig16_comps.plotXLabel = xlabel('time [h]');
fig16_comps.plotYLabel = ylabel('SoC [ ]');


fig16_comps.plotLegend = legend([fig16_comps.p1, fig16_comps.p2, fig16_comps.p3, fig16_comps.p4], ...
'Cluster A', 'Cluster B', 'Cluster C', 'Cluster D', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
set(fig16_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
    legendHeight], 'Box', 'on');
set(fig16_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
    'EdgeColor', PS.MyGrey1);
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig16_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig16_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig16_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig16_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig16_comps.p5, 'LineStyle', 'none', 'Marker', '.', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Blue5, 'MarkerFaceColor', PS.Blue5);
set(fig16_comps.p6, 'LineStyle', 'none', 'Marker', '.', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Green5, 'MarkerFaceColor', PS.Green5);
set(fig16_comps.p7, 'LineStyle', 'none', 'Marker', '.', 'MarkerSize', 8, 'MarkerEdgeColor', PS.DBlue5, 'MarkerFaceColor', PS.DBlue5);
set(fig16_comps.p8, 'LineStyle', 'none', 'Marker', '.', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Orange5, 'MarkerFaceColor', PS.Orange5);
set(fig16_comps.p9, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig16_comps.p10, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% ADJUST AXES PROPERTIES

% Change axis properties
ax = gca;
ax.YAxis.Limits = [0, 1];
hold off

%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig16_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig16_comps, 'Figures/SoC_small.png', 'small');
SAVE_MY_FIGURE(fig16_comps, 'Figures/SoC_big.png', 'big');

% SoC:
for k = 1:96
    results.LL.clusterSocSimi15min(k,:) = mean(results.LL.clusterSocSimi(15*(k-1)+1:15*k,:),1)
end

res_gen = maxGenPower - results.HL.HLGenPower(1:end,1);
res_bes = min(sum(maxDCHclustPower.*ones(96,4),2), sum(HLSampling^-1*bessCapacity.*fract.*( results.LL.clusterSocSimi15min-0.1*ones(96,nCluster)),2));
figure(17);
fig17_comps.fig = gcf;
hold on
fig17_comps.p1 = plot(results.T.time15min, res_gen);
fig17_comps.p2 = plot(results.T.time15min, res_bes);
fig17_comps.p3 = plot(results.T.time15min, res_gen + res_bes);
fig17_comps.p4 = plot(results.T.time15min, minSpinReserve*ones(1,96));
fig17_comps.plotTitle = title('Spinning reserves');
fig17_comps.plotXLabel = xlabel('time [h]');
fig17_comps.plotYLabel = ylabel('Power [KW]');


fig17_comps.plotLegend = legend([fig17_comps.p1, fig17_comps.p2, fig17_comps.p3, fig17_comps.p4], ...
'Plant spinning res', 'BESS spinning res', 'Total spinning res', 'Minimum spinning res', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
set(fig17_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
    legendHeight], 'Box', 'on');
set(fig17_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
    'EdgeColor', PS.MyGrey1);
hold off

%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig17_comps.p1, 'LineStyle', '-.', 'Color', PS.DGreen1);
set(fig17_comps.p2, 'LineStyle','-.', 'Color', PS.Red3);
set(fig17_comps.p3, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig17_comps.p4, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 2);
% ADJUST AXES PROPERTIES

% Change axis properties

%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig17_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig17_comps, 'Figures/spin_res_small.png', 'small');
SAVE_MY_FIGURE(fig17_comps, 'Figures/spin_res_big.png', 'big');

figure(18);
fig18_comps.fig = gcf;
fig18_comps.t1 = tiledlayout(2,2);
fig18_comps.n(1) = nexttile;
hold on
fig18_comps.p1 = plot(results.T.time1min(1:60), results.BMS.BatterySoCA(1:60,1));
fig18_comps.p2 = plot(results.T.time1min(1:60), results.BMS.BatterySoCA(1:60,2));
fig18_comps.p3 = plot(results.T.time1min(1:60), results.BMS.BatterySoCA(1:60,3));
fig18_comps.p4 = plot(results.T.time1min(1:60), results.BMS.BatterySoCA(1:60,4));
fig18_comps.p12 = plot(results.T.time1min(1:60), 0.1*ones(1,60));
fig18_comps.p13 = plot(results.T.time1min(1:60), 0.9*ones(1,60));
hold off
fig18_comps.plotTitle = title('Cluster A battery SoCs');
fig18_comps.plotXLabel = xlabel('Time [h]');
fig18_comps.plotYLabel = ylabel('SoC [ ]');
set(fig18_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig18_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig18_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig18_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig18_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig18_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [0.0, 1];

fig18_comps.n(2) = nexttile;
hold on
fig18_comps.p5 = plot(results.T.time1min(1:60), results.BMS.BatterySoCB(1:60,1));
fig18_comps.p6 = plot(results.T.time1min(1:60), results.BMS.BatterySoCB(1:60,2));
fig18_comps.p7 = plot(results.T.time1min(1:60), results.BMS.BatterySoCB(1:60,3));
fig18_comps.p8 = plot(results.T.time1min(1:60), results.BMS.BatterySoCB(1:60,4));
fig18_comps.p12 = plot(results.T.time1min(1:60), 0.1*ones(1,60));
fig18_comps.p13 = plot(results.T.time1min(1:60), 0.9*ones(1,60));
hold off

fig18_comps.plotTitle = title('Cluster B battery SoCs');
fig18_comps.plotXLabel = xlabel('Time [h]');
fig18_comps.plotYLabel = ylabel('SoC [ ]');
set(fig18_comps.p5, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig18_comps.p6, 'Color', PS.Green1, 'LineWidth', 2);
set(fig18_comps.p7, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig18_comps.p8, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig18_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig18_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [0.0, 1];

fig18_comps.n(3) = nexttile;
hold on
fig18_comps.p9  = plot(results.T.time1min(1:60), results.BMS.BatterySoCC(1:60,1));
fig18_comps.p10 = plot(results.T.time1min(1:60), results.BMS.BatterySoCC(1:60,2));
fig18_comps.p12 = plot(results.T.time1min(1:60), 0.1*ones(1,60));
fig18_comps.p13 = plot(results.T.time1min(1:60), 0.9*ones(1,60));
hold off
fig18_comps.plotTitle = title('Cluster C battery SoCs');
fig18_comps.plotXLabel = xlabel('Time [h]');
fig18_comps.plotYLabel = ylabel('SoC [ ]');
set(fig18_comps.p9, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig18_comps.p10, 'Color', PS.Green1, 'LineWidth', 2);
set(fig18_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig18_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [0.0, 1];

fig18_comps.n(4) = nexttile;
hold on
fig18_comps.p11 = plot(results.T.time1min(1:60), results.BMS.BatterySoCD(1:60,1));
fig18_comps.p12 = plot(results.T.time1min(1:60), 0.1*ones(1,60));
fig18_comps.p13 = plot(results.T.time1min(1:60), 0.9*ones(1,60));
hold off
fig18_comps.plotTitle = title('Cluster D battery SoCs');
fig18_comps.plotXLabel = xlabel('Time [h]');
fig18_comps.plotYLabel = ylabel('SoC [ ]');
set(fig18_comps.p11, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig18_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig18_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% Change axis properties
ax = gca;
ax.YAxis.Limits = [0.0, 1];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig18_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig18_comps, 'Figures/battery_soc_small.png', 'small');
SAVE_MY_FIGURE(fig18_comps, 'Figures/battery_soc_big.png', 'big');

time5min = 0:5/60:24-5/60;
figure(19);
fig19_comps.fig = gcf;
fig19_comps.t1 = tiledlayout(2,2);
fig19_comps.n(1) = nexttile;
hold on
fig19_comps.p1 = plot(time5min(1:end), results.BMS.piA(1:end,1));
fig19_comps.p2 = plot(time5min(1:end), results.BMS.piA(1:end,2));
fig19_comps.p3 = plot(time5min(1:end), results.BMS.piA(1:end,3));
fig19_comps.p4 = plot(time5min(1:end), results.BMS.piA(1:end,4));
fig19_comps.p12 = plot(time5min(1:end), 0.0*ones(size(time5min)));
fig19_comps.p13 = plot(time5min(1:end), 1.0*ones(size(time5min)));
hold off
fig19_comps.plotTitle = title('Cluster A battery fraction');
fig19_comps.plotXLabel = xlabel('Time [h]');
fig19_comps.plotYLabel = ylabel('$\pi_j$ [ ]','Interpreter','latex');
set(fig19_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig19_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig19_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig19_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig19_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig19_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [-0.1, 1.1];


fig19_comps.n(2) = nexttile;
hold on

fig19_comps.p5 = plot(time5min(1:end), results.BMS.piB(1:end,1));
fig19_comps.p6 = plot(time5min(1:end), results.BMS.piB(1:end,2));
fig19_comps.p7 = plot(time5min(1:end), results.BMS.piB(1:end,3));
fig19_comps.p8 = plot(time5min(1:end), results.BMS.piB(1:end,4));
fig19_comps.p12 = plot(time5min(1:end), 0.0*ones(size(time5min)));
fig19_comps.p13 = plot(time5min(1:end), 1.0*ones(size(time5min)));

hold off

fig19_comps.plotTitle = title('Cluster B battery fraction');
fig19_comps.plotXLabel = xlabel('Time [h]');
fig19_comps.plotYLabel = ylabel('$\pi_j$ [ ]','Interpreter','latex');
set(fig19_comps.p5, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig19_comps.p6, 'Color', PS.Green1, 'LineWidth', 2);
set(fig19_comps.p7, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig19_comps.p8, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig19_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig19_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [-0.1, 1.1];
fig19_comps.n(3) = nexttile;
hold on
fig19_comps.p9 = plot(time5min(1:end), results.BMS.piC(1:end,1));
fig19_comps.p10 = plot(time5min(1:end), results.BMS.piC(1:end,2));
fig19_comps.p12 = plot(time5min(1:end), 0.0*ones(size(time5min)));
fig19_comps.p13 = plot(time5min(1:end), 1.0*ones(size(time5min)));
hold off
fig19_comps.plotTitle = title('Cluster C battery fraction');
fig19_comps.plotXLabel = xlabel('Time [h]');
fig19_comps.plotYLabel = ylabel('$\pi_j$ [ ]','Interpreter','latex');
set(fig19_comps.p9, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig19_comps.p10, 'Color', PS.Green1, 'LineWidth', 2);
set(fig19_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig19_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [-0.1, 1.1];

fig19_comps.n(4) = nexttile;
hold on
fig19_comps.p11 = plot(time5min(1:end), ones(size(time5min)));
fig19_comps.p12 = plot(time5min(1:end), 0.0*ones(size(time5min)));
fig19_comps.p13 = plot(time5min(1:end), 1.0*ones(size(time5min)));
hold off
fig19_comps.plotTitle = title('Cluster D battery fraction');
fig19_comps.plotXLabel = xlabel('Time [h]');
fig19_comps.plotYLabel = ylabel('$\pi_j$ [ ]','Interpreter','latex');
set(fig19_comps.p11, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig19_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig19_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [-0.1, 1.1];
%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
% set(fig10_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);


% Change axis properties

%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig19_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig19_comps, 'Figures/battery_frac2_small.png', 'small');
SAVE_MY_FIGURE(fig19_comps, 'Figures/battery_frac2_big.png', 'big');


figure(20);
fig20_comps.fig = gcf;
hold on
fig20_comps.p1 = plot(results.T.time1min, PVRealPower(1:length(results.T.time1min),40));
fig20_comps.plotTitle = title('Real PV production');
hold off
fig20_comps.plotXLabel = xlabel('time [h]');
fig20_comps.plotYLabel = ylabel('Power [KW]');

%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig20_comps.p1, 'Color', PS.Red1, 'LineWidth', 2);

% ADJUST AXES PROPERTIES

% Change axis properties
ax = gca;
ax.YAxis.Limits = [0, 70];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig20_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig20_comps, 'Figures/pv_ll_small.png', 'small');
SAVE_MY_FIGURE(fig20_comps, 'Figures/pv_ll_big.png', 'big');

figure(21);
fig21_comps.fig = gcf;
hold on
fig21_comps.p1 = plot(results.T.time1min, loadRealPower(1:length(results.T.time1min),40));
fig21_comps.plotTitle = title('Real load demand');
hold off
fig21_comps.plotXLabel = xlabel('time [h]');
fig21_comps.plotYLabel = ylabel('Power [KW]');

%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig21_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);

% ADJUST AXES PROPERTIES

% Change axis properties
ax = gca;
ax.YAxis.Limits = [50, 90];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig21_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig21_comps, 'Figures/load_ll_small.png', 'small');
SAVE_MY_FIGURE(fig21_comps, 'Figures/load_ll_big.png', 'big');

figure(22);
fig22_comps.fig = gcf;
fig22_comps.p1 = plot(results.T.time1min,  results.LL.eta(1:end-1,1));

fig22_comps.plotTitle = title('Variation of network power $\delta P_{net}(t)$','Interpreter','latex');
hold off
fig22_comps.plotXLabel = xlabel('time [h]');
fig22_comps.plotYLabel = ylabel('Power [KW]');

%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig22_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig22_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig22_comps, 'Figures/delta_Pnet_small.png', 'small');
SAVE_MY_FIGURE(fig22_comps, 'Figures/delta_Pnet_big.png', 'big');


figure(23);
fig23_comps.fig = gcf;
hold on
fig23_comps.p1 = stairs(results.T.time15min, results.HL.etaReal);
fig23_comps.p2 = plot(results.T.time15min, -5*ones(1,96));
fig23_comps.p3 = plot(results.T.time15min, +5*ones(1,96));

fig23_comps.plotTitle = title('Variation in energy exchanged with the network');
hold off
fig23_comps.plotXLabel = xlabel('time [h]');
fig23_comps.plotYLabel = ylabel('Energy [KWh]');

%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig23_comps.p1, 'Color', PS.Blue1, 'LineWidth', 2);
set(fig23_comps.p2, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig23_comps.p3, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);


%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig23_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig23_comps, 'Figures/eta_small.png', 'small');
SAVE_MY_FIGURE(fig23_comps, 'Figures/eta_big.png', 'big');

shr = 15:-1:1; k = 0; ll_time = zeros(1,15);
for t = 1:1440
    if mod(t,15) == 1
        k = 1;
    end
    ll_time(1,k) = ll_time(k) + results.T.dthl(t);
    k = k+1;
end

figure(24);
fig24_comps.fig = gcf; 
fig24_comps.p1 = bar(shr, ll_time/96,'FaceColor',PS.Red2);
fig24_comps.plotTitle = title('LL computational time');
fig24_comps.plotXLabel = xlabel('$n_{Shr} [ ]$','Interpreter','latex');
fig24_comps.plotYLabel = ylabel('Time [s]');

%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig24_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig24_comps, 'Figures/comp_time_small.png', 'small');
SAVE_MY_FIGURE(fig24_comps, 'Figures/comp_time_big.png', 'big');

figure(25);
fig25_comps.fig = gcf;
fig25_comps.t1 = tiledlayout(2,2);
fig25_comps.n(1) = nexttile;
hold on
fig25_comps.p1 = plot(results.T.time1min(1:1440), results.BMS.BatterySoCA(1:1440,1));
fig25_comps.p2 = plot(results.T.time1min(1:1440), results.BMS.BatterySoCA(1:1440,2));
fig25_comps.p3 = plot(results.T.time1min(1:1440), results.BMS.BatterySoCA(1:1440,3));
fig25_comps.p4 = plot(results.T.time1min(1:1440), results.BMS.BatterySoCA(1:1440,4));
fig25_comps.p12 = plot(results.T.time1min(1:1440), 0.1*ones(1,1440));
fig25_comps.p13 = plot(results.T.time1min(1:1440), 0.9*ones(1,1440));
hold off
fig25_comps.plotTitle = title('Cluster A battery SoCs');
fig25_comps.plotXLabel = xlabel('Time [h]');
fig25_comps.plotYLabel = ylabel('SoC [ ]');
set(fig25_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig25_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
set(fig25_comps.p3, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig25_comps.p4, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig25_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig25_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [0.0, 1];

fig25_comps.n(2) = nexttile;
hold on
fig25_comps.p5 = plot(results.T.time1min(1:1440), results.BMS.BatterySoCB(1:1440,1));
fig25_comps.p6 = plot(results.T.time1min(1:1440), results.BMS.BatterySoCB(1:1440,2));
fig25_comps.p7 = plot(results.T.time1min(1:1440), results.BMS.BatterySoCB(1:1440,3));
fig25_comps.p8 = plot(results.T.time1min(1:1440), results.BMS.BatterySoCB(1:1440,4));
fig25_comps.p12 = plot(results.T.time1min(1:1440), 0.1*ones(1,1440));
fig25_comps.p13 = plot(results.T.time1min(1:1440), 0.9*ones(1,1440));
hold off

fig25_comps.plotTitle = title('Cluster B battery SoCs');
fig25_comps.plotXLabel = xlabel('Time [h]');
fig25_comps.plotYLabel = ylabel('SoC [ ]');
set(fig25_comps.p5, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig25_comps.p6, 'Color', PS.Green1, 'LineWidth', 2);
set(fig25_comps.p7, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig25_comps.p8, 'Color', PS.Orange2, 'LineWidth', 2);
set(fig25_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig25_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [0.0, 1];

fig25_comps.n(3) = nexttile;
hold on
fig25_comps.p9  = plot(results.T.time1min(1:1440), results.BMS.BatterySoCC(1:1440,1));
fig25_comps.p10 = plot(results.T.time1min(1:1440), results.BMS.BatterySoCC(1:1440,2));
fig25_comps.p12 = plot(results.T.time1min(1:1440), 0.1*ones(1,1440));
fig25_comps.p13 = plot(results.T.time1min(1:1440), 0.9*ones(1,1440));
hold off
fig25_comps.plotTitle = title('Cluster C battery SoCs');
fig25_comps.plotXLabel = xlabel('Time [h]');
fig25_comps.plotYLabel = ylabel('SoC [ ]');
set(fig25_comps.p9, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig25_comps.p10, 'Color', PS.Green1, 'LineWidth', 2);
set(fig25_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig25_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [0.0, 1];

fig25_comps.n(4) = nexttile;
hold on
fig25_comps.p11 = plot(results.T.time1min(1:1440), results.BMS.BatterySoCD(1:1440,1));
fig25_comps.p12 = plot(results.T.time1min(1:1440), 0.1*ones(1,1440));
fig25_comps.p13 = plot(results.T.time1min(1:1440), 0.9*ones(1,1440));
hold off
fig25_comps.plotTitle = title('Cluster D battery SoCs');
fig25_comps.plotXLabel = xlabel('Time [h]');
fig25_comps.plotYLabel = ylabel('SoC [ ]');
set(fig25_comps.p11, 'Color', PS.DBlue1, 'LineWidth', 2);
set(fig25_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig25_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% Change axis properties
ax = gca;
ax.YAxis.Limits = [0.0, 1];
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig25_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig25_comps, 'Figures/battery_soc_full_small.png', 'small');
SAVE_MY_FIGURE(fig25_comps, 'Figures/battery_soc_full_big.png', 'big');

figure(26);
fig26_comps.fig = gcf;
hold on
fig26_comps.p1 = stairs(results.T.time15min, sign(results.HL.HLBessPower(1:end-1,1)));

fig26_comps.p2 = plot(results.T.time15min, +1*ones(1,96));
fig26_comps.p3 = plot(results.T.time15min, -1*ones(1,96));
fig26_comps.p4 = plot(results.T.time15min, +zeros(1,96));

fig26_comps.plotTitle = title('BESS states');
hold off
fig26_comps.plotXLabel = xlabel('State []');

%========================================================
% SET PLOT PROPERTIES
% Choices for COLORS can be found in ColorPalette.png
set(fig26_comps.p1, 'Color', PS.Blue1, 'LineWidth', 2);
set(fig26_comps.p2, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig26_comps.p3, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig26_comps.p3, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);

ax = gca;
ax.YAxis.Limits = [-1.5, 1.5];
fig26_comps.plotLegend = legend([fig26_comps.p1, fig26_comps.p2, fig26_comps.p3, fig26_comps.p4], ...
'BESS state', 'DCH', 'Idle', 'CH', 'Interpreter', 'latex');
legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
set(fig26_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
    legendHeight], 'Box', 'on');
set(fig26_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
    'EdgeColor', PS.MyGrey1);
%========================================================
% INSTANTLY IMPROVE AESTHETICS-most important step
STANDARDIZE_FIGURE(fig26_comps);

%========================================================
% SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
SAVE_MY_FIGURE(fig26_comps, 'Figures/BESS_small.png', 'small');
SAVE_MY_FIGURE(fig26_comps, 'Figures/BESS_big.png', 'big');
