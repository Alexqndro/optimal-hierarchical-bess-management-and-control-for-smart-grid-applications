clearvars; clc;
%close all;
yalmip('clear')

addpath('utils');
addpath('utils\data');

load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');

%% Optimization variables
% Inputs

% Scheduled generator and network power
PGenBar       = sdpvar(LLWindows,1);
PNetBar       = sdpvar(LLWindows,1);

% Scheduled cluster state of charge and cluster fractions
clustSocBar   = sdpvar(LLWindows,nCluster);
clustFractCH  = sdpvar(LLWindows,nCluster);
clustFractDCH = sdpvar(LLWindows,nCluster);
bessState     = intvar(LLWindows,1);

% Cluster initial state of charge
clustSoc0     = sdpvar(1,nCluster);

% Outputs
% Generator and network power
genPower      = sdpvar(LLWindows,1);
netPower      = sdpvar(LLWindows,1);
duGen         = sdpvar(LLWindows,1);
duNet         = sdpvar(LLWindows,1);
bessPower     = sdpvar(LLWindows,nScenarios);

% delta Energy exchanged with the network
eta         = sdpvar(LLWindows+1,1);
% Slack variables
l           = sdpvar(1,1);

% Non dispachable power scenarios
nondispScenarios  = sdpvar(LLWindows,nScenarios,'full');

for i = 1:nScenarios
    % Soc and energy exchanged
    clustSoc{i}  = sdpvar(LLWindows+1,nCluster); %#ok<*SAGROW> 
end

F = [];
LLconstraints = [];
LLconstraints = [LLconstraints; genPower == duGen + PGenBar]; %#ok<*AGROW> 
LLconstraints = [LLconstraints; netPower == duNet + PNetBar]; 

LLconstraints = [LLconstraints; genPower  >= minGenPower];
LLconstraints = [LLconstraints; genPower  <= maxGenPower];
LLconstraints = [LLconstraints; netPower  <= maxNetPower];
LLconstraints = [LLconstraints; netPower  >= minNetPower];
LLconstraints = [LLconstraints; bessPower <= maxDCHBessPower];
LLconstraints = [LLconstraints; bessPower >= -maxCHBessPower];

% Energy difference
LLconstraints = [LLconstraints; eta(1) == 0];
for t = 1:LLWindows
    LLconstraints = [LLconstraints; eta(t+1) == eta(t) + duNet(t)];
end
LLconstraints = [LLconstraints; l >= 0];

for i = 1:nScenarios
    % Power balance
    LLconstraints = [LLconstraints; bessPower(:,i) + genPower + netPower == nondispScenarios(:,i)];

    for t = 1:LLWindows
        LLconstraints = [LLconstraints; implies(bessState(t,1) == 1,  bessPower(t,i) >= 0)];
        LLconstraints = [LLconstraints; implies(bessState(t,1) == -1,  bessPower(t,i) <= 0)];
        LLconstraints = [LLconstraints; bessState(t,1)>= -1];
        LLconstraints = [LLconstraints; bessState(t,1)<=1];
    end

    % Battery state of charge 
    LLconstraints = [LLconstraints; clustSoc{i}(1,:) == clustSoc0];
    for t = 1:LLWindows
         LLconstraints = [LLconstraints; clustSoc{i}(t+1,:) == clustSoc{i}(t,:) - LLSampling*Kbch.*clustFractCH(t,:)*bessPower(t,i) - LLSampling*Kbch.*clustFractDCH(t,:)*bessPower(t,i)];
    end
    
    LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) <= 0.90 + l];
    LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) >= 0.1 - l];

    % Cost function
    LLf(i) = 100*(duGen'*duGen) + 0.1*(duNet'*duNet) + 1e6*l^2 + 10*eta(end)^2 ...
           + 20*(clustSocBar(end,:) - clustSoc{i}(end,:))*(clustSocBar(end,:) - clustSoc{i}(end,:))'...
           + 1000*(netPower(1:end-1,:) - netPower(2:end,:))'*(netPower(1:end-1,:) - netPower(2:end,:));
    for cl = 1:nCluster
        LLf(i) = LLf(i) + 10*((clustSocBar(1:end-1,cl) - clustSoc{i}(2:end-1,cl))'*(clustSocBar(1:end-1,cl) - clustSoc{i}(2:end-1,cl)));
    end
    
    F = [F; LLf(i)];
end
LLcostfnc = sum(F)/nScenarios;
%% Optimization options
% Gurobi
% optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.BarHomogeneous',1,'gurobi.NumericFocus',2);
optimOptions = sdpsettings('solver','gurobi');

LLIn  = {nondispScenarios, PGenBar, PNetBar, clustSoc0, clustSocBar, clustFractCH, clustFractDCH, bessState};
LLOut = {l, eta, clustSoc{1}, bessPower, genPower, netPower};
MPC_LL_soc = optimizer(LLconstraints, LLcostfnc, optimOptions, LLIn, LLOut);

save('utils\MPC_optimizer\MPC_LL_Soc','MPC_LL_soc');