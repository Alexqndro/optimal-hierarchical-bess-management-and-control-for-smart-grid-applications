clearvars; clc; close all;
yalmip('clear')

%% Addpaths
addpath('utils\');
addpath('utils\data');
addpath('utils\MPC_optimizer');

%% Load parameters, scheduled power and optimizers
load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');
load('MPC_LL_alfa.mat');

%% Allocation of HL MPC optimizer (too complex to save and load)

run MPC_HL_def.m

%% Grid level nominal Power
GridOptimGen     = [GridGenPower; GridGenPower(1:HLWindows)];
GridOptimNet     = [GridNetPower; GridNetPower(1:HLWindows)];
clustOptimSoc    = [GridOptimSoc; GridOptimSoc(end,:).*ones(HLWindows,4)];
loadNomPower     = [loadNomPower; loadNomPower(1:HLWindows)];
PVNomPower       = [PVNomPower; PVNomPower(1:HLWindows)];

%% Scenario generation
nondispScenarios = zeros(length(time1Min),nScenarios+1);

%% Simulation
clusterSocSim    = zeros(length(time1Min),nCluster);
clusterSocSim(1,:)  = clusterSocSim0;
k = 0;

for t = 1:length(time1Min)
    if mod(t,LLWindows) == 1
        k = k+1;
        disp(k);

        clustSocHL0      = clusterSocSim(t,:);
        InHL             = {clustSocHL0, nondispNomPower(k:k+HLWindows-1), GridOptimGen(k:k+HLWindows-1), GridOptimNet(k:k+HLWindows-1)};
        
        tic
        OutHL            = MPC_HL(InHL);
        HLdt(k)          = toc;

        % Solution
        HLCost(k)            = OutHL{1}; %#ok<*SAGROW> 
        HLclustSoc(k,:)      = OutHL{2}(1,:);
        HLBessPower(k:k+1,1) = OutHL{3}(1:2);
        HLUCh(k:k+1,:)       = OutHL{4}(1:2,:);
        HLUDch(k:k+1,:)      = OutHL{5}(1:2,:);
        HLGenPower(k,1)      = OutHL{6}(1);
        HLNetPower(k,1)      = OutHL{7}(1);

        HLOptimSoc1Min((k-1)*LLWindows+1:k*LLWindows,:)  = OutHL{2}(2,:).*ones(LLWindows,4);
        HLBessPower1Min((k-1)*LLWindows+1:k*LLWindows,1) = OutHL{3}(1).*ones(LLWindows,1);
        HLGenPower1Min((k-1)*LLWindows+1:k*LLWindows,1)  = OutHL{6}(1).*ones(LLWindows,1);
        HLNetPower1Min((k-1)*LLWindows+1:k*LLWindows,1)  = OutHL{7}(1).*ones(LLWindows,1);

        HLOptimSoc1Min(k*LLWindows+1:(k+1)*LLWindows,:)  = OutHL{2}(3,:).*ones(LLWindows,4);
        HLBessPower1Min(k*LLWindows+1:(k+1)*LLWindows,1) = OutHL{3}(2).*ones(LLWindows,1);
        HLGenPower1Min(k*LLWindows+1:(k+1)*LLWindows,1)  = OutHL{6}(2).*ones(LLWindows,1);
        HLNetPower1Min(k*LLWindows+1:(k+1)*LLWindows,1)  = OutHL{7}(2).*ones(LLWindows,1);

    
        if abs(HLBessPower(k,1)) <= 1e-4
            clusterFractDCH(k,:) = zeros(1,4);
            clusterFractCH(k,:)  = zeros(1,4);
            clusterFract1MinCH((k-1)*LLWindows+1:k*LLWindows,:)  = zeros(LLWindows,4);
            clusterFract1MinDCH((k-1)*LLWindows+1:k*LLWindows,:) = zeros(LLWindows,4);
            bessState((k-1)*LLWindows+1:k*LLWindows,1) = zeros(LLWindows,1);
        else
            clusterFractDCH(k,:) = HLUDch(k,:)./HLBessPower(k);
            clusterFractCH(k,:)  = -HLUCh(k,:)./HLBessPower(k);
            clusterFract1MinCH((k-1)*LLWindows+1:k*LLWindows,:)  = - HLUCh(k,:)./HLBessPower(k).*ones(LLWindows,4);
            clusterFract1MinDCH((k-1)*LLWindows+1:k*LLWindows,:) =   HLUDch(k,:)./HLBessPower(k).*ones(LLWindows,4);
        end

        if abs(HLBessPower(k+1,1)) <= 1e-4
            clusterFractDCH(k+1,:) = zeros(1,4);
            clusterFractCH(k+1,:)  = zeros(1,4);
            bessState(k*LLWindows+1:(k+1)*LLWindows,1) = zeros(LLWindows,1);
            clusterFract1MinCH(k*LLWindows+1:(k+1)*LLWindows,:)  =   zeros(LLWindows,4);
            clusterFract1MinDCH(k*LLWindows+1:(k+1)*LLWindows,:) =   zeros(LLWindows,4);
        else
            clusterFractDCH(k+1,:) = HLUDch(k+1,:)./HLBessPower(k+1);
            clusterFractCH(k+1,:)  = -HLUCh(k+1,:)./HLBessPower(k+1);
            clusterFract1MinCH(k*LLWindows+1:(k+1)*LLWindows,:)  = - HLUCh(k+1,:)./HLBessPower(k+1).*ones(LLWindows,4);
            clusterFract1MinDCH(k*LLWindows+1:(k+1)*LLWindows,:) =   HLUDch(k+1,:)./HLBessPower(k+1).*ones(LLWindows,4);
        end

        clusterFractMPC(k,:) = clusterFractDCH(k,:) + clusterFractCH(k,:);
        clusterFractMPC(k+1,:) = clusterFractDCH(k+1,:) + clusterFractCH(k+1,:);
        
        if (HLBessPower(k) > 1e-4)
            bessState((k-1)*LLWindows+1:k*LLWindows,1) = ones(LLWindows,1);
        elseif(HLBessPower(k) < - 1e-4)
            bessState((k-1)*LLWindows+1:k*LLWindows,1) = -ones(LLWindows,1);
        end
        if (HLBessPower(k+1) > 1e-4)
            bessState(k*LLWindows+1:(k+1)*LLWindows,1) = ones(LLWindows,1);
        elseif(HLBessPower(k+1) < - 1e-4)
            bessState(k*LLWindows+1:(k+1)*LLWindows,1) = -ones(LLWindows,1);
        end
    end 

    loadDist     = zeros(LLWindows+1,nScenarios+1);
    PVDist       = zeros(LLWindows+1,nScenarios+1);
    scenarioLoad(t:t+LLWindows-1,:) = loadRealPower(t,end)*ones(LLWindows,nScenarios+1);
    scenarioPV(t:t+LLWindows-1,:)   = PVRealPower(t,end)*ones(LLWindows,nScenarios+1);    
    for h = 1:LLWindows
        scenarioLoad(t + h - 1,:)   = scenarioLoad(t + h - 1,:) + loadDist(h,:);
        loadDist(h + 1,:)           = rw.alfaLoad*loadDist(h,:) + rw.covLoad*randn(1,nScenarios+1);
        if(t1 <= t+h && t+h <= t2)
            scenarioPV(t + h - 1,:) = scenarioPV(t + h - 1,:) + PVDist(h,:);
            scenarioPV(t + h - 1,:) = scenarioPV(t + h  - 1,:).*(scenarioPV(t + h - 1,:)>=0);
            PVDist(h + 1,:)         = rw.alfaPV*PVDist(h,:) + rw.covPV*randn(1,nScenarios+1);
        else
            scenarioPV(t + h - 1,:) = zeros(1,nScenarios+1);
            PVDist(h + 1,:)         = zeros(1,nScenarios+1);
        end
    end
    nondispScenarios(t:t+LLWindows-1,:) = scenarioLoad(t:t+LLWindows-1,:) - scenarioPV(t:t+LLWindows-1,:);

    tic
    InLL = {nondispScenarios(t:t+LLWindows-1,1:end-1), HLGenPower1Min(t:t+LLWindows-1,1), HLNetPower1Min(t:t+LLWindows-1,1), HLBessPower1Min(t:t+LLWindows-1,1), ...
            clusterSocSim(t,:), HLOptimSoc1Min(t:t+LLWindows-1,:), clusterFract1MinCH(t:t+LLWindows-1,:), clusterFract1MinDCH(t:t+LLWindows-1,:), bessState(t:t+LLWindows-1,1)};
    [OutLL, LLflag] = MPC_LL_alfa(InLL);
    string = yalmiperror(LLflag);
    disp(string)
    LLdt(t) = toc;

    alfa(t,1)        = OutLL{1}(1);
    
    LLGenPower(t,1)  = OutLL{4}(1);
    LLBessPower(t,1) = HLBessPower1Min(t,1) + alfa(t,1)*(nondispScenarios(t,end) - LLGenPower(t,1) - HLNetPower1Min(t,1) - HLBessPower1Min(t,1));
    LLNetPower(t,1)  = HLNetPower1Min(t,1) + (1-alfa(t,1))*(nondispScenarios(t,end) - LLGenPower(t,1) - HLNetPower1Min(t,1) - HLBessPower1Min(t,1));
    eta(t,1)         = LLNetPower(t,1) - HLNetPower1Min(t,1);
    thetareal(t,1)   = abs(nondispScenarios(t,end) - LLGenPower(t,1) - LLBessPower(t,1) - LLNetPower(t,1));
    uCH_real(t,:)    = -clusterFractCH(k,:)*LLBessPower(t,1);
    uDCH_real(t,:)   = clusterFractDCH(k,:)*LLBessPower(t,1);
    
    clusterSocSim(t+1,:) = clusterSocSim(t,:) + LLSampling*Kbch.*uCH_real(t,:) - LLSampling*Kbdch.*uDCH_real(t,:);
end

%% Plot figures
plot_flag = true;
if plot_flag
    figure
    plot(time15Min, HLNetPower, time15Min, HLGenPower, time15Min, HLBessPower(1:length(time15Min)), time15Min, loadNomPower(1:length(time15Min)) - PVNomPower(1:length(time15Min)));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power scheduled by the HL MPC during the day')

    figure
    plot(time1Min, LLNetPower, time1Min, LLGenPower, time1Min, LLBessPower, time1Min, nondispScenarios(1:length(time1Min),nScenarios+1));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power exchange during the day')

    figure
    plot(time15Min, HLNetPower, time15Min, HLGenPower, time15Min, HLBessPower(1:length(time15Min)), time15Min, loadNomPower(1:length(time15Min)) - PVNomPower(1:length(time15Min)),time1Min, LLNetPower, time1Min, LLGenPower, time1Min, LLBessPower, time1Min, nondispScenarios(1:length(time1Min),nScenarios+1));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Power exchange during the day')

    figure
    plot(time1Min, clusterSocSim(1:end-1,:));
    xlabel('Time [h]');ylabel('SoC [ ]');
    legend('A','B','C','D')
    title('Cluster state of charge during the day')
    yline(0.9,'r')
    yline(0.1,'r')

    figure
    plot(time1Min, clusterSocSim(1:end-1,:),time15Min,HLclustSoc,'.');
    xlabel('Time [h]');ylabel('SoC [ ]');
    title('Cluster state of charge during the day')
    yline(0.9,'r')
    yline(0.1,'r')

    figure
    tiledlayout(2,1)
    nexttile
    plot(time15Min, clusterFractCH(1:end-1,:));
    xlabel('Time [h]');ylabel('Ci-CH [ ]');
    legend('A','B','C','D')
    ylim([-0.1;1.1])
    title('Cluster power fraction during the day: charge')
    nexttile
    plot(time15Min, clusterFractDCH(1:end-1,:));
    xlabel('Time [h]');ylabel('Ci-DCH [ ]');
    legend('A','B','C','D')
    title('Cluster power fraction during the day: discharge')
    ylim([-0.1;1.1])

    figure
    tiledlayout(2,2)
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),1), time1Min, uDCH_real(:,1));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharge power real vs HL: cluster - A')
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),2), time1Min, uDCH_real(:,2));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharge power real vs HL: cluster - B')
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),3), time1Min, uDCH_real(:,3));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharge power real vs HL: cluster - C')
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),4), time1Min, uDCH_real(:,4));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharge power real vs HL: cluster - D')

    figure
    tiledlayout(2,2)
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),1), time1Min, uCH_real(:,1));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Charge power real vs HL: cluster - A')
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),2), time1Min, uCH_real(:,2));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Charge power real vs HL: cluster - B')
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),3), time1Min, uCH_real(:,3));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Charge power real vs HL: cluster - C')
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),4), time1Min, uCH_real(:,4));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Charge power real vs HL: cluster - D')


    figure
    tiledlayout(2,1)
    nexttile
    stairs(time1Min,LLdt)
    yline(mean(LLdt),'r');
    legend('LL time', 'mean')
    xlabel('Time [h]');ylabel('Time [s]');
    title('LL MPC computational time')
    nexttile
    stairs(time15Min,HLdt)
    yline(mean(HLdt),'r');
    legend('HL time', 'mean')
    xlabel('Time [h]');ylabel('Time [s]');
    title('HL MPC computational time')

end