clearvars; clc;
%close all;
yalmip('clear')

addpath('utils');
addpath('utils\data');

load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');

%% Optimization variables
% Inputs

% Scheduled generator and network power
PGenBar       = sdpvar(LLWindows,1);
PNetBar       = sdpvar(LLWindows,1);
PBessBar      = sdpvar(LLWindows,1);

% Scheduled cluster state of charge and cluster fractions
clustSocBar   = sdpvar(LLWindows,nCluster);
clustFractCH  = sdpvar(LLWindows,nCluster);
clustFractDCH = sdpvar(LLWindows,nCluster);
bessState     = intvar(LLWindows,1);

% Cluster initial state of charge
clustSoc0     = sdpvar(1,nCluster);

% Bess power
bessPower     = sdpvar(LLWindows,nScenarios); %#ok<*SAGROW> 
duBess        = sdpvar(LLWindows,nScenarios); 

% Network power
netPower      = sdpvar(LLWindows,nScenarios);
duNet         = sdpvar(LLWindows,nScenarios);

% delta Energy exchanged with the network
eta           = sdpvar(LLWindows+1,nScenarios);
theta         = sdpvar(1,nScenarios);
% Non dispachable power scenarios
nondispScenarios  = sdpvar(LLWindows,nScenarios,'full');

for i = 1:nScenarios
    clustSoc{i}    = sdpvar(LLWindows+1,nCluster);
end

F = [];
LLconstraints = [];
% Operational constraints
LLconstraints = [LLconstraints; netPower  <= maxNetPower];
LLconstraints = [LLconstraints; netPower  >= minNetPower];
LLconstraints = [LLconstraints; bessPower <= maxDCHBessPower];
LLconstraints = [LLconstraints; bessPower >= -maxCHBessPower];

% Network energy balance
for i = 1:nScenarios
    LLconstraints = [LLconstraints; netPower(:,i) == duNet(:,i) + PNetBar]; %#ok<*AGROW>
    LLconstraints = [LLconstraints; bessPower(:,i) == duBess(:,i) + PBessBar]; %#ok<*AGROW> 
end
for i = 1:nScenarios-1
    LLconstraints = [LLconstraints; netPower(1,i) == netPower(1,i+1)]; %#ok<*AGROW>
    LLconstraints = [LLconstraints; bessPower(1,i) == bessPower(1,i+1)]; %#ok<*AGROW> 
end

% Energy difference
for i = 1:nScenarios
    LLconstraints = [LLconstraints; eta(1,i) == 0];
    for t = 1:LLWindows
        LLconstraints = [LLconstraints; eta(t+1,i) == eta(t,i) + duNet(t,i)];
    end
end

for i = 1:nScenarios
    % Power balance
    LLconstraints = [LLconstraints; PGenBar + bessPower(:,i) + netPower(:,i) == nondispScenarios(:,i)];
    LLconstraints = [LLconstraints; abs(PGenBar + bessPower(:,i) + netPower(:,i) - nondispScenarios(:,i)) <= theta(1,i)];

    % Network operational power
    for t = 1:LLWindows
        LLconstraints = [LLconstraints; implies(bessState(t,1) == 1,  bessPower(t,i) >= 0)];
        LLconstraints = [LLconstraints; implies(bessState(t,1) == 0,  bessPower(t,i) == 0)];
        LLconstraints = [LLconstraints; implies(bessState(t,1) == -1, bessPower(t,i) <= 0)];
        LLconstraints = [LLconstraints; bessState(t,1) >= -1];
        LLconstraints = [LLconstraints; bessState(t,1) <= 1];
    end
    % Battery state of charge 
    LLconstraints = [LLconstraints; clustSoc{i}(1,:) == clustSoc0];
    for t = 1:LLWindows
         LLconstraints = [LLconstraints; clustSoc{i}(t+1,:) == clustSoc{i}(t,:) - LLSampling*Kbch.*clustFractCH(t,:)*bessPower(t,i) - LLSampling*Kbch.*clustFractDCH(t,:)*bessPower(t,i)];
    end

    LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) <= 0.90];
    LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) >= 0.1];
   
    % Cost function
    LLf(i) = 1*duNet(:,i)'*duNet(:,i) + 0.01*duBess(:,i)'*duBess(:,i) + 1e6*theta(1,i)^2 + 10*eta(end,i)^2 + 1*(clustSocBar(end,:) - clustSoc{i}(end,:))*(clustSocBar(end,:) - clustSoc{i}(end,:))';
    for cl = 1:nCluster
        LLf(i) = LLf(i) + 1*((clustSocBar(1:end-1,cl) - clustSoc{i}(2:end-1,cl))'*(clustSocBar(1:end-1,cl) - clustSoc{i}(2:end-1,cl)));
    end
    F = [F; LLf(i)];
end
LLcostfnc = sum(F)/nScenarios;
%% Optimization options
% Gurobi
optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.BarHomogeneous',1,'gurobi.NumericFocus',2);

LLIn  = {nondispScenarios, PGenBar, PNetBar, PBessBar, clustSoc0, clustSocBar, clustFractCH, clustFractDCH, bessState};
LLOut = {theta, eta, clustSoc{1}, bessPower, netPower};
MPC_LL_theta = optimizer(LLconstraints, LLcostfnc, optimOptions, LLIn, LLOut);

save('utils\MPC_optimizer\MPC_LL_theta','MPC_LL_theta');