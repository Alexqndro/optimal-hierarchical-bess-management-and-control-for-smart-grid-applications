clearvars; clc;
%close all;

addpath('utils');
addpath('utils\data');

load('parameters.mat');
%% Cluster A BMS
yalmip('clear')
% Inputs
% Scheduled BESS power
PBessBar      = sdpvar(1,1);
% Cluster fraction
C_CH          = sdpvar(1,1);
C_DCH         = sdpvar(1,1);

% Battery soc0
SoCA0         = sdpvar(1,A.nBattery,'full');
SoCA          = sdpvar(4,A.nBattery,'full');

% Battery fractions
piA            = sdpvar(3,A.nBattery,'full');

BMSconstraints = []; %#ok<*NASGU> 
BMSconstraints = [BMSconstraints; piA >= 0];
BMSconstraints = [BMSconstraints; sum(piA,2) == 1];
BMSconstraints = [BMSconstraints; SoCA(1,:) == SoCA0];

for t = 1:3
    BMSconstraints = [BMSconstraints; SoCA(t+1,:) == SoCA(t,:) - piA(t,:).*A.muCharge/A.rateCapacity*5/60*C_CH*PBessBar -  piA(t,:).*A.muDischarge/A.rateCapacity*5/60*C_DCH*PBessBar];
end

% Cost function
BMS_costfnc = 0;
h = 1;
for t = 2:4
    Lb{t} = [];%#ok<*SAGROW> 
    for j = 1:A.nBattery-1
        for k = 1:A.nBattery-h
            battError = SoCA(t,j) - SoCA(t,k + h); 
            Lb{t} = [Lb{t}, battError];
        end
        h = h + 1;
    end
    h = 1;
    BMS_costfnc = BMS_costfnc + 1e3*norm(Lb{t},inf);
end
%% Optimization options
% Gurobi
optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.BarHomogeneous',0,'gurobi.NumericFocus',3,'gurobi.NonConvex',1);
%optimOptions = sdpsettings('solver','mosek','verbose',2);
BMSIn  = {PBessBar, C_CH, C_DCH, SoCA0};
BMSOut = {piA, SoCA};
BMS_MPC_A = optimizer(BMSconstraints, BMS_costfnc, optimOptions, BMSIn, BMSOut);
save('utils\MPC_optimizer\BMS_MPC_A','BMS_MPC_A');

%% Cluster B BMS
yalmip('clear')
%% Optimization variables
% Inputs
% Scheduled BESS power
PBessBar      = sdpvar(1,1);
% Cluster fraction
C_CH          = sdpvar(1,1);
C_DCH         = sdpvar(1,1);

% Battery soc0
SoCB0         = sdpvar(1,B.nBattery,'full');
SoCB          = sdpvar(4,B.nBattery,'full');

% Battery fractions
piB            = sdpvar(3,B.nBattery,'full');

BMSconstraints = []; %#ok<*NASGU> 
BMSconstraints = [BMSconstraints; piB >= 0];
BMSconstraints = [BMSconstraints; sum(piB,2) == 1];
BMSconstraints = [BMSconstraints; SoCB(1,:) == SoCB0];

for t = 1:3
    BMSconstraints = [BMSconstraints; SoCB(t+1,:) == SoCB(t,:) - piB(t,:).*B.muCharge/B.rateCapacity*5/60*C_CH*PBessBar -  piB(t,:).*B.muDischarge/B.rateCapacity*5/60*C_DCH*PBessBar];
end

% Cost function
BMS_costfnc = 0;
h = 1;
for t = 2:4
    Lb{t} = [];%#ok<*SAGROW> 
    for j = 1:B.nBattery-1
        for k = 1:B.nBattery-h
            battError = SoCB(t,j) - SoCB(t,k + h); 
            Lb{t} = [Lb{t}, battError];
        end
        h = h + 1;
    end
    h = 1;
    BMS_costfnc = BMS_costfnc + 1e3*norm(Lb{t},inf);
end
%% Optimization options
% Gurobi
optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.BarHomogeneous',0,'gurobi.NumericFocus',3,'gurobi.NonConvex',1);
%optimOptions = sdpsettings('solver','mosek','verbose',2);
BMSIn  = {PBessBar, C_CH, C_DCH, SoCB0};
BMSOut = {piB, SoCB};
BMS_MPC_B = optimizer(BMSconstraints, BMS_costfnc, optimOptions, BMSIn, BMSOut);
save('utils\MPC_optimizer\BMS_MPC_B','BMS_MPC_B');
%% Cluster B BMS
yalmip('clear')
%% Optimization variables
% Inputs
% Scheduled BESS power
PBessBar      = sdpvar(1,1);
% Cluster fraction
C_CH          = sdpvar(1,1);
C_DCH         = sdpvar(1,1);

% Battery soc0
SoCC0         = sdpvar(1,C.nBattery,'full');
SoCC          = sdpvar(4,C.nBattery,'full');

% Battery fractions
piC            = sdpvar(3,C.nBattery,'full');

BMSconstraints = []; %#ok<*NASGU> 
BMSconstraints = [BMSconstraints; piC >= 0];
BMSconstraints = [BMSconstraints; sum(piC,2) == 1];
BMSconstraints = [BMSconstraints; SoCC(1,:) == SoCC0];

for t = 1:3
    BMSconstraints = [BMSconstraints; SoCC(t+1,:) == SoCC(t,:) - piC(t,:).*C.muCharge/C.rateCapacity*5/60*C_CH*PBessBar -  piC(t,:).*C.muDischarge/C.rateCapacity*5/60*C_DCH*PBessBar];
end

% Cost function
BMS_costfnc = 0;
h = 1;
for t = 2:4
    Lb{t} = [];%#ok<*SAGROW> 
    for j = 1:C.nBattery-1
        for k = 1:C.nBattery-h
            battError = SoCC(t,j) - SoCC(t,k + h); 
            Lb{t} = [Lb{t}, battError];
        end
        h = h + 1;
    end
    h = 1;
    BMS_costfnc = BMS_costfnc + 1e3*norm(Lb{t},inf);
end
%% Optimization options
% Gurobi
optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.BarHomogeneous',0,'gurobi.NumericFocus',3,'gurobi.NonConvex',1);
%optimOptions = sdpsettings('solver','mosek','verbose',2);
BMSIn  = {PBessBar, C_CH, C_DCH, SoCC0};
BMSOut = {piC, SoCC};
BMS_MPC_C = optimizer(BMSconstraints, BMS_costfnc, optimOptions, BMSIn, BMSOut);
save('utils\MPC_optimizer\BMS_MPC_C','BMS_MPC_C');