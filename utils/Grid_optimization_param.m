%% Parameters: Run a script that allocate all the necessary parameters
clearvars; close all; clc;
addpath('data');

%% Grid optimization time and forecasting parameters
gridSampling   = 15/60;                       % Grid optimization sampling time  [h]
gridTimePeriod = 24;                          % Grid optimization period         [h]
gridWindows    = gridTimePeriod/gridSampling; % Grid optimization window         [ ]

%% Higher level sMPC
HLSampling     = 15/60;                       % HL sMPC sampling time            [h]
HLTimePeriod   = 6;                           % HL sMPC time period              [h]
HLWindows      = HLTimePeriod/HLSampling;     % HL sMPC optimization window      [ ]

%% Lower level sMPC
LLSampling     = 1/60;                        % LL sMPC sampling time            [h]
LLTimePeriod   = 15/60;                       % LL sMPC time period              [h]
LLWindows      = LLTimePeriod/LLSampling;     % LL sMPC optimization window      [ ]

%% Time vectors
time1Min       = 0:LLSampling:gridTimePeriod - LLSampling;
time15Min      = 0:gridSampling:gridTimePeriod - HLSampling;

%% Bess system
epsilon        = 0.05;                        % Violation probability
rho1           = 1;                           % Support rank for additive disturbance -> rho1 = m (number of input)
nScenarios     = rho1/epsilon - 1;            % Number of scenarios: E[vk | xt ] <= rho1/(N+1) = eps 
nScenarios     = 10;
%% Clusters
nCluster       = 4;                     % Number of clusters                  [ ]

% Modelling parameters:
% Cluster A
A.nBattery     = 4;                     % Number of batteries in cluster A    [ ]
A.muCharge     = 0.90;                  % Battery efficiency during charge    [ ]
A.muDischarge  = 0.80;                  % Battery efficiency during discharge [ ]
A.rateCapacity = 10;                    % Battery rated capacity              [KWh]

% Cluster B
B.nBattery     = 4;                     % Number of batteries in cluster A    [ ]
B.muCharge     = 0.80;                  % Battery efficiency during charge    [ ]
B.muDischarge  = 0.75;                  % Battery efficiency during discharge [ ]
B.rateCapacity = 8;                     % Battery rated capacity              [KWh]

% Cluster C
C.nBattery     = 2;                     % Number of batteries in cluster A    [ ]
C.muCharge     = 0.95;                  % Battery efficiency during charge    [ ]
C.muDischarge  = 0.85;                  % Battery efficiency during discharge [ ]
C.rateCapacity = 15;                    % Battery rated capacity              [KWh]
 
% Cluster D
D.nBattery     = 1;                     % Number of batteries in cluster A    [ ]
D.muCharge     = 0.95;                  % Battery efficiency during charge    [ ]
D.muDischarge  = 0.85;                  % Battery efficiency during discharge [ ]
D.rateCapacity = 50;                    % Battery rated capacity              [KWh]

clusters.A = A;
clusters.B = B;
clusters.C = C;
clusters.D = D;

bessCapacity =    A.rateCapacity*A.nBattery + B.rateCapacity*B.nBattery ...
    + C.rateCapacity*C.nBattery + D.rateCapacity*D.nBattery;
fractA = A.rateCapacity*A.nBattery/bessCapacity;
fractB = B.rateCapacity*B.nBattery/bessCapacity;
fractC = C.rateCapacity*C.nBattery/bessCapacity;
fractD = D.rateCapacity*D.nBattery/bessCapacity;
fract  = [fractA, fractB, fractC, fractD];

KAch  = A.muCharge/(A.rateCapacity*A.nBattery);
KBch  = B.muCharge/(B.rateCapacity*B.nBattery);
KCch  = C.muCharge/(C.rateCapacity*C.nBattery);
KDch  = D.muCharge/(D.rateCapacity*D.nBattery);

Kbch  = [KAch, KBch ,KCch, KDch];

KAdch = A.muDischarge/(A.rateCapacity*A.nBattery);
KBdch = B.muDischarge/(B.rateCapacity*B.nBattery);
KCdch = C.muDischarge/(C.rateCapacity*C.nBattery);
KDdch = D.muDischarge/(D.rateCapacity*D.nBattery);

Kbdch = [KAdch, KBdch, KCdch, KDdch];
clusterSocSim0 = [0.3, 0.5, 0.6, 0.4];

maxCHBessPower   = 150;                     % Maximum bess Power                 [Kw]
maxDCHBessPower  = 150;                     % Minimum bess Power                 [Kw]

maxCHclustPower  = maxCHBessPower*fract;   % Maximum bess Power                 [Kw]
maxDCHclustPower = maxDCHBessPower*fract;  % Minimum bess Power                 [Kw]

clear('KAch','KBch','KCch','KDch');
clear('KAdch','KBdch','KCdch','KDdch');

%% Bess system


%% Generator
nomGenPower    = 85;                       % Generator nominal power             [Kw]
maxGenPower    = 1.5*nomGenPower;          % Generator maximum power 120% of the nominal power
minGenPower    = 0.5*nomGenPower;          % Generator minimum power 80% of the nominal power
gc             = 0.3;

cgmin          = 1.1;
cgnom          = 1.0;
cgmax          = 1.2;
kgen           = polyfit([minGenPower,nomGenPower,maxGenPower],[cgmin,cgnom,cgmax],2);
k1_gen         = kgen(1); k2_gen = kgen(2); k3_gen = kgen(3);

P_genspan      = minGenPower:0.5:maxGenPower;
Q_gen          = k1_gen.*P_genspan.^2 + k2_gen*P_genspan + k3_gen;
C_gen          = gc*Q_gen.*P_genspan;

p              = polyfit(P_genspan',C_gen',2);
p1             = p(1);
p2             = p(2);
p3             = p(3);
C_approx       = p1.*P_genspan.^2 + p2.*P_genspan + p3.*ones(size(P_genspan));

%% Network
etaMax         =  5;                       % Network maximum energy unbalances     [KWh]
maxNetPower    =  50;                      % Network maximum power that we can buy [Kw]
minNetPower    = -75;                      % Network minimum power that we can sell[Kw]
minSpinReserve =  50;                      % Network minimum spin reserves         [Kw]

%% Load and PV stochastic parameters
wn.covLoad     = 0.5;                      % Load white noise covariance         [Kw^2]
rw.covLoad     = 0.5;                      % Load white noise covariance         [Kw^2]
rw.alfaLoad    = 0.95;                     % Load autocorrelation coeff          [ ]

wn.covPV       = 0.5;                        % PV white noise covariance           [Kw^2]
rw.covPV       = 1.0;                      % PV white noise covariance           [Kw^2]
rw.alfaPV      = 0.98;                     % PV autocorrelation coeff            [ ]
t1 = 5/LLSampling; t2 = 20/LLSampling;

%% Costs
nb             = 1;                        % Network power acquisition cost      [€/Kw]
ns             = 0.50;                     % Network power selling cost          [€/Kw]
% gc             = 0.3;                    % Generator operational cost          [€/Kw]
bc             = 0.05;                     % Bess operational cost               [€/Kw]
rc             = 0.05;                     % Spinning reserves cost              [€/Kw]
T1 = 16/gridSampling; T2 = 22/gridSampling;

nbuy           = nb*ones(size(time15Min));
nsell          = ns*ones(size(time15Min));nsell(T1:T2)  = 0.7;
gcost          = gc*ones(size(time15Min));
bcost          = bc*ones(size(time15Min));
rcost          = rc*ones(size(time15Min));

T1 = 15/gridSampling; T2 = 24/gridSampling;
nbuy_HL        = nb*ones(size(time15Min));
nsell_HL       = ns*ones(size(time15Min));nsell_HL(T1:T2)  = 0.8;
costs          = [nbuy', nsell', gcost', bcost',rcost'];

%% Cost figure
plot_flag = true;

if plot_flag
    figure %#ok<UNRCH>
    plot(time15Min, nbuy, time15Min, nsell, time15Min, nbuy_HL, time15Min, nsell_HL);
    xlabel('Time [h]');ylabel('Costs [€/Kwh]');
    legend('Buying price', 'Selling price','Updated buying price', 'Updated selling price');
    title('Power costs and price');
    ylim([0,1])

    figure
    tiledlayout(3,1)
    nexttile
    plot(P_genspan, Q_gen);
    xlabel('Power [kW]');ylabel('Costs [€/J]');
    nexttile
    plot(P_genspan, C_gen);
    xlabel('Power [kW]');ylabel('Costs [€/kW]');
    nexttile
    plot(P_genspan, C_approx,'-',P_genspan, C_gen)
    xlabel('Power [kW]');ylabel('Costs [€/kW]');


%     P = minGenPower:1:maxGenPower;
%     figure
%     plot(P,agen*P.^2+bgen*P+cgen)
%     xlabel('Power [KW]');ylabel('Costs [€]');
%     title('Generator costs based on operational point');
end

%% Save
clear('nb','ns','gc', 'bc')
clear('T1','T2','plot_flag', 'bc')

save('data\parameters.mat')