clearvars; close all; clc
%% Non dispachable power generation
addpath('nondispatchable_power');
addpath('data');
load parameters.mat

pv_type   = 'italy';
load_type = 'italy';
dist_type = 'rw';

%% Photovoltaic panel power production nominal
if strcmp(pv_type,'italy')
    t1 = 5/LLSampling; t2 = 20/LLSampling;
    load italy_PV_1-10_7_22.mat;
    italyPV      = table2array(italyPVproduction(:,:));     x = 0:1:23;
    PVNomPower   = 5*interp1(x,mean(italyPV,2),time15Min,'linear','extrap')'/1000;
else
    t1 = 6/LLSampling; t2 = 18/LLSampling;
    T1 = 6/HLSampling; T2 = 18/HLSampling;
    a = -1; b = 24; c = -108;
    PVNomPower = a*time15Min.^2 + b*time15Min + c*ones(size(time15Min));
    PVNomPower = 2*PVNomPower';
    PVNomPower(1:T1)   = 0;
    PVNomPower(T2:end) = 0;
end

%% Load power consumption nominal
if strcmp(load_type,'ramp')
    T1 = 2/HLSampling; T2 = 5/HLSampling; T3 = 8/HLSampling; T4 = 10/HLSampling; T5 = 13/HLSampling; T6 = 18/HLSampling; T7 = 20/HLSampling; T8 = 22/HLSampling; T9 = 24/HLSampling;
    loadNomPower          = zeros(length(time15Min),1);
    for t = 1:T1-1
        loadNomPower(t) = 30 - t*10/(T1-1);
    end
    loadNomPower(T1:T2) = 20;
    for t = 1: T3-T2
        loadNomPower(T2+t) = loadNomPower(T2) + t*60/(T3-T2);
    end
    loadNomPower(T3:T4) = 80;
    for t = 1: T5-T4
        loadNomPower(T4+t) = loadNomPower(T4) - t*30/(T5-T4);
    end
    loadNomPower(T5:T6) = 50;
    for t = 1: T7 - T6
        loadNomPower(T6+t) = loadNomPower(T6) + t*20/(T7-T6);
    end
    loadNomPower(T7:T8) = 70;
    for t = 1: T9 - T8
        loadNomPower(T8+t) = loadNomPower(T8) - t*40/(T9-T8);
    end

elseif strcmp(load_type,'building')
    x = 0:1:23;
    y = [30;27;25;27;30;35;42;50;60;72;75;72;68;65;62;59;55;63;65;63;60;55;50;43;];
    loadNomPower = interp1(x,y,time15Min,'spline','extrap')';

elseif strcmp(load_type,'italy')
    load italy_load_1-10_7_22.mat;
    italyLoad = table2array(italyloadprofile(:,:));
    loadNomPower = 2*mean(italyLoad,2)/1000;
end

%% Real scenarios
if strcmp(dist_type,'wn')
    PVNomPower    = [PVNomPower; PVNomPower(1:HLWindows)];
    for j = 1:nScenarios+1
        PVRealPower(:,j)   = interp(PVNomPower,15)';  %#ok<*SAGROW,*AGROW>
        PVDisturbance(:,j) = wn.covPV*randn(length(PVRealPower),1);
        for t = 1:length(PVRealPower(:,j))
            if t1<=t && t<=t2
                PVRealPower(t,j) = PVRealPower(t,j) + PVDisturbance(t,j);
            end
            if PVRealPower(t,j) < 0
                PVRealPower(t,j) = 0;
            end
        end
    end

    loadNomPower    = [loadNomPower; loadNomPower(1:HLWindows)];
    for j = 1:nScenarios+1
        loadRealPower(:,j)   = interp(loadNomPower,15)';
        loadDisturbance(:,j) = wn.covLoad*randn(length(loadRealPower),1);
        for t = 1:length(loadRealPower(:,j))
            loadRealPower(t,j) = loadRealPower(t,j) + loadDisturbance(t,j);
        end
    end

elseif strcmp(dist_type,'rw')
    PVNomPower    = [PVNomPower; PVNomPower(1:HLWindows)]; k = 0;
    for j = 1:nScenarios+1
        PVRealPower(:,j)   = interp(PVNomPower,15)';
        PVDisturbance(:,j) = zeros(LLWindows+1,1);
        for t = 1:length(PVRealPower(:,j))
            if mod(t,15)==1
                PVDisturbance(:,j) =  zeros(LLWindows+1,1);
                k = 1;
            end
            if t1<=t && t<=t2
                PVRealPower(t,j) = PVRealPower(t,j) + PVDisturbance(k ,j);
                PVDisturbance(k+1,j) = rw.alfaPV*PVDisturbance(k,j) + rw.covPV*randn(1);
                k = k+1;
            end
            if PVRealPower(t,j) < 0
                PVRealPower(t,j) = 0;
            end
        end
    end

    loadNomPower    = [loadNomPower; loadNomPower(1:HLWindows)]; k = 0;
    for j = 1:nScenarios+1
        loadRealPower(:,j)   = interp(loadNomPower,15);
        loadDisturbance(:,j) = zeros(LLWindows+1,1);
        for t = 1:length(loadRealPower(:,j))
            if mod(t,15)==1
                loadDisturbance(:,j) =  zeros(LLWindows+1,1);
                k = 1;
            end
            loadRealPower(t,j) = loadRealPower(t,j) + loadDisturbance(k,j);
            loadDisturbance(k+1,j) = rw.alfaLoad*loadDisturbance(k,j) + rw.covLoad*randn(1);
            k = k + 1;
        end
    end    
else
    error('Chose Type = wn or Type = rw');
end

%% Plot
PLOT = true;
if PLOT
    figure %#ok<*UNRCH>
    plot(time15Min, loadNomPower(1:length(time15Min)));
    xlabel('Time [h]'); ylabel('Power [Kw]');
    title('Load nominal power demand');

    figure
    plot(time15Min, PVNomPower(1:length(time15Min)));
    xlabel('Time [h]'); ylabel('Power [Kw]');
    title('PV nominal power generation');

    figure
    plot(time15Min, loadNomPower(1:length(time15Min)) - PVNomPower(1:length(time15Min)),time15Min, nomGenPower*ones(length(time15Min)),'g');
    yline(0,'r')
    legend('non dispachable sources','nominal generator power')
    xlabel('Time [h]'); ylabel('Power [Kw]');    xlabel('Time [h]'); ylabel('Power [Kw]');
    title('Nominal net-demand');

    figure
    stairs(time1Min, loadRealPower(1:length(time1Min),1));
    xlabel('Time [h]'); ylabel('Power [Kw]');
    title('Load Real power demand');

    loadRealMean = mean(loadRealPower(1:length(time1Min),:),2);
    curve1       = loadRealMean + 3*rw.covLoad;
    curve2       = loadRealMean - 3*rw.covLoad;

    figure
    plot(time1Min, curve1, 'LineWidth', 1);
    hold on;
    plot(time1Min, curve2, 'LineWidth', 1);
    shade(time1Min,curve1,time1Min,curve2,'FillType',[1 2]);
    plot(time1Min, loadRealMean(1:length(time1Min),1));
    xlabel('Time [h]'); ylabel('Power [Kw]');
    title('Load Real power demand');
    hold off

    figure
    plot(time1Min, PVRealPower(1:length(time1Min),1));
    xlabel('Time [h]'); ylabel('Power [Kw]');
    title('PV real power generation');

    PVRealMean   = mean(PVRealPower(1:length(time1Min),:),2);
    curve1       = PVRealMean + 3*rw.covPV;
    curve2       = PVRealMean - 3*rw.covPV;
    curve2       = (curve2 >= 0).*curve2;
    figure
    plot(time1Min, curve1, 'LineWidth', 1);
    hold on;
    plot(time1Min, curve2, 'LineWidth', 1);
    shade(time1Min,curve1,time1Min,curve2,'FillType',[1 2]);
    plot(time1Min, PVRealMean(1:length(time1Min),1));
    xlabel('Time [h]'); ylabel('Power [Kw]');
    title('Load Real power demand');
    hold off

    figure
    plot(time1Min, PVRealPower(1:length(time1Min),1));
    xlabel('Time [h]'); ylabel('Power [Kw]');
    title('PV real power generation');

    figure
    plot(time1Min, loadRealPower(1:length(time1Min),1) - PVRealPower(1:length(time1Min),1));
    yline(nomGenPower,'g')
    yline(0,'r')
    legend('non dispachable sources','nominal generator power')
    xlabel('Time [h]'); ylabel('Power [Kw]');
    title('Real net-demand');
end

%% Save
nondispNomPower   = loadNomPower - PVNomPower;
nondispNomGrid    = loadNomPower - 0.9*PVNomPower;
nondispRealPower  = loadRealPower - PVRealPower;
nondispMean       = mean(nondispRealPower(1:length(time1Min)),1)'; k = 1;
for i = 1:length(time15Min)
    nondisp15Mean(i,1) = mean(nondispMean(k:k+LLWindows-1));
    k = k+LLWindows;
end
save('data\nominalNondisp.mat','loadNomPower','PVNomPower','nondispNomPower','nondisp15Mean','nondispNomGrid');
save('data\realNondisp.mat','loadRealPower','PVRealPower','nondispRealPower','nondispMean');