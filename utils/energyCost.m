function [cf,Cf] = energyCost(powers, costs)

cf    = zeros(1,length(powers));
nbuy  = costs(:,1); pnet  = powers(:,1);
nsell = costs(:,2);
gcost = costs(:,3); pgen  = powers(:,2);
bcost = costs(:,4); pbess = powers(:,3);

for t = 1:length(powers)
    cf(t) = nbuy(t)*max(pnet(t),0) + nsell(t)*min(pnet(t),0) + gcost(t)*pgen(t) + bcost(t)*pbess(t);
end
Cf = sum(cf); 

end
