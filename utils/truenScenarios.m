function nScenarios = truenScenarios(epsilon, beta, decision)
n1 = decision;
n2 = ceil(2/epsilon*(decision-1+log(1/beta)));

while n2 - n1 > 1
    nScenarios = floor((n1+n2)/2);
    if betainc(1-epsilon, nScenarios - decision + 1, decision)> beta
        n1 = nScenarios;
    else
        n2 = nScenarios;
    end
end
nScenarios = n2;

end
