clearvars; clc; close all;
yalmip('clear')

%% Addpaths
addpath('utils\');
addpath('utils\data');
addpath('utils\MPC_optimizer');

%% Load parameters, scheduled power and optimizers
load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');
load('MPC_LL_alfa_dch.mat');
load('MPC_LL_alfa_ch.mat');
load('MPC_LL_alfa_off.mat');
load('BMS_MPC_A.mat');
load('BMS_MPC_B.mat');
load('BMS_MPC_C.mat');

%% Allocation of HL MPC optimizer (too complex to save and load)

run MPC_HL_def.m

for l = 1:20

    %% Grid level nominal Power
    GridOptimGen     = [GridGenPower; GridGenPower(1:HLWindows)];
    GridOptimNet     = [GridNetPower; GridNetPower(1:HLWindows)];
    clustOptimSoc    = [GridOptimSoc; GridOptimSoc(end,:).*ones(HLWindows,4)];
    loadNomPower     = [loadNomPower; loadNomPower(1:HLWindows)];
    PVNomPower       = [PVNomPower; PVNomPower(1:HLWindows)];
    
    %% Scenario generation
    nondispScenarios = zeros(length(time1Min),nScenarios+1);
    
    %% Simulation
    clusterSocSim       = zeros(length(time1Min),nCluster);
    clusterSocSimi      = zeros(length(time1Min),nCluster);
    
    clusterSocSim(1,:)  = clusterSocSim0;
    BatterySoCASim(1,:) = clusterSocSim0(1,1) + 0.05*randn(1,A.nBattery);
    BatterySoCBSim(1,:) = clusterSocSim0(1,2) + 0.05*randn(1,B.nBattery);
    BatterySoCCSim(1,:) = clusterSocSim0(1,3) + 0.05*randn(1,C.nBattery);
    BatterySoCDSim(1,:) = clusterSocSim0(1,4);
    clusterSocSimi(1,:)  = [mean(BatterySoCASim(1,:)),mean(BatterySoCBSim(1,:)),mean(BatterySoCCSim(1,:)),BatterySoCDSim(1,:)];
    
    k = 0; eta = 0; p = 0;
    
    for t = 1:length(time1Min)
        if mod(t,LLWindows) == 1
            k = k+1;
            disp(k);
    
            clustSocHL0      = clusterSocSimi(t,:);
            InHL             = {clustSocHL0, nondispNomPower(k:k+HLWindows-1), GridOptimGen(k:k+HLWindows-1), GridOptimNet(k:k+HLWindows-1)};
            
            tic
            OutHL            = MPC_HL(InHL);
            HLdt(k)          = toc;
    
            % Solution
            HLCost(k)            = OutHL{1}; %#ok<*SAGROW> 
            HLclustSoc(k,:)      = OutHL{2}(1,:);
            HLBessPower(k:k+1,1) = OutHL{3}(1:2);
            HLUCh(k:k+1,:)       = OutHL{4}(1:2,:);
            HLUDch(k:k+1,:)      = OutHL{5}(1:2,:);
            HLGenPower(k,1)      = OutHL{6}(1);
            HLNetPower(k,1)      = OutHL{7}(1);
    
            HLOptimSoc1Min((k-1)*LLWindows+1:k*LLWindows,:)  = OutHL{2}(2,:).*ones(LLWindows,4);
            HLBessPower1Min((k-1)*LLWindows+1:k*LLWindows,1) = OutHL{3}(1).*ones(LLWindows,1);
            HLGenPower1Min((k-1)*LLWindows+1:k*LLWindows,1)  = OutHL{6}(1).*ones(LLWindows,1);
            HLNetPower1Min((k-1)*LLWindows+1:k*LLWindows,1)  = OutHL{7}(1).*ones(LLWindows,1);
    
            HLOptimSoc1Min(k*LLWindows+1:(k+1)*LLWindows,:)  = OutHL{2}(3,:).*ones(LLWindows,4);
            HLBessPower1Min(k*LLWindows+1:(k+1)*LLWindows,1) = OutHL{3}(2).*ones(LLWindows,1);
            HLGenPower1Min(k*LLWindows+1:(k+1)*LLWindows,1)  = OutHL{6}(2).*ones(LLWindows,1);
            HLNetPower1Min(k*LLWindows+1:(k+1)*LLWindows,1)  = OutHL{7}(2).*ones(LLWindows,1);
    
        
            if abs(HLBessPower(k,1)) <= 1e-4
                clusterFractDCH(k,:) = zeros(1,4);
                clusterFractCH(k,:)  = zeros(1,4);
                clusterFract1MinCH((k-1)*LLWindows+1:k*LLWindows,:)  = zeros(LLWindows,4);
                clusterFract1MinDCH((k-1)*LLWindows+1:k*LLWindows,:) = zeros(LLWindows,4);
                bessState((k-1)*LLWindows+1:k*LLWindows,1) = zeros(LLWindows,1);
            else
                clusterFractDCH(k,:) = HLUDch(k,:)./HLBessPower(k);
                clusterFractCH(k,:)  = -HLUCh(k,:)./HLBessPower(k);
                clusterFract1MinCH((k-1)*LLWindows+1:k*LLWindows,:)  = - HLUCh(k,:)./HLBessPower(k).*ones(LLWindows,4);
                clusterFract1MinDCH((k-1)*LLWindows+1:k*LLWindows,:) =   HLUDch(k,:)./HLBessPower(k).*ones(LLWindows,4);
            end
    
            if abs(HLBessPower(k+1,1)) <= 1e-4
                clusterFractDCH(k+1,:) = zeros(1,4);
                clusterFractCH(k+1,:)  = zeros(1,4);
                bessState(k*LLWindows+1:(k+1)*LLWindows,1) = zeros(LLWindows,1);
                clusterFract1MinCH(k*LLWindows+1:(k+1)*LLWindows,:)  =   zeros(LLWindows,4);
                clusterFract1MinDCH(k*LLWindows+1:(k+1)*LLWindows,:) =   zeros(LLWindows,4);
            else
                clusterFractDCH(k+1,:) = HLUDch(k+1,:)./HLBessPower(k+1);
                clusterFractCH(k+1,:)  = -HLUCh(k+1,:)./HLBessPower(k+1);
                clusterFract1MinCH(k*LLWindows+1:(k+1)*LLWindows,:)  = - HLUCh(k+1,:)./HLBessPower(k+1).*ones(LLWindows,4);
                clusterFract1MinDCH(k*LLWindows+1:(k+1)*LLWindows,:) =   HLUDch(k+1,:)./HLBessPower(k+1).*ones(LLWindows,4);
            end
    
            clusterFractMPC(k,:) = clusterFractDCH(k,:) + clusterFractCH(k,:);
            clusterFractMPC(k+1,:) = clusterFractDCH(k+1,:) + clusterFractCH(k+1,:);
            
            if (HLBessPower(k) > 1e-4)
                bessState((k-1)*LLWindows+1:k*LLWindows,1) = ones(LLWindows,1);
            elseif(HLBessPower(k) < - 1e-4)
                bessState((k-1)*LLWindows+1:k*LLWindows,1) = -ones(LLWindows,1);
            else
                bessState((k-1)*LLWindows+1:k*LLWindows,1) = zeros(LLWindows,1);
            end
            if (HLBessPower(k+1) > 1e-4)
                bessState(k*LLWindows+1:(k+1)*LLWindows,1) = ones(LLWindows,1);
            elseif(HLBessPower(k+1) < - 1e-4)
                bessState(k*LLWindows+1:(k+1)*LLWindows,1) = -ones(LLWindows,1);
            else
                bessState((k-1)*LLWindows+1:k*LLWindows,1) = zeros(LLWindows,1);
            end
            Shrinking    = 15;
            etaReal(k,1) = eta(t,1);
            eta(t,1)     = 0;
            loadDist     = zeros(LLWindows+1,nScenarios+1); loadDist(1,:) = rw.covLoad*randn(1,nScenarios+1);
            PVDist       = zeros(LLWindows+1,nScenarios+1); pvDist(1,:) = rw.covPV*randn(1,nScenarios+1);
            scenarioLoad(t:t+LLWindows-1,:) = loadRealPower(t,end)*ones(LLWindows,nScenarios+1);
            scenarioPV(t:t+LLWindows-1,:)   = PVRealPower(t,end)*ones(LLWindows,nScenarios+1);    
            for h = 1:LLWindows
            scenarioLoad(t + h - 1,:)   = scenarioLoad(t + h - 1,:) + loadDist(h,:);
            loadDist(h + 1,:)           = rw.alfaLoad*loadDist(h,:) + rw.covLoad*randn(1,nScenarios+1);
            if(t1 <= t+h && t+h <= t2)
                scenarioPV(t + h - 1,:) = scenarioPV(t + h - 1,:) + PVDist(h,:);
                scenarioPV(t + h - 1,:) = scenarioPV(t + h  - 1,:).*(scenarioPV(t + h - 1,:)>=0);
                PVDist(h + 1,:)         = rw.alfaPV*PVDist(h,:) + rw.covPV*randn(1,nScenarios+1);
            else
                scenarioPV(t + h - 1,:) = zeros(1,nScenarios+1);
                PVDist(h + 1,:)         = zeros(1,nScenarios+1);
            end
        end
        nondispScenarios(t:t+LLWindows-1,:) = scenarioLoad(t:t+LLWindows-1,:) - scenarioPV(t:t+LLWindows-1,:);
        end 
        if mod(t,5) == 1
           p = p+1;
           if clusterFractCH(k,1)==0 && clusterFractDCH(k,1)== 0
                piA(p,:) = zeros(1,A.nBattery);
           else
                InBMS = {HLBessPower(k), clusterFractCH(k,1), clusterFractDCH(k,1), BatterySoCASim(t,:)};
                [OutBMSA, BMSFlagA] = BMS_MPC_A(InBMS);
                piA(p,:) = OutBMSA{1}(1,:);
           end
           if clusterFractCH(k,2)==0 && clusterFractDCH(k,2)== 0
                piB(p,:) = zeros(1,B.nBattery);
           else
                InBMS = {HLBessPower(k), clusterFractCH(k,2), clusterFractDCH(k,2), BatterySoCBSim(t,:)};
                [OutBMSB, BMSFlagB] = BMS_MPC_B(InBMS);
                piB(p,:) = OutBMSB{1}(1,:);
           end
           if clusterFractCH(k,3)==0 && clusterFractDCH(k,3)== 0
                piC(p,:) = zeros(1,C.nBattery);
           else
                InBMS = {HLBessPower(k), clusterFractCH(k,3), clusterFractDCH(k,3), BatterySoCCSim(t,:)};
                [OutBMSC, BMSFlagC] = BMS_MPC_C(InBMS);
                piC(p,:) = OutBMSC{1}(1,:);
           end
        end
    
        
    
        if bessState(t) == 1
            tic
            InLL = {nondispScenarios(t:t+Shrinking-1,1:end-1), HLGenPower1Min(t:t+Shrinking-1,1), HLNetPower1Min(t:t+Shrinking-1,1), HLBessPower1Min(t:t+Shrinking-1,1), ...
                    clusterSocSimi(t,:), HLOptimSoc1Min(t:t+Shrinking-1,:), clusterFract1MinDCH(t:t+Shrinking-1,:), eta(t,1)};
            [OutLL, LLflag] = MPC_LL_alfa_dch{Shrinking}(InLL);
            string = yalmiperror(LLflag);
            disp(string)
            LLdt(t) = toc;
            alfa(t,1)        = OutLL{1}(1);
            LLGenPower(t,1)  = OutLL{4}(1);
            LLBessPower(t,1) = HLBessPower1Min(t,1) + alfa(t,1)*(nondispScenarios(t,end) - LLGenPower(t,1) - HLNetPower1Min(t,1) - HLBessPower1Min(t,1));
            LLNetPower(t,1)  = HLNetPower1Min(t,1) + (1-alfa(t,1))*(nondispScenarios(t,end) - LLGenPower(t,1) - HLNetPower1Min(t,1) - HLBessPower1Min(t,1));
         elseif bessState(t) == -1
            tic
            InLL = {nondispScenarios(t:t+Shrinking-1,1:end-1), HLGenPower1Min(t:t+Shrinking-1,1), HLNetPower1Min(t:t+Shrinking-1,1), HLBessPower1Min(t:t+Shrinking-1,1), ...
                    clusterSocSimi(t,:), HLOptimSoc1Min(t:t+Shrinking-1,:), clusterFract1MinCH(t:t+Shrinking-1,:), eta(t,1)};
            [OutLL, LLflag] = MPC_LL_alfa_ch{Shrinking}(InLL);
            string = yalmiperror(LLflag);
            disp(string)
            LLdt(t) = toc;
            alfa(t,1)        = OutLL{1}(1);
            LLGenPower(t,1)  = OutLL{4}(1);
            LLBessPower(t,1) = HLBessPower1Min(t,1) + alfa(t,1)*(nondispScenarios(t,end) - LLGenPower(t,1) - HLNetPower1Min(t,1) - HLBessPower1Min(t,1));
            LLNetPower(t,1)  = HLNetPower1Min(t,1) + (1-alfa(t,1))*(nondispScenarios(t,end) - LLGenPower(t,1) - HLNetPower1Min(t,1) - HLBessPower1Min(t,1));
        else 
            tic
            InLL = {nondispScenarios(t:t+Shrinking-1,1:end-1), HLGenPower1Min(t:t+Shrinking-1,1), HLNetPower1Min(t:t+Shrinking-1,1), eta(t,1)};
            [OutLL, LLflag] = MPC_LL_alfa_off{Shrinking}(InLL);
            string = yalmiperror(LLflag);
            disp(string)
            LLdt(t) = toc;
            alfa(t,1)        = OutLL{1}(1);
            LLGenPower(t,1)  = HLGenPower1Min(t,1) + alfa(t,1)*(nondispScenarios(t,end) - HLGenPower1Min(t,1) - HLNetPower1Min(t,1));
            LLNetPower(t,1)  = HLNetPower1Min(t,1) + (1-alfa(t,1))*(nondispScenarios(t,end) - HLGenPower1Min(t,1) - HLNetPower1Min(t,1));
            LLBessPower(t,1) = 0;
        end
        eta(t+1,1)        = LLNetPower(t,1) - HLNetPower1Min(t,1);
        thetareal(t,1)    = abs(nondispScenarios(t,end) - LLGenPower(t,1) - LLBessPower(t,1) - LLNetPower(t,1));
        uCH_real(t,:)     = -clusterFractCH(k,:)*LLBessPower(t,1);
        uDCH_real(t,:)    = clusterFractDCH(k,:)*LLBessPower(t,1);
        uA_CH_real(t,:)   = piA(p,:)*clusterFractCH(k,1)*LLBessPower(t,1);
        uA_DCH_real(t,:)  = piA(p,:)*clusterFractDCH(k,1)*LLBessPower(t,1);
        uB_CH_real(t,:)   = piB(p,:)*clusterFractCH(k,2)*LLBessPower(t,1);
        uB_DCH_real(t,:)  = piB(p,:)*clusterFractDCH(k,2)*LLBessPower(t,1);
        uC_CH_real(t,:)   = piC(p,:)*clusterFractCH(k,3)*LLBessPower(t,1);
        uC_DCH_real(t,:)  = piC(p,:)*clusterFractDCH(k,3)*LLBessPower(t,1);
        uD_CH_real(t,:)   = clusterFractCH(k,4)*LLBessPower(t,1);
        uD_DCH_real(t,:)  = clusterFractDCH(k,4)*LLBessPower(t,1);
        BatterySoCASim(t+1,:) = BatterySoCASim(t,:) - LLSampling*A.muCharge/A.rateCapacity*uA_CH_real(t,:) - LLSampling*A.muCharge/A.rateCapacity*uA_DCH_real(t,:);
        BatterySoCBSim(t+1,:) = BatterySoCBSim(t,:) - LLSampling*B.muCharge/B.rateCapacity*uB_CH_real(t,:) - LLSampling*B.muCharge/B.rateCapacity*uB_DCH_real(t,:);
        BatterySoCCSim(t+1,:) = BatterySoCCSim(t,:) - LLSampling*C.muCharge/C.rateCapacity*uC_CH_real(t,:) - LLSampling*C.muCharge/C.rateCapacity*uC_DCH_real(t,:);
        BatterySoCDSim(t+1,1) = BatterySoCDSim(t,1) - LLSampling*D.muCharge/D.rateCapacity*uD_CH_real(t,:) - LLSampling*D.muCharge/D.rateCapacity*uD_DCH_real(t,:);
        clusterSocSimi(t+1,:) = [mean(BatterySoCASim(t+1,:)),mean(BatterySoCBSim(t+1,:)),mean(BatterySoCCSim(t+1,:)),BatterySoCDSim(t+1,1)];
        clusterSocSim(t+1,:)  = clusterSocSim(t,:) + LLSampling*Kbch.*uCH_real(t,:) - LLSampling*Kbdch.*uDCH_real(t,:);
    
        Shrinking        = Shrinking - 1;
    end
    %% Save results
    % Low level results
    results.LL.Scenarios{l}      = nondispScenarios;
    results.LL.clusterSocSimi{l}  = clusterSocSimi;
    results.LL.eta{l}           = eta;
    results.LL.alfa{l}           = alfa;
    results.LL.thetareal{l}     = thetareal;
    results.LL.LLGenPower{l}     = LLGenPower;
    results.LL.LLNetPower{l}     = LLNetPower;
    results.LL.LLBessPower{l}    = LLBessPower;
    results.LL.uchReal{l}        = uCH_real;
    results.LL.udchReal{l}       = uDCH_real;
    
    % High level results
    results.HL.HLGenPower{l}     = HLGenPower;
    results.HL.HLNetPower{l}     = HLNetPower;
    results.HL.HLBessPower{l}   = HLBessPower;
    results.HL.clFractCH{l}     = clusterFractCH;
    results.HL.clFractDCH{l}    = clusterFractDCH;
    results.HL.etaReal{l}        = etaReal;
    
    % Grid level results
    results.GR.GridGenPower{l}   = GridGenPower;
    results.GR.GridNetPower{l}   = GridNetPower;
    results.GR.GridBessPower{l}  = GridBessPower;
    results.GR.GridOptimSoc{l}   = GridOptimSoc;
    results.GR.GridFract{l}      = GridFract;
    
    % BMS results
    results.BMS.BatterySoCA{l}   = BatterySoCASim;
    results.BMS.BatterySoCB{l}   = BatterySoCBSim;
    results.BMS.BatterySoCC{l}   = BatterySoCCSim;
    results.BMS.BatterySoCD{l}   = BatterySoCDSim;
    results.BMS.BatteryAuCH{l}   = uA_CH_real;
    results.BMS.BatteryBuCH{l}   = uB_CH_real;
    results.BMS.BatteryCuCH{l}   = uC_CH_real;
    results.BMS.BatteryDuCH{l}   = uD_CH_real;
    results.BMS.BatteryAuDCH{l}  = uA_DCH_real;
    results.BMS.BatteryBuDCH{l}  = uB_DCH_real;
    results.BMS.BatteryCuDCH{l}  = uC_DCH_real;
    results.BMS.BatteryDuDCH{l}  = uD_DCH_real;
    results.BMS.piA{l}         = piA;
    results.BMS.piB{l}         = piB;
    results.BMS.piC{l}         = piC;
    
    % Computattional times
    results.T.dthl{l}         = LLdt;
    results.T.dtll{l}         = HLdt;
    results.T.time1min{l}     = time1Min;
    results.T.time15min{l}     = time15Min;
end

save('results','results')
%% Plot figures
plot_flag = true;
if plot_flag
    figure
    plot(time15Min, HLNetPower, time15Min, HLGenPower, time15Min, HLBessPower(1:length(time15Min)), time15Min, loadNomPower(1:length(time15Min)) - PVNomPower(1:length(time15Min)));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power scheduled by the HL MPC during the day')

    figure
    plot(time1Min, LLNetPower, time1Min, LLGenPower, time1Min, LLBessPower, time1Min, nondispScenarios(1:length(time1Min),nScenarios+1));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power exchange during the day')

    figure
    plot(time15Min, HLNetPower, time15Min, HLGenPower, time15Min, HLBessPower(1:length(time15Min)), time15Min, loadNomPower(1:length(time15Min)) - PVNomPower(1:length(time15Min)),time1Min, LLNetPower, time1Min, LLGenPower, time1Min, LLBessPower, time1Min, nondispScenarios(1:length(time1Min),nScenarios+1));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Power exchange during the day')

    figure
    plot(time1Min, clusterSocSimi(1:end-1,:));
    xlabel('Time [h]');ylabel('SoC [ ]');
    legend('A','B','C','D')
    title('Cluster state of charge during the day')
    yline(0.9,'r')
    yline(0.1,'r')

    figure
    plot(time1Min, clusterSocSimi(1:end-1,:),time15Min,HLclustSoc,'.');
    xlabel('Time [h]');ylabel('SoC [ ]');
    title('Cluster state of charge during the day')
    yline(0.9,'r')
    yline(0.1,'r')

    figure
    tiledlayout(2,1)
    nexttile
    plot(time15Min, clusterFractCH(1:end-1,:));
    xlabel('Time [h]');ylabel('Ci-CH [ ]');
    legend('A','B','C','D')
    ylim([-0.1;1.1])
    title('Cluster power fraction during the day: charge')
    nexttile
    plot(time15Min, clusterFractDCH(1:end-1,:));
    xlabel('Time [h]');ylabel('Ci-DCH [ ]');
    legend('A','B','C','D')
    title('Cluster power fraction during the day: discharge')
    ylim([-0.1;1.1])

    figure
    tiledlayout(2,2)
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),1), time1Min, uDCH_real(:,1));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharge power real vs HL: cluster - A')
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),2), time1Min, uDCH_real(:,2));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharge power real vs HL: cluster - B')
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),3), time1Min, uDCH_real(:,3));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharge power real vs HL: cluster - C')
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),4), time1Min, uDCH_real(:,4));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharge power real vs HL: cluster - D')

    figure
    tiledlayout(2,2)
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),1), time1Min, uCH_real(:,1));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Charge power real vs HL: cluster - A')
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),2), time1Min, uCH_real(:,2));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Charge power real vs HL: cluster - B')
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),3), time1Min, uCH_real(:,3));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Charge power real vs HL: cluster - C')
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),4), time1Min, uCH_real(:,4));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Charge power real vs HL: cluster - D')


    figure
    tiledlayout(2,1)
    nexttile
    stairs(time1Min,LLdt)
    yline(mean(LLdt),'r');
    legend('LL time', 'mean')
    xlabel('Time [h]');ylabel('Time [s]');
    title('LL MPC computational time')
    nexttile
    stairs(time15Min,HLdt)
    yline(mean(HLdt),'r');
    legend('HL time', 'mean')
    xlabel('Time [h]');ylabel('Time [s]');
    title('HL MPC computational time')

    figure
    plot(time1Min,100*alfa);xlabel('Time [h]');ylabel('alfa [%]');
    title('Alfa - mixing coefficient')
    ylim([-5;110])

    figure
    tiledlayout(3,1)
    nexttile
    plot(time1Min,LLNetPower, time1Min, HLNetPower1Min(1:length(time1Min)));
    nexttile
    plot(time1Min,eta(1:length(time1Min)))    
    yline(-5,'r')
    yline(+5,'r')
    nexttile
    stairs(time15Min,etaReal)
    yline(-5,'r')
    yline(+5,'r')

end