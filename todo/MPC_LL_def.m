clearvars; clc;
%close all;
yalmip('clear')

addpath('utils');
addpath('utils\data');

load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');

%% Optimization variables (Charge)
% Scheduled Bess
PBessBar      = sdpvar(LLWindows,1);
% Scheduled Gen
PGenBar       = sdpvar(LLWindows,1);
% % Scheduled Net
PNetBar       = sdpvar(LLWindows,1);
% Scheduled soc
clustSocBar   = sdpvar(LLWindows,nCluster);
clustFractCH  = sdpvar(LLWindows,nCluster);
clustFractDCH = sdpvar(LLWindows,nCluster);

clustSoc0     = sdpvar(1,nCluster);

% Gen control action
duGen       = sdpvar(LLWindows,1);
duBess      = sdpvar(LLWindows,1);
% Powers
genPower    = sdpvar(LLWindows,1);
bessPower   = sdpvar(LLWindows,1);

% Slack variables
l           = sdpvar(1,1);

% Non dispachable power
nondispScenarios  = sdpvar(LLWindows,nScenarios,'full');

F = [];
LLconstraints = [];
for t = 1:LLWindows
    LLconstraints = [LLconstraints; genPower(t) == duGen(t) + PGenBar(t)]; %#ok<*AGROW> 
    LLconstraints = [LLconstraints; bessPower(t) == duBess(t) + PBessBar(t)]; 

end

LLconstraints = [LLconstraints; bessPower <= maxDCHBessPower];
LLconstraints = [LLconstraints; bessPower >= -maxCHBessPower];
LLconstraints = [LLconstraints; genPower >= minGenPower];
LLconstraints = [LLconstraints; genPower <= maxGenPower];

for i = 1:nScenarios
    % Powers
    netPower{i}  = sdpvar(LLWindows,1);
    % Soc and energy exchanged
    clustSoc{i}  = sdpvar(LLWindows+1,nCluster,'full');
    eta{i}       = sdpvar(LLWindows+1,1);

    % Power balance
    LLconstraints = [LLconstraints; bessPower + genPower + netPower{i} == nondispScenarios(:,i)];
    
    % Network operational power
    LLconstraints = [LLconstraints; netPower{i} <= maxNetPower];
    LLconstraints = [LLconstraints; netPower{i} >= minNetPower];
    LLconstraints = [LLconstraints; clustFractCH <= 2; clustFractCH >= -1];
    LLconstraints = [LLconstraints; clustFractDCH <= 2; clustFractDCH >= -1];

    for t = 1:LLWindows
        LLconstraints = [LLconstraints; implies(clustFractCH(t,:) == zeros(1,nCluster),bessPower(t)>= 0)];
        LLconstraints = [LLconstraints; implies(clustFractDCH(t,:) == zeros(1,nCluster),bessPower(t)<= 0)];
    end
    % Battery state of charge 
    LLconstraints = [LLconstraints; clustSoc{i}(1,:) == clustSoc0];
    for t = 1:LLWindows
         LLconstraints = [LLconstraints; clustSoc{i}(t+1,:) == clustSoc{i}(t,:) - LLSampling*Kbch.*clustFractCH(t,:)*bessPower(t) - LLSampling*Kbch.*clustFractDCH(t,:)*bessPower(t)];
    end
    
    % Battery operational constraints
    LLconstraints = [LLconstraints; clustSoc{i} <= 0.90];
    LLconstraints = [LLconstraints; clustSoc{i} >= 0.1];
    
    
    % Energy difference
    LLconstraints = [LLconstraints; eta{i}(1) == 0];
    for t = 1:LLWindows
        LLconstraints = [LLconstraints; eta{i}(t+1) == eta{i}(t) + (netPower{i}(t) - PNetBar(t))];
    end
    LLconstraints = [LLconstraints; eta{i}(end) <= etaMax + l];
    LLconstraints = [LLconstraints; eta{i}(end) >= -etaMax - l];
    LLconstraints = [LLconstraints; l >= 0];

    
    % Cost function
    LLf{i} = 0;
    for t = 1:LLWindows
        LLf{i} = LLf{i} + 0.1*duGen(t)^2 + 0.1*duBess(t)^2 ;
    end
    
    LLf{i} = LLf{i} + 10*l +  10*(clustSoc{i}(end,:) - clustSocBar(end,:))*(clustSoc{i}(end,:) - clustSocBar(end,:))';
    F = [F; LLf{i}];
end

LLcostfnc = max(F);
% Gurobi
optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.NumericFocus',3,'gurobi.BarHomogeneous',1);

LLIn  = {nondispScenarios, PBessBar, PGenBar, PNetBar, clustSoc0, clustSocBar, clustFractCH, clustFractDCH};
LLOut = {LLcostfnc, eta{1}, clustSoc{1}, bessPower, genPower, netPower{1}};
MPC_LL = optimizer(LLconstraints, LLcostfnc, optimOptions, LLIn, LLOut);

save('utils\MPC_optimizer\MPC_LL','MPC_LL');