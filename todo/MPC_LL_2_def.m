clearvars; clc;
%close all;
yalmip('clear')

addpath('utils');
addpath('utils\data');

load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');

%% Optimization variables (Charge)
% Scheduled Bess
PBessBar      = sdpvar(LLWindows,1);
% Scheduled Gen
PGenBar       = sdpvar(LLWindows,1);
% % Scheduled Net
PNetBar       = sdpvar(LLWindows,1);
% Scheduled soc
clustFractCH  = sdpvar(LLWindows,nCluster);
clustFractDCH = sdpvar(LLWindows,nCluster);

clustSoc0     = sdpvar(1,nCluster);

% Generator power
genPower      = sdpvar(LLWindows,1);
duGen         = sdpvar(LLWindows,1);
alfa          = sdpvar(LLWindows,1);

% Slack variables
l             = sdpvar(1,1);

% Non dispachable power
nondispScenarios  = sdpvar(LLWindows,nScenarios,'full');

F = [];
LLconstraints = [];
for t = 1:LLWindows
    LLconstraints = [LLconstraints; genPower(t) == duGen(t) + PGenBar(t)]; %#ok<*AGROW> 
end

% Operational constraints
LLconstraints = [LLconstraints; genPower >= minGenPower];
LLconstraints = [LLconstraints; genPower <= maxGenPower];

LLconstraints = [LLconstraints; clustFractCH <= 2; clustFractCH >= -1];
LLconstraints = [LLconstraints; clustFractDCH <= 2; clustFractDCH >= -1];

for i = 1:nScenarios
    % Bess powers
    bessPower{i}  = sdpvar(LLWindows,1); %#ok<*SAGROW> 
    duBess{i}     = sdpvar(LLWindows,1);
    % Network power
    netPower{i}   = sdpvar(LLWindows,1);
    duNet{i}      = sdpvar(LLWindows,1);

    % Energy
    eta{i}       = sdpvar(LLWindows+1,1);
    % Soc
    clustSoc{i}  = sdpvar(LLWindows+1,nCluster);

    % Operational constraints
    LLconstraints = [LLconstraints; bessPower{i} <= maxDCHBessPower];
    LLconstraints = [LLconstraints; bessPower{i} >= -maxCHBessPower];
    LLconstraints = [LLconstraints; netPower{i} >= minNetPower];
    LLconstraints = [LLconstraints; netPower{i} <= maxNetPower];

    for t = 1:LLWindows
        LLconstraints = [LLconstraints; bessPower{i}(t) == duBess{i}(t) + PBessBar(t)]; 
        LLconstraints = [LLconstraints; netPower{i}(t) == duNet{i}(t) + PNetBar(t)]; 
        LLconstraints = [LLconstraints; implies(clustFractCH(t,:) == zeros(1,nCluster), bessPower{i}(t)>= 0)];
        LLconstraints = [LLconstraints; implies(clustFractDCH(t,:) == zeros(1,nCluster), bessPower{i}(t)<= 0)];
    end

    % Power balance
    LLconstraints = [LLconstraints; alfa.*bessPower{i} + genPower + (ones(size(alfa))-alfa).*netPower{i} == nondispScenarios(:,i)];
    
    % Battery state of charge 
    LLconstraints = [LLconstraints; clustSoc{i}(1,:) == clustSoc0];
    for t = 1:LLWindows
         LLconstraints = [LLconstraints; clustSoc{i}(t+1,:) == clustSoc{i}(t,:) - LLSampling*Kbch.*clustFractCH(t,:)*bessPower{i}(t) - LLSampling*Kbch.*clustFractDCH(t,:)*bessPower{i}(t)];
    end
    
    % Battery operational constraints
    LLconstraints = [LLconstraints; clustSoc{i} <= 0.90];
    LLconstraints = [LLconstraints; clustSoc{i} >= 0.1];

    % Energy difference
    LLconstraints = [LLconstraints; eta{i}(1) == 0];
    for t = 1:LLWindows
        LLconstraints = [LLconstraints; eta{i}(t+1) == eta{i}(t) + duNet{i}(t)];
    end
    LLconstraints = [LLconstraints; eta{i}(end) <= etaMax + l];
    LLconstraints = [LLconstraints; eta{i}(end) >= -etaMax - l];
    LLconstraints = [LLconstraints; l >= 0];

    % Cost function
    LLf{i} = 0;
    for t = 1:LLWindows
        LLf{i} = LLf{i} + 0.01*duNet{i}(t)^2 + 1*duGen(t)^2;
    end
    
    LLf{i} = LLf{i} + 10*l;
    F = [F; LLf{i}];
end
LLcostfnc = max(F);


% Gurobi
optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.NumericFocus',3,'gurobi.BarHomogeneous',1);

LLIn  = {nondispScenarios, PBessBar, PGenBar, PNetBar, clustSoc0, clustFractCH, clustFractDCH};
LLOut = {LLcostfnc, eta{1}, clustSoc{1}, alfa, genPower};
MPC_LL_2 = optimizer(LLconstraints, LLcostfnc, optimOptions, LLIn, LLOut);

save('utils\MPC_optimizer\MPC_LL_2','MPC_LL_2');