clearvars; clc;
%close all;
yalmip('clear')

addpath('utils');
addpath('utils\data');

load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');

%% Optimization variables (Charge)
% Scheduled Gen
PGenBar       = sdpvar(LLWindows,1);
% % Scheduled Net
PNetBar       = sdpvar(LLWindows,1);
% Scheduled soc
clustFractCH  = sdpvar(LLWindows,nCluster);
clustFractDCH = sdpvar(LLWindows,nCluster);

clustSoc0     = sdpvar(1,nCluster);

% Network power
netPower      = sdpvar(LLWindows,1);
duNet         = sdpvar(LLWindows,1);
eta           = sdpvar(LLWindows+1,1);

% Non dispachable power
nondispScenarios  = sdpvar(LLWindows,nScenarios,'full');

LLconstraints = []; F = [];
for t = 1:LLWindows
    LLconstraints = [LLconstraints; netPower(t) == duNet(t) + PNetBar(t)]; %#ok<*AGROW> 

end

% Operational constraints
LLconstraints = [LLconstraints; netPower >= minNetPower];
LLconstraints = [LLconstraints; netPower <= maxNetPower];

LLconstraints = [LLconstraints; clustFractCH <= 1; clustFractCH >= 0];
LLconstraints = [LLconstraints; clustFractDCH <= 1; clustFractDCH >= 0];

LLconstraints = [LLconstraints; eta(1) == 0;];
for t = 1:LLWindows
    LLconstraints = [LLconstraints; eta(t+1) == eta(t) +  duNet(t);];
end


for i = 1:nScenarios
    % Bess powers
    bessPower{i}  = sdpvar(LLWindows,1); %#ok<*SAGROW> 

    % Soc
    clustSoc{i}  = sdpvar(LLWindows+1,nCluster);

    % Operational constraints
    LLconstraints = [LLconstraints; bessPower{i} <= maxDCHBessPower];
    LLconstraints = [LLconstraints; bessPower{i} >= -maxCHBessPower];


    for t = 1:LLWindows
        LLconstraints = [LLconstraints; implies(clustFractCH(t,:) == zeros(1,4), bessPower{i}(t)>= 0)];
        LLconstraints = [LLconstraints; implies(clustFractDCH(t,:) == zeros(1,4), bessPower{i}(t)<= 0)];
    end

    % Power balance
    LLconstraints = [LLconstraints; bessPower{i} + PGenBar + netPower == nondispScenarios(:,i)];
    
    % Battery state of charge 
    LLconstraints = [LLconstraints; clustSoc{i}(1,:) == clustSoc0];
    for t = 1:LLWindows
         LLconstraints = [LLconstraints; clustSoc{i}(t+1,:) == clustSoc{i}(t,:) - LLSampling*Kbch.*clustFractCH(t,:)*bessPower{i}(t) - LLSampling*Kbdch.*clustFractDCH(t,:)*bessPower{i}(t)];
    end
    
    % Battery operational constraints
    LLconstraints = [LLconstraints; clustSoc{i} <= 0.9];
    LLconstraints = [LLconstraints; clustSoc{i} >= 0.1];

    % Cost function
    LLf{i} = 0;
    for t = 1:LLWindows
        LLf{i} = LLf{i} + 0.1*duNet(t)^2;
    end
    for t = 2:LLWindows
        LLf{i} = LLf{i} + 0.01*(bessPower{i}(t)-bessPower{i}(t-1))^2;
    end

    F = [F; LLf{i} + eta(end)^2];
end
LLcostfnc = max(F);

% for i = 1:nScenarios-1
%     LLconstraints = [LLconstraints; bessPower{i}(1) == bessPower{i+1}(1)];
% end

% Gurobi
optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.NumericFocus',3,'gurobi.BarHomogeneous',1);
%optimOptions = sdpsettings('solver','gurobi','verbose',2);

LLIn  = {nondispScenarios, PGenBar, PNetBar, clustSoc0, clustFractCH, clustFractDCH};
LLOut = {clustSoc{1}, bessPower{1}, netPower};
MPC_LL_3 = optimizer(LLconstraints, [], optimOptions, LLIn, LLOut);

save('utils\MPC_optimizer\MPC_LL_3','MPC_LL_3');