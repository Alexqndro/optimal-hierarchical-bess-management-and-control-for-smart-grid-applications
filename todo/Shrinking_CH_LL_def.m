clearvars; clc;
%close all;
yalmip('clear')

addpath('utils');
load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');

risk        = 15/100;                                                      % Risk level                         [ ]
confidence  = 10^-3;                                                       % Confidence level                   [ ]
optimVar    = 60;                                                          % Number of optimization variables   [ ]
nScenarios  = 2/risk*(log(1/confidence) + optimVar - 1);                   %#ok<*NASGU>
                                                                           % Number of scenarios needed to      [ ]
                                                                           % ensure e-guaranteeness
%nScenarios  = truenScenarios(risk,confidence,optimVar);
nScenarios  = 15;

for shiftWindow = LLWindows:-1:1
    %% Optimization variables (Charge)
    % Scheduled Bess
    PBessBar    = sdpvar(1,1);
    % Scheduled Gen
    PGenBar     = sdpvar(1,1);
    % % Scheduled Net
    PNetBar     = sdpvar(1,1);
    % Scheduled soc
    clustSocBar = sdpvar(1,nCluster);
    clustSoc0   = sdpvar(1,nCluster);
    clustFract  = sdpvar(1,nCluster);
    
    % Gen control action
    duGen       = sdpvar(shiftWindow,1);
    duBess      = sdpvar(shiftWindow,1);
    % Powers
    genPower    = sdpvar(shiftWindow,1);
    bessPower   = sdpvar(shiftWindow,1);
    
    % Non dispachable power
    nondispScenarios  = sdpvar(shiftWindow,nScenarios,'full');
    
    F = [];
    LLconstraints = [];
    for t = 1:shiftWindow
        LLconstraints = [LLconstraints; genPower(t) == duGen(t) + PGenBar]; %#ok<*AGROW> 
        LLconstraints = [LLconstraints; bessPower(t) == duBess(t) + PBessBar]; 

    end
    
    LLconstraints = [LLconstraints; bessPower <= 0];
    LLconstraints = [LLconstraints; bessPower >= -maxCHBessPower];
    LLconstraints = [LLconstraints; genPower >= minGenPower];
    LLconstraints = [LLconstraints; genPower <= maxGenPower];
    
    for i = 1:nScenarios
        % Powers
        netPower{i}  = sdpvar(shiftWindow,1);
    
        % Soc and energy exchanged
        clustSoc{i}  = sdpvar(shiftWindow+1,nCluster,'full');
        eta{i}       = sdpvar(shiftWindow+1,1);
    
        % Power balance
        LLconstraints = [LLconstraints; bessPower + genPower + netPower{i} == nondispScenarios(:,i)];
        
        % Network operational power
        LLconstraints = [LLconstraints; netPower{i} <= maxNetPower];
        LLconstraints = [LLconstraints; netPower{i} >= minNetPower];
        
        % Battery state of charge 
        LLconstraints = [LLconstraints; clustSoc{i}(1,:) == clustSoc0];
        for t = 1:shiftWindow
             LLconstraints = [LLconstraints; clustSoc{i}(t+1,:) == clustSoc{i}(t,:) - LLSampling*Kbch.*clustFract*bessPower(t)];
        end
        
        % Battery operational constraints
        LLconstraints = [LLconstraints; clustSoc{i} <= 0.90];
        LLconstraints = [LLconstraints; clustSoc{i} >= 0.1];
        
        
        % Energy difference
        LLconstraints = [LLconstraints; eta{i}(1) == 0];
        for t = 1:shiftWindow
            LLconstraints = [LLconstraints; eta{i}(t+1) == eta{i}(t) + (netPower{i}(t) - PNetBar)];
        end
        
        % Cost function
        LLf{i} = 0;
        for t = 1:shiftWindow
            LLf{i} = LLf{i} + duGen(t)^2 + duBess(t)^2 ;
        end
        
        LLf{i} = LLf{i} + 1*eta{i}(end)^2 +  10*(clustSoc{i}(end,:) - clustSocBar(1,:))*(clustSoc{i}(end,:) - clustSocBar(1,:))';
        F = [F; LLf{i}];
    end
    
    LLcostfnc = max(F);
    % Gurobi
    optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.NumericFocus',3,'gurobi.BarHomogeneous',1);
    
    LLIn  = {nondispScenarios, PBessBar, PGenBar, PNetBar, clustSoc0, clustSocBar, clustFract};
    LLOut = {LLcostfnc, eta{1}, clustSoc{1}, bessPower, genPower, netPower{1}};
    MPC_CH_LL{16-shiftWindow}  = optimizer(LLconstraints, LLcostfnc, optimOptions, LLIn, LLOut);
end

Scenarios = nondispNomPower(1)*ones(LLWindows,nScenarios) + 2*randn(LLWindows,nScenarios);
LLInput = {Scenarios, GridBessPower(1), GridGenPower(1), GridNetPower(1) ,GridOptimSoc(1,:) ,GridOptimSoc(2,:) ,GridFract(1,:)};
sol = MPC_CH_LL{1}(LLInput);
save('utils\Shrinking_MPC_CH_LL','MPC_CH_LL');