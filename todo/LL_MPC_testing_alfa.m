clearvars; clc; close all;
yalmip('clear')

%% Addpaths
addpath('utils\');
addpath('utils\data');
addpath('utils\MPC_optimizer');

%% Load parameters, scheduled power and optimizers
load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');
load('MPC_LL_2.mat');

%% Allocation of HL MPC optimizer (too complex to save and load)

run MPC_HL_def.m

%% Grid level nominal Power
GridOptimGen    = [GridGenPower; GridGenPower(1:HLWindows)];
GridOptimNet    = [GridNetPower; GridNetPower(1:HLWindows)];
clustOptimSoc   = [GridOptimSoc; 0.5*ones(HLWindows,4)];
PVNomPower      = [PVNomPower; PVNomPower(1:HLWindows)];
loadNomPower    = [loadNomPower; loadNomPower(1:HLWindows)];

%% Simulation
clusterSocSim       = zeros(length(time1Min),nCluster);
clusterSocSim(1,:)  = clusterSocSim0;
k = 0;
tic
for t = 1:length(time1Min)
    if mod(t,LLWindows) == 1
        k = k+1;
        disp(k);

        clustSocHL0      = clusterSocSim(t,:);
        InHL             = {clustSocHL0, nondispNomPower(k:k+HLWindows-1), GridOptimGen(k:k+HLWindows-1), GridOptimNet(k:k+HLWindows-1)};
        OutHL            = MPC_HL(InHL);

        % Solution
        HLCost(k)            = OutHL{1}; %#ok<*SAGROW> 
        HLBessPower(k:k+1,1) = OutHL{3}(1:2);
        HLUCh(k:k+1,:)       = OutHL{4}(1:2,:);
        HLUDch(k:k+1,:)      = OutHL{5}(1:2,:);
        HLGenPower(k,1)      = OutHL{6}(1);
        HLNetPower(k,1)      = OutHL{7}(1);

        HLOptimSoc1Min((k-1)*LLWindows+1:k*LLWindows,:)  = OutHL{2}(2,:).*ones(LLWindows,4);
        HLBessPower1Min((k-1)*LLWindows+1:k*LLWindows,1) = OutHL{3}(1).*ones(LLWindows,1);
        HLGenPower1Min((k-1)*LLWindows+1:k*LLWindows,1)  = OutHL{6}(1).*ones(LLWindows,1);
        HLNetPower1Min((k-1)*LLWindows+1:k*LLWindows,1)  = OutHL{7}(1).*ones(LLWindows,1);

        HLOptimSoc1Min(k*LLWindows+1:(k+1)*LLWindows,:)  = OutHL{2}(3,:).*ones(LLWindows,4);
        HLBessPower1Min(k*LLWindows+1:(k+1)*LLWindows,1) = OutHL{3}(2).*ones(LLWindows,1);
        HLGenPower1Min(k*LLWindows+1:(k+1)*LLWindows,1)  = OutHL{6}(2).*ones(LLWindows,1);
        HLNetPower1Min(k*LLWindows+1:(k+1)*LLWindows,1)  = OutHL{7}(2).*ones(LLWindows,1);

    
        if abs(HLBessPower(k,1)) <= 1e-4
            clusterFractDCH(k,:) = zeros(1,4);
            clusterFractCH(k,:)  = zeros(1,4);
            clusterFract1MinCH((k-1)*LLWindows+1:k*LLWindows,:)  = zeros(LLWindows,4);
            clusterFract1MinDCH((k-1)*LLWindows+1:k*LLWindows,:) = zeros(LLWindows,4);

        else
            clusterFractDCH(k,:) = HLUDch(k,:)./HLBessPower(k);
            clusterFractCH(k,:)  = -HLUCh(k,:)./HLBessPower(k);
            clusterFract1MinCH((k-1)*LLWindows+1:k*LLWindows,:)  = - HLUCh(k,:)./HLBessPower(k).*ones(LLWindows,4);
            clusterFract1MinDCH((k-1)*LLWindows+1:k*LLWindows,:) =   HLUDch(k,:)./HLBessPower(k).*ones(LLWindows,4);
        end

        if abs(HLBessPower(k+1,1)) <= 1e-4
            clusterFract1MinCH(k*LLWindows+1:(k+1)*LLWindows,:)  =   zeros(LLWindows,4);
            clusterFract1MinDCH(k*LLWindows+1:(k+1)*LLWindows,:) =   zeros(LLWindows,4);
        else
            clusterFract1MinCH(k*LLWindows+1:(k+1)*LLWindows,:)  = - HLUCh(k+1,:)./HLBessPower(k+1).*ones(LLWindows,4);
            clusterFract1MinDCH(k*LLWindows+1:(k+1)*LLWindows,:) =   HLUDch(k+1,:)./HLBessPower(k+1).*ones(LLWindows,4);
        end

        clusterFractMPC(k,:) = clusterFractDCH(k,:) + clusterFractCH(k,:);
    end 
    
    InLL  = {nondispScenarios(t:t+LLWindows-1,1:nScenarios), HLBessPower1Min(t:t+LLWindows-1,1), HLGenPower1Min(t:t+LLWindows-1,1), HLNetPower1Min(t:t+LLWindows-1,:), clusterSocSim(t,:), clusterFract1MinCH(t:t+LLWindows-1,:), clusterFract1MinDCH(t:t+LLWindows-1,:)};
    OutLL = MPC_LL_2(InLL);
    LLalfa(t,1)      = OutLL{3}(1);
    LLGenPower(t,1)  = OutLL{4}(1);

    LLBessPower(t,1) = LLalfa(t,1)*(nondispScenarios(t,nScenarios+1) - LLGenPower(t,1));
    LLNetPower(t,1)  =(1 - LLalfa(t,1))*(nondispScenarios(t,nScenarios+1) - LLGenPower(t,1));
    uCH_real(t,:)    = -clusterFractCH(k,:)*LLBessPower(t,1);
    uDCH_real(t,:)   = clusterFractDCH(k,:)*LLBessPower(t,1);
    clusterSocSim(t+1,:) = clusterSocSim(t,:) + LLSampling*Kbch.*uCH_real(t,:) - LLSampling*Kbdch.*uDCH_real(t,:);
end
tSim = toc;


%% Plot figures
plot_flag = true;
if plot_flag
    figure
    plot(time15Min, HLNetPower, time15Min, HLGenPower, time15Min, HLBessPower(1:length(time15Min)), time15Min, loadNomPower(1:length(time15Min)) - PVNomPower(1:length(time15Min)));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power scheduled by the HL MPC during the day')

    figure
    plot(time1Min, LLNetPower, time1Min, LLGenPower, time1Min, LLBessPower, time1Min, nondispScenarios(1:length(time1Min),nScenarios+1));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power exchange during the day')

    figure
    plot(time15Min, HLNetPower, time15Min, HLGenPower, time15Min, HLBessPower(1:length(time15Min)), time15Min, loadNomPower(1:length(time15Min)) - PVNomPower(1:length(time15Min)),time1Min, LLNetPower, time1Min, LLGenPower, time1Min, LLBessPower, time1Min, nondispScenarios(1:length(time1Min),nScenarios+1));
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Power exchange during the day')

    figure
    plot(time1Min, clusterSocSim(1:end-1,:));
    xlabel('Time [h]');ylabel('SoC [ ]');
    legend('A','B','C','D')
    title('Cluster state of charge during the day')
    yline(0.9,'r')
    yline(0.1,'r')

    figure
    tiledlayout(2,1)
    nexttile
    plot(time15Min, clusterFractCH);
    xlabel('Time [h]');ylabel('SoC [ ]');
    legend('A','B','C','D')
    nexttile
    plot(time15Min, clusterFractDCH);
    xlabel('Time [h]');ylabel('SoC [ ]');
    legend('A','B','C','D')

    figure
    tiledlayout(2,2)
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),1), time1Min, uCH_real(:,1));
    xlabel('Time [h]');ylabel('Power [KW]');
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),2), time1Min, uCH_real(:,2));
    xlabel('Time [h]');ylabel('Power [KW]');
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),3), time1Min, uCH_real(:,3));
    xlabel('Time [h]');ylabel('Power [KW]');
    nexttile
    plot(time15Min, HLUCh(1:length(time15Min),4), time1Min, uCH_real(:,4));
    xlabel('Time [h]');ylabel('Power [KW]');

    figure
    tiledlayout(2,2)
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),1), time1Min, uDCH_real(:,1));
    xlabel('Time [h]');ylabel('Power [KW]');
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),2), time1Min, uDCH_real(:,2));
    xlabel('Time [h]');ylabel('Power [KW]');
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),3), time1Min, uDCH_real(:,3));
    xlabel('Time [h]');ylabel('Power [KW]');
    nexttile
    plot(time15Min, HLUDch(1:length(time15Min),4), time1Min, uDCH_real(:,4));
    xlabel('Time [h]');ylabel('Power [KW]');
end