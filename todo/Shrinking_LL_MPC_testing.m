clearvars; clc; close all;
yalmip('clear')

%% Addpaths
addpath('utils\');

%% Load parameters, scheduled power and optimizers
load('parameters.mat');
load('nominalNondisp.mat');
load('scheduledPower.mat');
load('Shrinking_MPC_CH_LL.mat');
load('Shrinking_MPC_DCH_LL.mat');

%% Allocation of HL MPC optimizer (too complex to save and load)
nScenarios = 15;
run MPC_HL_def.m

%% Photovoltaic panel power production real
t1 = 6/LLSampling; t2 = 18/LLSampling;
PVNomPower    = [PVNomPower; PVNomPower(1:HLWindows)];
PVDisturbance = zeros(t2-t1,nScenarios);
for j = 1:nScenarios
    PVRealPower(:,j) = interp(PVNomPower,15);
    for t = 1:length(PVRealPower(:,j))
        if t1<=t && t<=t2
            PVRealPower(t,j) = PVRealPower(t,j) + PVDisturbance(t,j);
            PVDisturbance(t+1,j) = alfaPV*PVDisturbance(t,j) + covPV*randn(1);
        end
        if PVRealPower(t,j) < 0
            PVRealPower(t,j) = 0;
        end
    end
end

%% Load power consumption real
loadNomPower    = [loadNomPower; loadNomPower(1:HLWindows)];
for j = 1:nScenarios
    loadRealPower(:,j)   = interp(loadNomPower,15);
    loadDisturbance(:,j) = zeros(length(loadRealPower)+1,1);
    for t = 1:length(loadRealPower(:,j))
        loadRealPower(t,j) = loadRealPower(t,j) + loadDisturbance(t,j);
        loadDisturbance(t+1,j) = alfaLoad*loadDisturbance(t,j) + covLoad*randn(1);
    end
end
nondispScenarios = loadRealPower-PVRealPower;

%% Grid level nominal Power
GridOptimGen    = [GridGenPower; GridGenPower(1:HLWindows)];
GridOptimNet    = [GridNetPower; GridNetPower(1:HLWindows)];
clustOptimSoc   = [GridOptimSoc; 0.5*ones(HLWindows,4)];

%% Simulation
clusterSocSim       = zeros(length(time1Min),nCluster);
clusterSocSim(1,:)  = clusterSocSim0;
k = 0; h = 1;
tic
for t = 1:length(time1Min)
    if mod(t,15) == 1
        k = k+1;
        clustSoc0        = clusterSocSim(t,:);
        InHL             = {clustOptimSoc(k:k+HLWindows,:), clustSoc0, PVNomPower(k:k+HLWindows-1), loadNomPower(k:k+HLWindows-1), GridOptimGen(k:k+HLWindows-1), GridOptimNet(k:k+HLWindows-1)};
        OutHL            = MPC_HL(InHL);

        % Solution
        HLCost(k)        = OutHL{1}; %#ok<*SAGROW> 
        HLOptimSoc(k,:)  = OutHL{2}(2,:);
        HLBessPower(k,1) = OutHL{3}(1);
        HLUCh(k,:)       = OutHL{4}(1,:);
        HLUDch(k,:)      = OutHL{5}(1,:);
        HLGenPower(k,1)  = OutHL{6}(1);
        HLNetPower(k,1)  = OutHL{7}(1);
    
        if abs(HLBessPower(k,1)) <= 1e-4
            clusterFractDCH(k,:) = zeros(1,4);
            clusterFractCH(k,:)  = zeros(1,4);
        else
            clusterFractDCH(k,:) = HLUDch(k,:)./HLBessPower(k);
            clusterFractCH(k,:)  = -HLUCh(k,:)./HLBessPower(k);
        end
        clusterFractMPC(k,:)     = clusterFractDCH(k,:) + clusterFractCH(k,:);
        h = 1;
    end 
    
    if HLBessPower(k,1)>= 0
        InLL  = {nondispScenarios(t:t+LLWindows-h,:), HLBessPower(k,1), HLGenPower(k,1), HLNetPower(k,1), clusterSocSim(t,:), HLOptimSoc(k,:), clusterFractDCH(k,:)};
        OutLL = MPC_DCH_LL{h}(InLL);
        LLBessPower(t,1) = OutLL{4}(1);
        LLGenPower(t,1)  = OutLL{5}(1);
        LLnetPower(t,1)  = OutLL{6}(1);
    end

    if HLBessPower(k,1) < 0
        InLL = {nondispScenarios(t:t+LLWindows-h,:), HLBessPower(k,1), HLGenPower(k,1), HLNetPower(k,1), clusterSocSim(t,:), HLOptimSoc(k,:), clusterFractCH(k,:)};
        OutLL = MPC_CH_LL{h}(InLL);
        LLBessPower(t,1) = OutLL{4}(1);
        LLGenPower(t,1)  = OutLL{5}(1);
        LLnetPower(t,1)  = OutLL{6}(1);
    end
      
    uCH_real(t,:)  = clusterFractCH(k,:)*LLBessPower(t,1);
    uDCH_real(t,:) = clusterFractDCH(k,:)*LLBessPower(t,1);
    LLnetPower(t,1) = loadRealPower(t,1) - PVRealPower(t,1) - LLGenPower(t,1) - LLBessPower(t,1);
    clusterSocSim(t+1,:) = clusterSocSim(t,:) - LLSampling*Kbch.*clusterFractCH(k,:)*LLBessPower(t,1) - LLSampling*Kbdch.*clusterFractDCH(k,:)*LLBessPower(t,1);
    h = h+1;
end
tSim = toc;


%% Plot figures
plot_flag = true;
if plot_flag
    figure
    plot(time15Min, HLNetPower, time15Min, HLGenPower, time15Min, HLBessPower, time15Min, loadNomPower(1:length(time15Min)) - PVNomPower(1:length(time15Min)));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power scheduled by the HL MPC during the day')

    figure
    plot(time1Min, LLnetPower, time1Min, LLGenPower, time1Min, LLBessPower, time1Min, loadRealPower(1:length(time1Min)) - PVRealPower(1:length(time1Min)));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power exchange during the day')

    figure
    plot(time1Min, clusterSocSim(1:end-1,:));
    xlabel('Time [h]');ylabel('SoC [ ]');
    legend('A','B','C','D')
    title('Cluster state of charge during the day')
    yline(0.9,'r')
    yline(0.1,'r')

    figure
    tiledlayout(3,1)
    nexttile
    plot(time15Min, HLUCh, time1Min, -uCH_real);
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Scheduled','Real')
    title('Charging power')
    nexttile
    plot(time15Min, HLUDch, time1Min, uDCH_real);
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharging power')
    legend('Scheduled','Real')
    nexttile
    plot(time15Min, clusterFractMPC);
    xlabel('Time [h]');ylabel('Ci [ ]');
    title('Cluster fraction')
    legend('A','B','C','D')
end