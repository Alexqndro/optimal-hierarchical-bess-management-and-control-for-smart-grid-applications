clearvars; clc;
close all;
yalmip('clear')

addpath('utils\');

load('parameters.mat');
load('nominalNondisp.mat');
load('scheduledPower.mat');

%% Optimization variables
% Input
PVpower       = sdpvar(HLWindows,1);
loadPower     = sdpvar(HLWindows,1);
clustSoc0     = sdpvar(1,nCluster);

% Scheduled inputs
scheduledPnet = sdpvar(HLWindows,1);
scheduledPgen = sdpvar(HLWindows,1);
scheduledSoc  = sdpvar(HLWindows+1,nCluster);

% Control actions
uCh           = sdpvar(HLWindows,nCluster);
uDch          = sdpvar(HLWindows,nCluster);
modLoad       = sdpvar(HLWindows,1);
modulation    = intvar(HLWindows,1);

% Outputs
bessPower     = sdpvar(HLWindows,1);
genPower      = sdpvar(HLWindows,1);
netPower      = sdpvar(HLWindows,1);
% States
clustSoc      = sdpvar(HLWindows+1,nCluster);

%% Constraints
HL_Constraints = [];

% Load balance in nominal condition
HL_Constraints = [HL_Constraints; bessPower + genPower + netPower == modLoad - PVpower];

% % Generator operational limits
HL_Constraints = [HL_Constraints; genPower <= maxGenPower];
HL_Constraints = [HL_Constraints; genPower >= minGenPower];

% % Network selling/buying constraint
HL_Constraints = [HL_Constraints; netPower <= maxNetPower];
HL_Constraints = [HL_Constraints; netPower >= minNetPower];

% Bess operational limits
HL_Constraints = [HL_Constraints; uDch >= 0];
HL_Constraints = [HL_Constraints; uCh  >= 0];

for t = 1:HLWindows
    HL_Constraints = [HL_Constraints; uDch(t,:) <= maxDCHclustPower];
    HL_Constraints = [HL_Constraints; uCh(t,:)  <= maxCHclustPower];
end

HL_Constraints = [HL_Constraints; sum(uDch,2) - sum(uCh,2)  == bessPower];

for t = 1:HLWindows
    HL_Constraints = [HL_Constraints; (uCh(t,1) == 0 & uCh(t,2) == 0 & uCh(t,3) == 0 & uCh(t,4) == 0)  |  (uDch(t,1) == 0 & uDch(t,2) == 0 & uDch(t,3) == 0 & uDch(t,4) == 0)];
end

% Load modulation
HL_Constraints = [HL_Constraints; modulation <= 12];
HL_Constraints = [HL_Constraints; modulation >= 5];
HL_Constraints = [HL_Constraints; modLoad == 0.1*modulation.*loadPower];

% SoC initial condition
HL_Constraints = [HL_Constraints; clustSoc(1,:) == clustSoc0];

% SoC dynamic
for t = 1:HLWindows
    HL_Constraints = [HL_Constraints; clustSoc(t+1,:) == clustSoc(t,:) + HLSampling*Kbch.*uCh(t,:) - HLSampling*Kbdch.*uDch(t,:)]; %#ok<*AGROW>
end
% SoC limit
HL_Constraints = [HL_Constraints; clustSoc <= 0.9];
HL_Constraints = [HL_Constraints; clustSoc >= 0.1];

%% Cost function
besscostfnc = 0;

% Cost term
for t = 1:HLWindows
    besscostfnc = besscostfnc + 0.5*gcost(t)*(genPower(t)-scheduledPgen(t))^2 ...
                              + nsell(t)*max([0,netPower(t)])...
                              + nbuy(t)*min([0,netPower(t)]);
end
% Smart load term
besscostfnc = besscostfnc + 1*norm(modulation - 10*ones(size(modulation)),1);
besscostfnc = besscostfnc + 1*norm(sum(loadPower,1) - sum(modLoad,1),2);

% Bess filtering term
for t = 2:HLWindows
    besscostfnc = besscostfnc + bcost(t)*(bessPower(t) - bessPower(t-1))^2;
end
besscostfnc = besscostfnc + 1e2*norm(clustSoc(end,:) - scheduledSoc(end,:),2);

% Cluster balance term
h = 1;
for t = 1:HLWindows + 1
    Lc{t} = [];%#ok<*AGROW> 
    for j = 1:nCluster-1
        for k = 1:nCluster-h
            clustError = clustSoc(t,j) - clustSoc(t,k + h); 
            Lc{t} = [Lc{t}, clustError];
        end
        h = h + 1;
    end
    h = 1;
    besscostfnc = besscostfnc + 1e6*norm(Lc{t},inf);
end

%% Optimization options
% Gurobi
optimOptions = sdpsettings('solver','gurobi','verbose',2,'showprogress',1);

%% Optimizer definition
% Input
bessIn  = {scheduledSoc, clustSoc0, PVpower, loadPower, scheduledPgen, scheduledPnet};
% Output
bessOut = {besscostfnc, clustSoc, bessPower, uCh, uDch, genPower, netPower, modLoad};
% Optimizer
MPC_HL  = optimizer(HL_Constraints, besscostfnc, optimOptions, bessIn, bessOut);

save("MPC_HL.mat","MPC_HL")


%% Simulation
clusterSocSim           = zeros(length(time1Min),nCluster);
clusterSocSim(1,:)      = clusterSocSim0;

%% Photovoltaic panel power production real
t1 = 6/LLSampling; t2 = 18/LLSampling;
PVNomPower    = [PVNomPower; PVNomPower(1:HLWindows)];
PVRealPower   = interp(PVNomPower,15);
PVDisturbance = zeros(t2-t1,1);

%% Load power consumption real
loadNomPower    = [loadNomPower; loadNomPower(1:HLWindows)];
optimModLoad    = loadNomPower;
loadRealPower   = interp(loadNomPower,15);
loadDisturbance = zeros(size(loadRealPower));

for i = 1:length(time1Min)-1

end


GridOptimGen            = [optimGenPower; optimGenPower(1:HLWindows)];
GridOptimNet            = [optimNetPower; optimNetPower(1:HLWindows)];
clustOptimSoc           = [optimSoc; 0.5*ones(HLWindows,4)];
k = 0;
for t = 1:length(time1Min)
    if mod(t,15) == 1
        k = k+1;
        clustSoc0             = clusterSocSim(t,:);
        bessInput             = {clustOptimSoc(k:k+HLWindows,:), clustSoc0, PVNomPower(k:k+HLWindows-1), optimModLoad(k:k+HLWindows-1), GridOptimGen(k:k+HLWindows-1), GridOptimNet(k:k+HLWindows-1)};
        bessMPCOutput         = MPC_HL(bessInput);

        % Solution
        optimCostMPC(k)        = bessMPCOutput{1}; %#ok<*SAGROW> 
        optimBessPowerMPC(k,1) = bessMPCOutput{3}(1);
        optimUChMPC(k,:)       = bessMPCOutput{4}(1,:);
        optimUDchMPC(k,:)      = bessMPCOutput{5}(1,:);
        optimGenPowerMPC(k,1)  = bessMPCOutput{6}(1);
        optimNetPowerMPC(k,1)  = bessMPCOutput{7}(1);
        optimModLoad(k,1)      = bessMPCOutput{8}(1);
        for j = 1:HLWindows
            loadRealPower((k+j-2)*LLWindows+1:(k+j-1)*LLWindows) = bessMPCOutput{8}(j,1)*ones(15,1);
        end
      %  loadNomPower(t:t+HLWindows-1) = bessMPCOutput{8};
    
        if optimBessPowerMPC(k,1) == 0
            clusterFractDCH(k,:) = 0;
            clusterFractCH(k,:)  = 0;
        else
            clusterFractDCH(k,:) = optimUDchMPC(k,:)./optimBessPowerMPC(k);
            clusterFractCH(k,:)  = -optimUChMPC(k,:)./optimBessPowerMPC(k);
        end
        clusterFractMPC(k,:)     = clusterFractDCH(k,:) + clusterFractCH(k,:);
    end 

    if t1<=t && t<=t2
        PVRealPower(t) = PVRealPower(t) + PVDisturbance(t);
        PVDisturbance(t+1) = alfaPV*PVDisturbance(t) + covPV*randn(1);
    end
    if PVRealPower(t) < 0
        PVRealPower(t) = 0;
    end
    loadRealPower(t) = loadRealPower(t) + loadDisturbance(t);
    loadDisturbance(t+1) = alfaLoad*loadDisturbance(t) + covLoad*randn(1);

    realBessPower(t,1) = loadRealPower(t,1) - PVRealPower(t,1) - optimGenPowerMPC(k,1) - optimNetPowerMPC(k,1);
    clusterSocSim(t+1,:) = clusterSocSim(t,:) - LLSampling*Kbch.*clusterFractCH(k,:)*realBessPower(t,1) - LLSampling*Kbdch.*clusterFractDCH(k,:)*realBessPower(t,1);
end


%% Plot figures
plot_flag = true;
if plot_flag
    figure
    plot(time15Min, optimNetPowerMPC, time15Min, optimGenPowerMPC, time15Min, optimBessPowerMPC, time15Min, loadNomPower(1:length(time15Min)) - PVNomPower(1:length(time15Min)));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power exchange during the day')

    figure
    plot(time15Min, optimNetPowerMPC, time15Min, optimGenPowerMPC, time15Min, optimBessPowerMPC,time15Min, loadNomPower(1:length(time15Min)) - PVNomPower(1:length(time15Min)), time1Min, realBessPower, time1Min, loadRealPower(1:length(time1Min)) - PVRealPower(1:length(time1Min)));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Optim Network','Optim Generator','Optim Bess','Optim load','Real Bess', 'Real Load');
    title('Power exchange during the day')

    figure
    plot(time1Min, clusterSocSim(1:end-1,:));
    xlabel('Time [h]');ylabel('SoC [ ]');
    legend('A','B','C','D')
    title('Cluster state of charge during the day')
    yline(0.9,'r')
    yline(0.1,'r')

    figure
    tiledlayout(3,1)
    nexttile
    plot(time15Min, optimUChMPC);
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Charging power')
    nexttile
    plot(time15Min, optimUDchMPC);
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharging power')
    nexttile
    plot(time15Min, clusterFractMPC);
    xlabel('Time [h]');ylabel('Ci [ ]');
    title('Cluster fraction')
    legend('A','B','C','D')

end