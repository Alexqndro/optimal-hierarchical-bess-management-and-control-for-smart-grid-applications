# Optimal hierarchical BESS management and control for smart grid applications

## Description
With the increasing penetration of RES - Renewable Energy Sources - in smart grids, BESS - Battery Energy Storage Systems - have become one of the main tools to decouple generation and consumption and increase grid reliability. In this thesis, an optimal hierarchical BESS management for smart grid optimization is presented.
The proposed method is based on a two-level stochastic MPC with scenario approach, the hierarchical structure allows balancing the state of charge in clusters and batteries on different time scales, while meeting operational constraints; the scenario approach, a novel optimization method that deals with optimization under uncertainty, addresses the uncertainties in RES and loads and guarantees statistical bounds on constraint violation. The proposed method is then integrated in a smart grid economic MPC, which accounts for the grid topology and minimizes operational costs, and successfully tested in multiple grid configurations.

## Usage
TODO

## Support & Contributing
For any questions and suggestions, or if you want to contribute, write to alessandrodelduca.96@gmail.com

## Authors and acknowledgment
Author: Alessandro Del Duca
Supervisors: Riccardo Scattolini, Fredy Orlando Ruiz Palacios, Lorenzo Mario Fagiano

## License
All the code in the repository is under GNU General public License 3.0v 

## Project status
Running
