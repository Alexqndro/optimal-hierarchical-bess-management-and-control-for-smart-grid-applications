clearvars; clc;
% close all;
yalmip('clear')

addpath('utils\');
addpath('utils\data');

load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');

run MPC_HL_def.m;

%% Simulation
clusterSocSim           = zeros(length(time1Min),nCluster);
clusterSocSim(1,:)      = clusterSocSim0;

GridOptimGen    = [GridGenPower; GridGenPower(1:HLWindows)];
GridOptimNet    = [GridNetPower; GridNetPower(1:HLWindows)];
GridOptimBess   = [GridBessPower; GridBessPower(1:HLWindows)];
clustOptimSoc   = [GridOptimSoc; 0.3*ones(HLWindows,4)];
chosenScenario  = randi([1,nScenarios+1],1);
disp(chosenScenario);
k = 0;
for t = 1:length(time1Min)
    if mod(t,15) == 1
        k = k+1;
        disp(k);
        clustSoc0             = clusterSocSim(t,:);
        bessInput             = {clustSoc0, nondispNomPower(k:k+HLWindows-1), GridOptimGen(k:k+HLWindows-1), GridOptimNet(k:k+HLWindows-1)};
        bessMPCOutput         = MPC_HL(bessInput);

        % Solution
        optimCostMPC(k)        = bessMPCOutput{1}; %#ok<*SAGROW> 
        optimBessPowerMPC(k,1) = bessMPCOutput{3}(1);
        optimUChMPC(k,:)       = bessMPCOutput{4}(1,:);
        optimUDchMPC(k,:)      = bessMPCOutput{5}(1,:);
        optimGenPowerMPC(k,1)  = bessMPCOutput{6}(1);
        optimNetPowerMPC(k,1)  = bessMPCOutput{7}(1);
        optimSocMPC(k,1)       = bessMPCOutput{2}(2);
        if optimBessPowerMPC(k,1) == 0
            clusterFractDCH(k,:) = 0;
            clusterFractCH(k,:)  = 0;
        else
            clusterFractDCH(k,:) = optimUDchMPC(k,:)./optimBessPowerMPC(k);
            clusterFractCH(k,:)  = -optimUChMPC(k,:)./optimBessPowerMPC(k);
        end
        clusterFractMPC(k,:)     = clusterFractDCH(k,:) + clusterFractCH(k,:);
    end 

    realBessPower(t,1) = nondispRealPower(t,chosenScenario) - optimGenPowerMPC(k,1) - optimNetPowerMPC(k,1);
    clusterSocSim(t+1,:) = clusterSocSim(t,:) - LLSampling*Kbch.*clusterFractCH(k,:)*realBessPower(t,1) - LLSampling*Kbdch.*clusterFractDCH(k,:)*realBessPower(t,1);
end


%% Plot figures
plot_flag = true;
if plot_flag
    
    figure
    plot(time15Min, optimNetPowerMPC, time15Min, optimGenPowerMPC, time15Min, optimBessPowerMPC, ...
         time15Min, GridOptimNet(1:length(time15Min)), time15Min, GridOptimGen(1:length(time15Min)), time15Min, GridOptimBess(1:length(time15Min)),...
         time15Min, nondispNomPower(1:length(time15Min)));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power exchange during the day Grid vs HL')

    figure
    plot(time15Min, optimNetPowerMPC, time15Min, optimGenPowerMPC, time15Min, optimBessPowerMPC, time15Min, nondispNomPower(1:length(time15Min)));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power exchange during the day HL scheduled')

    figure
    plot(time15Min, optimNetPowerMPC, time15Min, optimGenPowerMPC, time1Min, realBessPower, time1Min, nondispRealPower(1:length(time1Min),chosenScenario),time15Min, optimBessPowerMPC, time15Min, nondispNomPower(1:length(time15Min)));
    xlabel('Time [h]');ylabel('Power [KW]');
    legend('Network','Generator','Bess','Load');
    title('Power exchange during the day real')

    figure
    plot(time1Min, clusterSocSim(1:end-1,:));
    xlabel('Time [h]');ylabel('SoC [ ]');
    legend('A','B','C','D')
    title('Cluster state of charge during the day')
    yline(0.9,'r')
    yline(0.1,'r')

    figure
    plot(time1Min, clusterSocSim(1:end-1,:))
    hold on
    plot(time15Min, optimSocMPC)
    plot(time15Min, clustOptimSoc(1:length(time15Min),:))
    xlabel('Time [h]');ylabel('SoC [ ]');
    legend('soc real','soc HLMPC','soc grid')
    title('Cluster state of charge during the day')
    yline(0.9,'r')
    yline(0.1,'r')

    figure
    tiledlayout(3,1)
    nexttile
    plot(time15Min, optimUChMPC);
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Charging power')
    nexttile
    plot(time15Min, optimUDchMPC);
    xlabel('Time [h]');ylabel('Power [KW]');
    title('Discharging power')
    nexttile
    plot(time15Min, clusterFractMPC);
    xlabel('Time [h]');ylabel('Ci [ ]');
    title('Cluster fraction')
    legend('A','B','C','D')

end