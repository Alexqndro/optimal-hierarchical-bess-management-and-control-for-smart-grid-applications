clearvars; clc;
%close all;
yalmip('clear')

addpath('utils');
addpath('utils\data');

load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');

%% Optimization variables
% Inputs

% Scheduled generator and network power
PGenBar       = sdpvar(LLWindows,1);
PNetBar       = sdpvar(LLWindows,1);
PBessBar      = sdpvar(LLWindows,1);

% Scheduled cluster state of charge and cluster fractions
clustSocBar   = sdpvar(LLWindows,nCluster);
clustFractCH  = sdpvar(LLWindows,nCluster);
clustFractDCH = sdpvar(LLWindows,nCluster);

% Cluster initial state of charge
clustSoc0     = sdpvar(1,nCluster);

% Gen power
genPower      = sdpvar(LLWindows,1);
duGen         = sdpvar(LLWindows,1);

% Bess power
bessPower     = sdpvar(LLWindows,nScenarios); %#ok<*SAGROW> 
duBess        = sdpvar(LLWindows,nScenarios); 
bessState     = sdpvar(LLWindows,1);
% Network power
netPower      = sdpvar(LLWindows,nScenarios);
duNet         = sdpvar(LLWindows,nScenarios);

% Mixing coefficient
alfa         = sdpvar(LLWindows,1);

% delta Energy exchanged with the network
eta           = sdpvar(LLWindows+1,nScenarios);
eta0          = sdpvar(1,1);
rho           = sdpvar(1);

% Non dispachable power scenarios
nondispScenarios  = sdpvar(LLWindows,nScenarios,'full');
deltaScenarios    = sdpvar(LLWindows,nScenarios);

for i = 1:nScenarios
    clustSoc{i}    = sdpvar(LLWindows+1,nCluster);
end

LLconstraints = [];
% Operational constraints
LLconstraints = [LLconstraints; netPower  <= maxNetPower];
LLconstraints = [LLconstraints; netPower  >= minNetPower];
LLconstraints = [LLconstraints; bessPower <= maxDCHBessPower];
LLconstraints = [LLconstraints; bessPower >= -maxCHBessPower];
LLconstraints = [LLconstraints; genPower <= maxGenPower];
LLconstraints = [LLconstraints; genPower >= minGenPower];

% gamma bounds
LLconstraints = [LLconstraints; alfa >= 0];
LLconstraints = [LLconstraints; alfa <= 1];

LLconstraints = [LLconstraints; genPower == duGen + PGenBar];
% Network energy balance
for i = 1:nScenarios
    LLconstraints = [LLconstraints; netPower(:,i) == duNet(:,i) + PNetBar]; %#ok<*AGROW>
    LLconstraints = [LLconstraints; bessPower(:,i) == duBess(:,i) + PBessBar]; %#ok<*AGROW> 
end


% Energy difference
for i = 1:nScenarios
    LLconstraints = [LLconstraints; eta(1,i) == 0];
    for t = 1:LLWindows
        LLconstraints = [LLconstraints; eta(t+1,i) == eta(t,i) + duNet(t,i)/LLTimePeriod];
    end
    LLconstraints = [LLconstraints; abs(eta(2:end,i)) <= etaMax];
end


for i = 1:nScenarios
    % Power balance
    LLconstraints = [LLconstraints; genPower + bessPower(:,i) + netPower(:,i) == nondispScenarios(:,i)];
    LLconstraints = [LLconstraints; deltaScenarios(:,i) == nondispScenarios(:,i) - PGenBar - PNetBar - PBessBar];

    for t = 1:LLWindows
        LLconstraints = [LLconstraints; duBess(t,i) == alfa(t)*deltaScenarios(t,i);];
        LLconstraints = [LLconstraints; duNet(t,i)  == (1-alfa(t))*deltaScenarios(t,i);];
        LLconstraints = [LLconstraints; implies(bessState == 1, bessPower >= 0)];
        LLconstraints = [LLconstraints; implies(bessState == -1, bessPower <= 0)];
        LLconstraints = [LLconstraints; implies(bessState == 0, bessPower == 0)];
        LLconstraints = [LLconstraints; implies(bessState == 0, alfa == 0)];
        LLconstraints = [LLconstraints; bessState <= 1; bessState >= -1;];
    end

    % Battery state of charge 
    LLconstraints = [LLconstraints; clustSoc{i}(1,:) == clustSoc0];
    for t = 1:LLWindows
         LLconstraints = [LLconstraints; clustSoc{i}(t+1,:) == clustSoc{i}(t,:) - LLSampling*Kbch.*clustFractCH(t,:)*bessPower(t,i) - LLSampling*Kbch.*clustFractDCH(t,:)*bessPower(t,i)];
    end

    LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) <= 0.90 + rho];
    LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) >= 0.1 - rho];
    LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) <= 1];
    LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) >= 0];

    % Cost function
     F(i) = 1e6*rho^2  + 1*(clustSocBar(end,:) - clustSoc{i}(end,:))*(clustSocBar(end,:) - clustSoc{i}(end,:))'+ 1*(duNet(:,i)'*duNet(:,i)) + 100*(duGen'*duGen);
    for cl = 1:nCluster
        F(i) = F(i) + 0.1*((clustSocBar(1:end-1,cl) - clustSoc{i}(2:end-1,cl))'*(clustSocBar(1:end-1,cl) - clustSoc{i}(2:end-1,cl)));
    end
end
LLcostfnc = sum(F)/nScenarios;
%% Optimization options
% Gurobi
%optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.BarHomogeneous',1,'gurobi.NumericFocus',2);
optimOptions = sdpsettings('solver','gurobi','verbose',2);
LLIn  = {nondispScenarios, PGenBar, PNetBar, PBessBar, clustSoc0, clustSocBar, clustFractCH, clustFractDCH, bessState};
LLOut = {alfa, eta, clustSoc{1}, genPower, bessPower, netPower};
MPC_LL_alfa = optimizer(LLconstraints, LLcostfnc, optimOptions, LLIn, LLOut);

save('utils\MPC_optimizer\MPC_LL_alfa','MPC_LL_alfa');