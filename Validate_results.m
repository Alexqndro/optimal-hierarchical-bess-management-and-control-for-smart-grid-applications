clearvars; close all; clc
yalmip('clear')

%% Addpaths
addpath('utils\');
addpath('utils\data');
addpath('utils\MPC_optimizer');
addpath('Figures')
%% Load parameters, scheduled power and optimizers
load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');
load('results.mat');
genStatPower(:,1) = results.LL.LLGenPower{1}(:,1);
for j = 1:10
    bessStatPower(:,j) = results.LL.LLBessPower{j}(:,1);
    netStatPower(:,j)  = results.LL.LLNetPower{j}(:,1);
    scenStatPower(:,j) = results.LL.Scenarios{j}(:,end);

end
PS = PLOT_STANDARDS();
netStatMean = mean(netStatPower,2);
bessStatMean = mean(bessStatPower,2);
scenStatMean = mean(scenStatPower,2);
% 
% figure(1);
% fig1_comps.fig = gcf;
% hold on
% fig1_comps.p1 = plot(results.T.time1min{1}, genStatPower);
% fig1_comps.p2 = plot(results.T.time1min{1}, netStatPower);
% fig1_comps.p3 = plot(results.T.time1min{1}, bessStatPower);
% fig1_comps.p4 = plot(results.T.time1min{1}, scenStatPower(1:1440,:));
% fig1_comps.p5 = plot(results.T.time1min{1}, netStatMean);
% fig1_comps.p6 = plot(results.T.time1min{1}, bessStatMean);
% fig1_comps.p7 = plot(results.T.time1min{1}, scenStatMean(1:1440,1));
% 
% fig1_comps.p8 = plot(results.T.time1min{1}, zeros(size(results.T.time1min{1})));
% 
% hold off
% fig1_comps.plotTitle = title('LL planned powers');
% fig1_comps.plotXLabel = xlabel('time [h]');
% fig1_comps.plotYLabel = ylabel('power [KW]');
% 
% 
% fig1_comps.plotLegend = legend([fig1_comps.p1, fig1_comps.p5, fig1_comps.p6, fig1_comps.p7], ...
% 'LL Generator', ' LL Network', 'LL BESS', 'LL Net-demand', 'Interpreter', 'latex');
% legendX0 = .7; legendY0 = .08; legendWidth = .15; legendHeight = .15;
% set(fig1_comps.plotLegend, 'position', [legendX0, legendY0, legendWidth, ...
%     legendHeight], 'Box', 'on');
% set(fig1_comps.plotLegend, 'FontSize', PS.LegendFontSize, 'LineWidth', 0.5, ...
%     'EdgeColor', PS.MyGrey1);
% %========================================================
% % SET PLOT PROPERTIES
% % Choices for COLORS can be found in ColorPalette.png
% set(fig1_comps.p1, 'Color', PS.Blue1, 'LineWidth', 1);
% set(fig1_comps.p2, 'Color', PS.DGreen1, 'LineWidth', 1);
% set(fig1_comps.p3, 'Color', PS.Red3, 'LineWidth', 1);
% set(fig1_comps.p4, 'Color', PS.Orange2, 'LineWidth', 1);
% % set(fig3_comps.p5, 'LineStyle', 'none', 'Marker', 'x', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Blue1, 'MarkerFaceColor', PS.Blue1);
% % set(fig3_comps.p6, 'LineStyle', 'none', 'Marker', 'x', 'MarkerSize', 8, 'MarkerEdgeColor', PS.DGreen1, 'MarkerFaceColor', PS.DGreen1);
% % set(fig3_comps.p7, 'LineStyle', 'none', 'Marker', 'x', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Red3, 'MarkerFaceColor', PS.Red3);
% % set(fig3_comps.p8, 'LineStyle', 'none', 'Marker', 'x', 'MarkerSize', 8, 'MarkerEdgeColor', PS.Orange2, 'MarkerFaceColor', PS.Orange2);
% set(fig1_comps.p6, 'LineStyle', '-.', 'Color', PS.Blue3, 'LineWidth', 2);
% set(fig1_comps.p7, 'LineStyle', '-.', 'Color', PS.Blue3, 'LineWidth', 2);
% set(fig1_comps.p8, 'LineStyle', '-.', 'Color', PS.Red1, 'LineWidth', 1);
% 
% %========================================================
% % INSTANTLY IMPROVE AESTHETICS-most important step
% STANDARDIZE_FIGURE(fig1_comps);

% %========================================================
% % SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
% SAVE_MY_FIGURE(fig1_comps, 'Figures/LL_power_small2.png', 'small');
% SAVE_MY_FIGURE(fig1_comps, 'Figures/LL_power_big2.png', 'big');

for j = 1:10
    clustA(:,j) = results.LL.clusterSocSimi{j}(:,1);
    clustB(:,j) = results.LL.clusterSocSimi{j}(:,2);
    clustC(:,j) = results.LL.clusterSocSimi{j}(:,3);
    clustD(:,j) = results.LL.clusterSocSimi{j}(:,4);
end

ClustAMean = mean(clustA,2);
ClustBMean = mean(clustB,2);
ClustCMean = mean(clustC,2);
ClustDMean = mean(clustD,2);

figure(2);
fig2_comps.fig = gcf;
% fig2_comps.t1 = tiledlayout(2,2);
% fig2_comps.n(1) = nexttile;
hold on
fig2_comps.p1 = plot(results.T.time1min{1}(900:1080), clustA(900:1080,:)-0.001);
fig2_comps.p2 = plot(results.T.time1min{1}(900:1080), ClustAMean(900:1080)-0.001);
fig2_comps.p13 = plot(results.T.time1min{1}(900:1080), 0.9*ones(1,181));
hold off
fig2_comps.plotTitle = title('Cluster A SoCs for different scenarios');
fig2_comps.plotXLabel = xlabel('Time [h]');
fig2_comps.plotYLabel = ylabel('SoC [ ]');
set(fig2_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig2_comps.p2, 'Color', PS.Red1, 'LineWidth', 2);
set(fig2_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [0.8, 0.92];
ax.XAxis.Limits = [15, 18];

% % 
% fig2_comps.n(2) = nexttile;
% hold on
% fig2_comps.p1 = plot(results.T.time1min{1}, clustB(1:end-1,:));
% fig2_comps.p2 = plot(results.T.time1min{1}, ClustBMean(1:end-1));
% fig2_comps.p12 = plot(results.T.time1min{1}, 0.1*ones(1,1440));
% fig2_comps.p13 = plot(results.T.time1min{1}, 0.9*ones(1,1440));
% hold off
% 
% fig2_comps.plotTitle = title('Cluster B SoCs for different scenarios');
% fig2_comps.plotXLabel = xlabel('Time [h]');
% fig2_comps.plotYLabel = ylabel('SoC [ ]');
% set(fig2_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
% set(fig2_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
% set(fig2_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% set(fig2_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% ax = gca;
% ax.YAxis.Limits = [0.0, 1];
% 
% fig2_comps.n(3) = nexttile;
% hold on
% fig2_comps.p1 = plot(results.T.time1min{1}, clustC(1:end-1,:));
% fig2_comps.p2 = plot(results.T.time1min{1}, ClustCMean(1:end-1));
% fig2_comps.p12 = plot(results.T.time1min{1}, 0.1*ones(1,1440));
% fig2_comps.p13 = plot(results.T.time1min{1}, 0.9*ones(1,1440));
% hold off
% fig2_comps.plotTitle = title('Cluster C SoCs for different scenarios');
% fig2_comps.plotXLabel = xlabel('Time [h]');
% fig2_comps.plotYLabel = ylabel('SoC [ ]');
% set(fig2_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
% set(fig2_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
% set(fig2_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% set(fig2_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% ax = gca;
% ax.YAxis.Limits = [0.0, 1];
% 
% fig2_comps.n(4) = nexttile;
% hold on
% fig2_comps.p1 = plot(results.T.time1min{1}, clustD(1:end-1,:));
% fig2_comps.p2 = plot(results.T.time1min{1}, ClustDMean(1:end-1));
% fig2_comps.p12 = plot(results.T.time1min{1}, 0.1*ones(1,1440));
% fig2_comps.p13 = plot(results.T.time1min{1}, 0.9*ones(1,1440));
% hold off
% fig2_comps.plotTitle = title('Cluster D SoCs for different scenarios');
% fig2_comps.plotXLabel = xlabel('Time [h]');
% fig2_comps.plotYLabel = ylabel('SoC [ ]');
% set(fig2_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
% set(fig2_comps.p2, 'Color', PS.Green1, 'LineWidth', 2);
% set(fig2_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% set(fig2_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
% Change axis properties
% ax = gca;
% ax.YAxis.Limits = [0.0, 1];
% %========================================================
% % INSTANTLY IMPROVE AESTHETICS-most important step
% STANDARDIZE_FIGURE(fig2_comps);
% 
% %========================================================
% % SAVE FIGURE AS AN IMAGE: SAVE_MY_FIGURE(fig1_comps, 'Myfigure.png', 'small');
% SAVE_MY_FIGURE(fig2_comps, 'Figures/soc_ll_small3.png', 'small');
% SAVE_MY_FIGURE(fig2_comps, 'Figures/soc_ll_big3.png', 'big');

figure(3)
fig3_comps.fig = gcf;
% fig2_comps.t1 = tiledlayout(2,2);
% fig2_comps.n(1) = nexttile;
hold on
fig3_comps.p1 = stairs(results.T.time15min{1}, results.HL.etaReal{1,10});
fig3_comps.p12 = plot(results.T.time15min{1}, -5*ones(size(results.T.time15min{1})));
fig3_comps.p13 = plot(results.T.time15min{1}, 5*ones(size(results.T.time15min{1})));
hold off
fig3_comps.plotTitle = title('Cluster A SoCs for different scenarios');
fig3_comps.plotXLabel = xlabel('Time [h]');
fig3_comps.plotYLabel = ylabel('SoC [ ]');
set(fig3_comps.p1, 'Color', PS.Blue3, 'LineWidth', 2);
set(fig3_comps.p12, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
set(fig3_comps.p13, 'LineStyle', '-.', 'Color', PS.Red3, 'LineWidth', 1);
ax = gca;
ax.YAxis.Limits = [-10, 10];