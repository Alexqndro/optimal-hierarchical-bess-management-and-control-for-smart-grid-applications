%% Optimization variables
% Input
% Scheduled power
nondispPower  = sdpvar(HLWindows,1);
scheduledPnet = sdpvar(HLWindows,1);
scheduledPgen = sdpvar(HLWindows,1);

% Initial cluster state of charge
clustSoc0     = sdpvar(1,nCluster);

% Outputs
% Bess control actions
uCh           = sdpvar(HLWindows,nCluster);
uDch          = sdpvar(HLWindows,nCluster);
bessPower     = sdpvar(HLWindows,1);
spinReserves  = sdpvar(HLWindows,1);

% Generator and network power
genPower      = sdpvar(HLWindows,1);
netPower      = sdpvar(HLWindows,1);

% Cluster state of charge
clustSoc      = sdpvar(HLWindows+1,nCluster);

% Slack variables
l             = sdpvar(1,1);
%% Constraints
HL_Constraints = [];

% Load balance in nominal condition
HL_Constraints = [HL_Constraints; bessPower + genPower + netPower == nondispPower];

% % Generator operational limits
HL_Constraints = [HL_Constraints; genPower <= maxGenPower];
HL_Constraints = [HL_Constraints; genPower >= minGenPower];

% % Network selling/buying constraint
HL_Constraints = [HL_Constraints; netPower <= maxNetPower];
HL_Constraints = [HL_Constraints; netPower >= minNetPower];

for t = 1:HLWindows
    HL_Constraints = [HL_Constraints; spinReserves(t) == maxGenPower - genPower(t) + min(sum(HLSampling^-1*bessCapacity.*fract.*(clustSoc(t,:)-0.1*ones(1,nCluster))),sum(maxDCHclustPower))];
    HL_Constraints = [HL_Constraints; spinReserves(t) >= minSpinReserve];
    %HL_Constraints = [HL_Constraints; abs(netPower(t) - scheduledPnet(t)) <= etaMax];

%     HL_Constraints = [HL_Constraints; netPower(t) - scheduledPnet(t) <= 1.5*etaMax];
%     HL_Constraints = [HL_Constraints; -netPower(t) + scheduledPnet(t) <= 1.5*etaMax];
end

% Bess operational limits
HL_Constraints = [HL_Constraints; uDch >= 0];
HL_Constraints = [HL_Constraints; uCh  >= 0];

for t = 1:HLWindows
    HL_Constraints = [HL_Constraints; uDch(t,:) <= maxDCHclustPower];
    HL_Constraints = [HL_Constraints; uCh(t,:)  <= maxCHclustPower];
end

HL_Constraints = [HL_Constraints; sum(uDch,2) - sum(uCh,2)  == bessPower];

for t = 1:HLWindows
    HL_Constraints = [HL_Constraints; (uCh(t,1) == 0 & uCh(t,2) == 0 & uCh(t,3) == 0 & uCh(t,4) == 0)  |  (uDch(t,1) == 0 & uDch(t,2) == 0 & uDch(t,3) == 0 & uDch(t,4) == 0)];
end

% SoC initial condition
HL_Constraints = [HL_Constraints; clustSoc(1,:) == clustSoc0];

% SoC dynamic
for t = 1:HLWindows
    HL_Constraints = [HL_Constraints; clustSoc(t+1,:) == clustSoc(t,:) + HLSampling*Kbch.*uCh(t,:) - HLSampling*Kbdch.*uDch(t,:)]; %#ok<*AGROW>
end
% SoC limit
for t = 2:HLWindows+1
    HL_Constraints = [HL_Constraints; clustSoc(t,:) <= 0.9 + l];
    HL_Constraints = [HL_Constraints; clustSoc(t,:) >= 0.1 - l];
    HL_Constraints = [HL_Constraints; l >= 0];
    HL_Constraints = [HL_Constraints; l <= 0.1];
end

%% Cost function
HL_costfnc = 0;

% Cost term
% for t = 1:HLWindows
%     HL_costfnc = HL_costfnc + gcost(t)*(genPower(t) - scheduledPgen(t))^2 ...
%                             + nbuy_HL(t)*(max(10,(netPower(t) - scheduledPnet(t))^2) -10)...
%                             + nbuy_HL(t)*max([0,netPower(t)])...
%                             + nsell_HL(t)*min([0,netPower(t)]);
% end
 for t = 1:HLWindows
    HL_costfnc = HL_costfnc + gcost(t)*(genPower(t) - scheduledPgen(t))^2 ...
                            + nbuy_HL(t)*max([0,netPower(t)])...
                            + nsell_HL(t)*min([0,netPower(t)])... 
                            + 0.25*nsell_HL(t)*(netPower(t) - scheduledPnet(t))^2;
end

% Bess filtering term
for t = 2:HLWindows
    HL_costfnc = HL_costfnc + bcost(t)*(bessPower(t)-bessPower(t-1))^2;
end


% Cluster balance term
h = 1;
for t = 2:HLWindows + 1
    Lc{t} = [];%#ok<*SAGROW> 
    for j = 1:nCluster-1
        for k = 1:nCluster-h
            clustError = clustSoc(t,j) - clustSoc(t,k + h); 
            Lc{t} = [Lc{t}, clustError];
        end
        h = h + 1;
    end
    h = 1;
    HL_costfnc = HL_costfnc + 1e3*norm(Lc{t},inf);
end
HL_costfnc = HL_costfnc + 1e5*l;
%% Optimization options
% Gurobi
optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.BarHomogeneous',1,'gurobi.NumericFocus',2);
%optimOptions = sdpsettings('solver','gurobi');

%% Optimizer definition
% Input
bessIn  = {clustSoc0, nondispPower, scheduledPgen, scheduledPnet};
% Output
bessOut = {HL_costfnc, clustSoc, bessPower, uCh, uDch, genPower, netPower};
% Optimizer
MPC_HL  = optimizer(HL_Constraints, HL_costfnc, optimOptions, bessIn, bessOut);

save("utils\MPC_optimizer\MPC_HL.mat","MPC_HL")