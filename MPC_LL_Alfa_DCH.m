clearvars; clc;
%close all;
yalmip('clear')

addpath('utils');
addpath('utils\data');

load('parameters.mat');
load('nominalNondisp.mat');
load('realNondisp.mat');
load('scheduledPower.mat');

%% Optimization variables
% Inputs

for SHWindows = 1:LLWindows
    % Scheduled generator and network power
    PGenBar       = sdpvar(SHWindows,1);
    PNetBar       = sdpvar(SHWindows,1);
    PBessBar      = sdpvar(SHWindows,1);

    % Scheduled cluster state of charge and cluster fractions
    clustSocBar   = sdpvar(SHWindows,nCluster,'full');
    clustFractDCH = sdpvar(SHWindows,nCluster,'full');
    
    % Cluster initial state of charge
    clustSoc0     = sdpvar(1,nCluster);
    
    % Gen power
    genPower      = sdpvar(SHWindows,1);
    duGen         = sdpvar(SHWindows,1);
    
    % Bess power
    bessPower     = sdpvar(SHWindows,nScenarios,'full'); %#ok<*SAGROW> 
    duBess        = sdpvar(SHWindows,nScenarios,'full'); 

    % Network power
    netPower      = sdpvar(SHWindows,nScenarios,'full');
    duNet         = sdpvar(SHWindows,nScenarios,'full');
    
    % Mixing coefficient
    alfa         = sdpvar(SHWindows,1);
    
    % delta Energy exchanged with the network
    eta           = sdpvar(SHWindows+1,nScenarios,'full');
    eta0          = sdpvar(1,1);
    rho           = sdpvar(1);
    gamma         = sdpvar(1);
    % Non dispachable power scenarios
    nondispScenarios  = sdpvar(SHWindows,nScenarios,'full');
    deltaScenarios    = sdpvar(SHWindows,nScenarios,'full');
    
    for i = 1:nScenarios
        clustSoc{i}    = sdpvar(SHWindows+1,nCluster,'full');
    end
    
    LLconstraints = [];
    % Operational constraints
%     LLconstraints = [LLconstraints; netPower  <= maxNetPower + 10];
%     LLconstraints = [LLconstraints; netPower  >= minNetPower - 10];
    LLconstraints = [LLconstraints; bessPower <= maxDCHBessPower];
    LLconstraints = [LLconstraints; bessPower >= 0];
    LLconstraints = [LLconstraints; genPower  <= maxGenPower];
    LLconstraints = [LLconstraints; genPower  >= minGenPower];
    
    % gamma bounds
    LLconstraints = [LLconstraints; alfa >= 0];
    LLconstraints = [LLconstraints; alfa <= 1];
    LLconstraints = [LLconstraints; rho <= 0.1;];
    LLconstraints = [LLconstraints; rho >= 0;];
    LLconstraints = [LLconstraints; gamma >= 0;];

    LLconstraints = [LLconstraints; genPower == duGen + PGenBar];
    % Network energy balance
    for i = 1:nScenarios
        LLconstraints = [LLconstraints; netPower(:,i) == duNet(:,i) + PNetBar]; %#ok<*AGROW>
        LLconstraints = [LLconstraints; bessPower(:,i) == duBess(:,i) + PBessBar]; %#ok<*AGROW> 
    end
    
    
    % Energy difference
    for i = 1:nScenarios
        LLconstraints = [LLconstraints; eta(1,i) == eta0];
        for t = 1:SHWindows
            LLconstraints = [LLconstraints; eta(t+1,i) == eta(t,i) + duNet(t,i)*LLTimePeriod];
        end
        %LLconstraints = [LLconstraints; abs(eta(end,i)) <= gamma + 0.5*etaMax];
        LLconstraints = [LLconstraints; eta(end,i) <= gamma + 0.5*etaMax];
        LLconstraints = [LLconstraints; -eta(end,i) <= gamma + 0.5*etaMax];

    end
    
    
    for i = 1:nScenarios
        % Power balance
        LLconstraints = [LLconstraints; genPower + bessPower(:,i) + netPower(:,i) == nondispScenarios(:,i)];
        LLconstraints = [LLconstraints; deltaScenarios(:,i) == nondispScenarios(:,i) - PGenBar - PNetBar - PBessBar];
    
        for t = 1:SHWindows
            LLconstraints = [LLconstraints; duBess(t,i) == alfa(t)*deltaScenarios(t,i);];
            LLconstraints = [LLconstraints; duNet(t,i)  == (1-alfa(t))*deltaScenarios(t,i);];
        end

        % Battery state of charge 
        LLconstraints = [LLconstraints; clustSoc{i}(1,:) == clustSoc0];
        for t = 1:SHWindows
             LLconstraints = [LLconstraints; clustSoc{i}(t+1,:) == clustSoc{i}(t,:) - LLSampling*Kbdch.*clustFractDCH(t,:)*bessPower(t,i)];
        end
    
        LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) <= 0.90 + rho];
        LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) >= 0.1 - rho];
        LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) <= 1];
        LLconstraints = [LLconstraints; clustSoc{i}(2:end,:) >= 0];
    
        % Cost function
        F(i) = 1e3*gamma^2 + 1e3*rho^2 + 0.1*(eta(2:end,i)'*eta(2:end,i)) + 1*(duBess(:,i)'*duBess(:,i)) + 0.1*(duNet(:,i)'*duNet(:,i));
        for cl = 1:nCluster
            F(i) = F(i) + 10*((clustSocBar(1:end,cl) - clustSoc{i}(2:end,cl))'*(clustSocBar(1:end,cl) - clustSoc{i}(2:end,cl)));
        end
    end
    LLcostfnc = sum(F)/nScenarios;
    %% Optimization options
    % Gurobi
    optimOptions = sdpsettings('solver','gurobi','verbose',2,'gurobi.BarHomogeneous',0,'gurobi.NumericFocus',3,'gurobi.NonConvex',1);
    %optimOptions = sdpsettings('solver','mosek','verbose',2);
    LLIn  = {nondispScenarios, PGenBar, PNetBar, PBessBar, clustSoc0, clustSocBar, clustFractDCH, eta0};
    LLOut = {alfa, eta, clustSoc{1}, genPower, bessPower, netPower};
    MPC_LL_alfa_dch{SHWindows} = optimizer(LLconstraints, LLcostfnc, optimOptions, LLIn, LLOut);
end

save('utils\MPC_optimizer\MPC_LL_alfa_dch','MPC_LL_alfa_dch');